<?php

namespace Tests\Unit;

use App\Models\Customer;
use App\Repositories\CustomerRepository;
use App\Repositories\RestaurantRepository;
use PHPUnit\Framework\TestCase;
use Faker\Factory;

class CustomerTest extends TestCase
{

    protected $customerRepository;

    public function setUp() : void
    {
        parent::setUp();
        $this->customerRepository = new CustomerRepository();
    }

    public function testCustomerCreateSuccess()
    {
            // create data customer
            $faker = Factory::create();
            $customerData = [
                'username' => $faker->name,
                'full_name' => $faker->name,
                'phone' => $faker->phoneNumber(),
                'address' => 'Da Nang',
                'email' => $faker->email,
                'token' => random_str_az(8),
                'password' => $faker->regexify('[A-Za-z0-9]{6}'),
                'role' => 5,
                'join_status' => 1,
                "wedding_id" => 1,
            ];

            $customer = $this->customerRepository->create($customerData);
            // Check customer created instance of Customer
            $this->assertInstanceOf(Customer::class, $customer);

    }
}



