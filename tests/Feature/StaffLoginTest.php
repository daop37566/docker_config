<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class StaffLoginTest extends TestCase
{
    public function testStaffLoginSuccess(){
        $formData = [
            'email' => 'daop37566@gmail.com',
            'password' => 'Duyphuong123'
        ];

        $respo = $this->post('http://localhost/wedding.backend/public/api/v1/auth/login',$formData)->assertOk();

        $this->arrayHasKey('data', $respo->json());
    }


    public function testStaffLoginFail()
    {
        $formData = [
            'email' => 'daop366@gmail.com',
            'password' => 'Duyphuong123'
        ];

        $respo = $this->post('http://localhost/wedding.backend/public/api/v1/auth/login',$formData)->assertDontSee("Tài khoản không chính xác");

        $this->arrayHasKey('data', $respo->json());
    }
}
