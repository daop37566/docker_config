<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTableWedding extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('weddings', function (Blueprint $table) {
            $table->dropColumn('is_close');
            
        });
        Schema::table('weddings', function (Blueprint $table) {
            $table->tinyInteger('is_close')
            ->nullable()
            ->default(0);
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('weddings', function (Blueprint $table) {
            $table->dropColumn('is_close');
            
        });
        Schema::table('weddings', function (Blueprint $table) {
            $table->tinyInteger('is_close')->nullable()->default(null);
        });
    }
}
