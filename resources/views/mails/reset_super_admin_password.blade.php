下記のリンクにアクセスしてパスワードをリセットしてください。<br>
<br>
<a target="_blank" href="{{$app_url}}/reset-password?token={{$token}}">{{$app_url}}/reset-password?token={{$token}}</a>