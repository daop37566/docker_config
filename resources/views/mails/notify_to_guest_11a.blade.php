{{$guest_name}} 様 <br>
<br>
ご出席のご連絡ありがとうございます。<br>
ご参加につきましては、挙式１週間前にリモート参加者様用のURLを送付致しますので、ご確認頂きますますようお願い申し上げます。<br>
また参加費用につきまして、下記の通り新郎新婦の銀行口座へ、参加費のお振込をお願いいたします。<br>
（口座が複数表示される場合には、いずれか1つの口座に全額をお振込みください）<br>
<br>
Info bank <br>
{{$bank_account['bank_name']}}　{{$bank_account['bank_branch']}} <br>
{{$bank_account['card_type']}}　{{$bank_account['account_number']}} <br>
名義：{{$bank_account['holder_name']}} <br>
<br>
リモート参加料金：{{$wedding_price}}円 <br>
<br>
お振込期日：{{$guest_invitation_response_date}} <br>
<br>
またご案内しております通り、リモート参加には下記の環境が必要になりますのでご準備のほどよろしくお願いいたします。<br>
<br>
・1x.0以上のAndroid端末、もしくは最新iOSのiPhone/iPad <br>
・インターネット接続環境（xxGbyte以上の通信が発生しますので、定額制サービスのご利用をおすすめいたします）<br>
・専用アプリ（後日ダウンロードについて別途ご案内いたします）<br>
<br>
それでは、当日のご参加を心待ちにしております。<br>
<br>
@include('mails/footer/to_guest', $data_footer)