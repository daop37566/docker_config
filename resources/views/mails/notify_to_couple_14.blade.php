{{$groom_name}} 様 <br>
{{$bride_name}} 様 <br>
<br>

いつもWow(Watching Online Wedding)のご利用ありがとうございます。<br>
リモートで参加される下記のゲストの方に、リモートでの参加方法について案内のメールを送付いたしました。<br>
@foreach($guests_join as $guest)
@php
$param = urlencode($app_url."/download?token=".$guest->token);
$link = $dynamic_link_url."/?link=".$param;
@endphp
{{ $guest->full_name}} 様　リモート参加URL {{$link}} <br>
@endforeach
<br>

なお、メールアドレスの登録がない下記のゲストの方々にはリモート参加のご案内をお送りしておりませんので、お手数ですが別途ご案内をお送りくださいますようお願いいたします。<br>
@foreach($guests_no_join as $guest)
@php
$param = urlencode($app_url."/download?token=".$guest->token);
$link = $dynamic_link_url."/?link=".$param;
@endphp
{{ $guest->full_name}} 様　リモート参加URL {{$link}} <br>
@endforeach
<br>
詳しくはサイトにログインしてご確認ください。<br>
<br>

{{$url}}<br>

担当：{{$data_footer['pic_name']}} <br>

@include('mails/footer/to_couple', $data_footer)
