{{$contact_name}} 様 <br>
<br>
いつもWow(Watching Online Wedding)のご利用ありがとうございます。<br>
お客様からパスワードリセットのリクエストがありましたので、本メールをお送りしております。<br>
下記のリンクにアクセスしてパスワードをリセットしてください。<br>
<br>
<a target="_blank" href="{{$app_url}}/reset-password?token={{$token}}">{{$app_url}}/reset-password?token={{$token}}</a><br>
<br>
なお、もしお心当たりがない場合、速やかに運営会社までご連絡ください。<br>
<br>
@include('mails/footer/to_restaurant')