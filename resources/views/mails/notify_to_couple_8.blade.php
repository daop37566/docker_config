{{$groom_name}} 様 <br>
{{$bride_name}} 様 <br>
<br>
いつもWow(Watching Online Wedding)のご利用ありがとうございます。<br>
下記の通りWEB招待状の返答がありました。<br>
<br>
@foreach($guests as $guest)
{{$guest['full_name']}} 様　 {{$guest['join_status']}} <br>
@endforeach
<br>
詳しくはサイトにログインしてご確認ください。<br>
<br>
{{$url}}<br>
<br>
担当：{{$data_footer['pic_name']}} <br>
<br>
@include('mails/footer/to_couple', $data_footer)