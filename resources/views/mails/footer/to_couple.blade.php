※system@wowwedding.jp は送信専用のメールアドレスのため、本メールに返信をいただくことはできません。<br>
お手数ですが、お問い合わせは式場担当まで直接お願いいたします。<br>
<br>
Wow(Watching Online Wedding) <br>
https://www.wowwedding.jp <br>
<br>
＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝<br>
{{$res_name}} <br>
{{$res_post_code}} <br>
{{$res_address_1}} <br>
@if(isset($res_address_2))
{{$res_address_2}} <br>
@endif
Tel: {{$res_phone}} <br>
Mail: {{$res_email}} <br>
＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝