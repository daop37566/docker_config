{{$guest_name}} 様 <br>
<br>
ご出席のご連絡ありがとうございます。下記に新郎新婦おふたりの専用ページをご用意しております。<br>
是非ご覧頂き、おふたりからのメッセージもご確認くださいませ。<br>
<br>
<a target="_blank" href="{{$url}}">{{$url}}</a> <br>
@include('mails/footer/to_guest', $data_footer)