{{$guest_name}} 様 <br>
<br>
新郎{{$groom_name}}様、新婦{{$bride_name}}様から結婚式の招待状をお預かりしております。<br>
下記リンクをクリックしてご確認いただけましたら幸いです。<br>
<br>
<a target="_blank" href="{{$url}}">{{$url}}</a><br>
<br>
なお勝手ながら出席のご返答期日を{{$deadline_guest}}に設定させていただいておりますので、早めのご返答をお待ちしております。<br>
<br>
@include('mails/footer/to_guest', $data_footer)