{{$guest_name}} 様 <br>
<br>
この度はWow(Watching Online Wedding)のご利用ありがとうございます。<br>
リモート参加されるゲストの皆様に専用アプリのご案内をしております。<br>
下記URLからアプリのダウンロードならびにアプリの起動が行えます。<br>
<br>
@php
$param = urlencode($app_url."/download?token=".$guest_token);
$link = $dynamic_link_url."/?link=".$param;
@endphp
{{$link}} <br>
<br>

またすでにご案内しております通り、リモート参加には下記の環境が必要になりますのでご注意ください。<br>
<br>

・1x.0以上のAndroid端末、もしくは最新iOSのiPhone/iPad <br>
・インターネット接続環境（xxGbyte以上の通信が発生しますので、定額制サービスのご利用をおすすめいたします）<br>   
<br>

それでは、当日のご参加を心待ちにしております。<br>
<br>

@include('mails/footer/to_guest', $data_footer)
