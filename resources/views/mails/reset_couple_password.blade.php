{{$groom_name}} 様 <br>
{{$bride_name}} 様 <br>
<br>
この度はWow(Watching Online Wedding)のご利用ありがとうございます。<br>
お客様のアカウントをご用意いたしましたので、下記のリンクからご利用いただけます。<br>
<br>
{{$url}} <br>
ログインID：{{$username}} <br>
パスワード：{{$password}} <br>
<br>
なお、もしお心当たりがない場合、お手数ですが担当までご連絡ください。 <br>
<br>
担当：{{$pic_name}} <br>
<br>
@include('mails/footer/to_couple', $data_footer)