<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'login' => [
        'admin' => [
            'login_fail' => '入力したアカウント情報に誤りがあります。正しいメールアドレスとパスワードを入力してください。',
        ],
        'couple' => [
            'login_fail' => '入力したアカウント情報に誤りがあります。正しいログインIDとパスワードを入力してください。パスワードが忘れた場合は式場と問い合わせください。',
        ]

    ],
    'place' => [
        'create_success' => 'Place create success',
        'create_fail' => 'Place create fail',
        'update_success' => 'Place update success',
        'update_fail' => 'Place update fail',
        'delete_success' => 'Place delete success',
        'delete_fail' => 'Place delete fail',
        'wedding_exists' => 'この会場に式があります。',

        'validation' => [
            'name' => [
                'required' => '必須項目に入力してください。',
                'max' => ':max文字列以内を入力してください。'
            ],
            'restaurant_id' => [
                'required' => '必須項目に入力してください。',
                'exists' => 'Restaurant is not exists or is not active'
            ],
            'table_positions' => [
                'array' => 'Must be an array',
                'required' => '必須項目に入力してください。',
                'integer' => 'Must be an integer',
                'max' => ':max文字列以内を入力してください。'
            ],
            'id' => [
                'exists' => 'The place id does not exists'
            ]
        ]
    ],
    'admin_staff' => [
        'delete_success' => 'Admin staff delete success',
        'delete_fail' => 'Admin staff delete fail'
    ],
    'couple' => [
        'validation' => [
            'username' => [
                'required' => '必須項目に入力してください。'
            ],
            'password' => [
                'required' => '必須項目に入力してください。'
            ]
        ]
    ],
    'event' => [
        'create_success' => 'Event create success',
        'create_fail' => 'Event create fail',
        'update_success' => 'Event update success',
        'update_fail' => 'Event update fail',
        'delete_success' => 'Event delete success',
        'delete_fail' => 'Event delete fail',
        'list_fail' => 'Event list fail',
        'list_null' => 'Event list null',
        'detail_fail' => 'Event detail fail',
        'not_found' => 'The wedding event not found !',
        'send_check_seat_subject' => '席次確認依頼',

        'validation' => [
            'title' => [
                'required' => '必須項目に入力してください。',
                'max' => ':max文字列以内を入力してください。'
            ],
            'date' => [
                'required' => '必須項目に入力してください。',
                'date_format' => 'The date format is invalid.',
                'after_or_equal' => '式の日付指定はシステムの現在日付の10日後以降に設定してください。',
                'was_held' => '会場は該当の日時に使用されています。',
            ],
            'pic_name' => [
                'required' => '必須項目に入力してください。',
                'max' => ':max文字列以内を入力してください。'
            ],
            'time_line' => [
                'required' => '必須項目に入力してください。',
                'array' => 'Must be an array.',
                'date_format' => 'The date format is invalid.',
                'after' => '時間が重複しています。ご確認してください。',
                'after_or_equal' => '時間が重複しています。ご確認してください。',
                'min' => ':min文字列以内を入力してください。',
                'max' => ':max文字列以内を入力してください。'
            ],
            'place' => [
                'required' => '必須項目に入力してください。',
                'exists' => 'Place is not exists or is not active'
            ],
            'is_close' => [
                'boolean' => 'Must be a boolean',
                'numeric' => 'The is_close status must be numeric type'
            ],
            'table_map_image' => [
                'max' => ':max文字列以内を入力してください。'
            ],
            'greeting_message' => [
                'max' => ':max文字列以内を入力してください。',
                'required' => 'The greeting message is required !'
            ],
            'thank_you_message' => [
                'max' => ':max文字列以内を入力してください。',
            ],
            'couple_name' => [
                'required' => '必須項目に入力してください。',
                'max' => ':max文字列以内を入力してください。'
            ],
            'allow_remote' => [
                'required' => '必須項目に入力してください。',
                'boolean' => 'Must be a boolean'
            ],
            'guest_invitation_response_date' => [
                'required' => '必須項目に入力してください。',
                'date_format' => 'The guest invitation response date format is invalid',
                'before' => '式日時と席次新郎新婦編集期日より前の日付を選択してください。',
                'before_or_equal' => 'ゲスト招待状返信期日は式当日の9日前に設定してください。'
            ],
            'couple_edit_date' => [
                'required' => '必須項目に入力してください。',
                'date_format' => 'The couple edit date format is invalid',
                'before' => '式日時より前の日付とWEB招待状ゲスト返答期日より後の日付を選択してください。',
                'after' => '式日時より前の日付とWEB招待状ゲスト返答期日より後の日付を選択してください。',
                'before_or_equal' => '席次新郎新婦編集期日は式当日の8日前に設定してください。',
            ],
            'token' => [
                'required' => 'The token is required !',
                'exists' => 'The token does not exist !'
            ],
            'time_table' => [
                'array' => 'The event must be an array.',
                'date_format' => 'The time table date format is invalid',
                'after_or_equal' => 'The time could not be loop',
                'after' => 'The time could not be loop',
                'required' => 'The time is required'
            ],
            'id' => [
                'required' => 'The event id is required',
                'exists' => 'The event id does not exists',
            ],
            'speech_mode' => [
                'required' => 'The speech mode is required',
                'numeric' => 'Must be numeric',
            ],
            'is_livestream' => [
                'required' => 'The islivestream is required',
                'numeric' => 'Must be numeric',
            ],
        ]
    ],
    'user' => [
        'create_success' => 'User create success',
        'create_fail' => 'User create fail',
        'update_sucess' => 'User update success',
        'update_fail' => 'User update fail',
        'delete_success' => 'User delete success',
        'delete_fail' => 'User delete fail',
        'list_fail' => 'User list fail',
        'detail_fail' => 'User detail fail',
        'password_success' => 'Update password success',
        'password_fail' => 'Update password fail',
        'token_success' => 'Token success',
        'token_fail' => 'Token fail',
        'password_verify_fail' => '古いパスワードが間違っています。ご確認してください。',
        'existed' => 'Failed ! User is existed !',
        'not_found' => 'このアカウントが存在しません。システム管理者と問い合わせください。',

        'validation' => [
            'email' => [
                'required' => '必須項目に入力してください。',
                'regex' => 'メールアドレスの形式が正しくありません。ご確認してください。',
                'max' => ':max文字列以内を入力してください。',
                'unique' => 'このアカウントが存在しません。システム管理者と問い合わせください。',
                'exists' => 'パスワードリセットのメールを送りました。\\n メール記載のリンクより1時間以内にパスワードを再設\\n定してください。'
            ],
            'password' => [
                'required' => '必須項目に入力してください。',
                'min' => '半角英数字8～255文字を入力してください。',
                'max' => '半角英数字8～255文字を入力してください。',
                'regex' => '半角英数字文字を入力してください。',
                'confirmed' => '上記の同じパスワードを入力してください。'
            ],
            'token' => [
                'required' => 'The token is required',
                'exists' => 'The token is not exists'
            ]
        ]
    ],
    'restaurant' => [
        'validation' => [
            'restaurant_name' => [
                'required' => '必須項目に入力してください。',
                'max' => ':max文字列以内を入力してください。',
            ],
            'contact_name' => [
                'required' => '必須項目に入力してください。',
                'max' => ':max文字列以内を入力してください。',
            ],
            'phone' => [
                'required' => '必須項目に入力してください。',
                'numeric' => '数字を入力してください。',
                'digits_between' => '半角英数字10～11文字を入力してください。'
            ],
            'company_name' => [
                'required' => '必須項目に入力してください。',
                'max' => ':max文字列以内を入力してください。'
            ],
            'post_code' => [
                'required' => '必須項目に入力してください。',
                'digits' => '半角数字7数字を入力してください。',
                'numeric' => '数字を入力してください。'
            ],
            'address' => [
                'required' => '必須項目に入力してください。',
                'max' => ':max文字列以内を入力してください。'
            ],
            'guest_invitation_response_num' => [
                'required' => '必須項目に入力してください。',
                'numeric' => 'Must be numeric',
                'max' => '半角数字1～180日前までの日数を入力してください。',
                'min' => '半角数字1～180日前までの日数を入力してください。'
            ],
            'couple_edit_num' => [
                'required' => '必須項目に入力してください。',
                'numeric' => 'Must be numeric',
                'max' => '半角数字1～180日前までの日数を入力してください。',
                'min' => '半角数字1～180日前までの日数を入力してください。'
            ],
            'url' => [
                'url' => '「会場リンク」欄に入力されているURLの形式が正しくありません。正しいURLを入力して下さい。'
            ]
        ]
    ],
    'mail' => [
        'send_success' => 'Mail send success',
        'send_fail' => 'Mail send fail',

        'validation' => [
            'email' => [
                'required' => '必須項目に入力してください。',
                'regex' => 'メールアドレスの形式が正しくありません。ご確認してください。',
                'exists' => '入力したアカウント情報に誤りがあります。正しいメールアドレスとパスワードを入力してください。',
                'max' => ':max文字列以内を入力してください。',
                'unique' => 'メールアドレスはすでに登録されています。',
                'different' => 'メールアドレスがすでにシステムで使用されています。 別のメールアドレスを入力してください。'
            ]
        ]
    ],
    'wedding_card' => [
        'create_success' => 'Wedding card create success',
        'create_fail' => 'Wedding card create fail',
        'update_success' => 'Wedding card update success',
        'update_fail' => 'Wedding card update fail',
        'delete_success' => 'Wedding card delete success',
        'delete_fail' => 'Wedding card delete fail',
        'send_mail_sucess' => 'The mail send success',
        'send_mail_fail' => 'The mail send fail',
        'subject_to_staff' => 'WEB招待状確認依頼',

        'validation' => [
            'template_card_id' => [
                'required' => '必須項目に入力してください。',
                'exists' => 'The template card does not exist'
            ],
            'content' => [
                'required' => '必須項目に入力してください。',
                'max' => ':max文字列以内を入力してください。'
            ],
            'couple_photo' => [
                'required' => '必須項目に入力してください。',
                'mimes' => 'The couple photo must be JPG, PNG type',
                'max' => 'The couple photo can not be greater than 10Mb',
            ],
            'wedding_price' => [
                'required' => '必須項目を入力してください。',
                'digits_between' => 'The wedding price is 0~6 number characters',
            ],
            'status' => [
                'required' => 'The status update is require',
                'numeric' => 'Must be numeric',
            ],
        ]
    ],
    'bank_account' => [
        'create_success' => 'Bank account create success',
        'create_fail' => 'Bank account create fail',
        'update_success' => 'Bank account update success',
        'update_fail' => 'Bank account update fail',
        'delete_success' => 'Bank account delete success',
        'delete_fail' => 'Bank account delete fail',
        'not_exist' => 'The bank account does not exists !',

        'validation' => [
            'bank_name' => [
                'required' => '必須項目に入力してください。',
                'max' => 'The bank account can not be greater than :max characters',
            ],
            'bank_branch' => [
                'required' => '必須項目に入力してください。',
                'max' => 'The bank account can not be greater than :max characters',
            ],
            'account_number' => [
                'required' => '必須項目に入力してください。',
                'digits_between' => 'The account number is :digits number characters',
            ],
            'card_type' => [
                'required' => '必須項目に入力してください。',
                'max' => 'The card type can not be greater than :max characters',
            ],
            'holder_name' => [
                'required' => '必須項目に入力してください。',
                'max' => 'The holder name can not be greater than :max characters',
            ],
            'wedding_card_id' => [
                'required' => '必須項目に入力してください。',
                'exists' => 'The wedding card id does not exist',
            ],
            'bank_account' => [
                'array' => 'The data type must be an array',
                'required' => 'The bank account data is required',
                'max' => 'The banks account can not be greater than :max items'
            ],
        ]
    ],
    'participant' => [
        'create_success' => 'Participant create success',
        'create_fail' => 'Participant create fail',
        'update_success' => 'Participant update success',
        'update_fail' => 'Participant update fail',
        'delete_success' => 'Participant delete success',
        'delete_fail' => 'Participant delete fail',
        'list_fail' => 'The list is fail',
        'detail_fail' => 'Participant detail fail',
        'not_found' => 'データがが存在しません。',
        'export_fail' => 'The guest list export fail',
        'max_remote' => '同じテーブルに最大６名のリモート参加者しか指定できません。',
        'max_offline' => 'このテーブルには、最大人数に達しています。',
        'not_check_table' => '出席とリモート参加の参列者に席座を指定してください。',

        'validation' => [
            'is_only_party' => [
                'required' => '必須項目に入力してください。',
                'boolean' => 'The is only party is boolean',
            ],
            'first_name' => [
                'required' => '必須項目を入力してください。',
                'max' => ':max文字列以内を入力してください。',
            ],
            'last_name' => [
                'required' => '必須項目を入力してください。',
                'max' => ':max文字列以内を入力してください。',
            ],
            'relationship_couple' => [
                'required' => '必須項目を入力してください。',
                'max' => ':max文字列以内を入力してください。',
            ],
            'email' => [
                'required' => '必須項目に入力してください。',
                'max' => ':max文字列以内を入力してください。',
                'regex' => 'メールアドレスの形式が正しくありません。 ご確認ください。'
            ],
            'post_code' => [
                'required' => '必須項目に入力してください。',
                'digits' => '半角数字7文字で入力してください。',
                'numeric' => '半角数字7文字で入力してください。',
            ],
            'address' => [
                'required' => '必須項目に入力してください。',
                'max' => ':max文字列以内を入力してください。',
            ],
            'phone' => [
                'required' => 'The phone is required',
                'digits_between' => '半角数字10～11文字で入力してください。'
            ],
            'customer_type' => [
                'required' => '必須項目に入力してください。' ,
                'numeric' => 'The customer type must be numeric',
            ],
            'task_content' => [
                'max' => ':max文字列以内を入力してください。',
            ],
            'free_word' => [
                'max' => ':max文字列以内を入力してください。',
            ],
            'bank_account_id' => [
                'required' => '必須項目に入力してください。',
                'exists' => 'The bank account does not exists'
            ],
            'bank_order' => [
                'required' => '必須項目に入力してください。',
                'min' => 'The bank account value can not be less than :min',
                'max' => 'The bank account value can not be greater than :max',
                'numeric' => 'The bank order must be a numeric',
            ],
            'is_send_wedding_card' => [
                'required' => '必須項目に入力してください。',
                'boolean' => 'The is send wedding card must be an boolean'
            ],
            'id' => [
                'required' => '必須項目に入力してください。',
                'exists' => 'The participant id does not exists',
                'numeric' => 'The id must be numeric',
            ],
            'customer_relatives' => [
                'first_name' => [
                    'required' => 'The customer relative is require',
                    'max' => 'The customer relative first name can not be greater than :max'
                ],
                'last_name' => [
                    'required' => 'The customer relative is require',
                    'max' => 'The customer relative last name can not be greater than :max',
                ],
                'relationship' => [
                    'max' => 'The customer relative last name can not be greater than :max',
                ]
            ],
            'join_status' => [
                'required' => 'The join status is required',
                'deadline' => 'The join status is deadline',
                'numeric' => 'The join status is numeric',
                'min' => 'The join status can not be less than :min',
                'max' => 'The join status can not be greater than :max',
            ],
            'current_position' => [
                'required' => 'The current position is required',
                'numeric' => 'The current position is numeric',
            ],
            'updated_position' => [
                'required' => 'The updated position is required',
                'numeric' => 'The updated position is numeric',
            ],
            'invitation_url' => [
                'required' => 'The invite url is required',
                'exists' => 'The invite url does not exists',
            ],
            'remember_token' => [
                'required' => 'The invite url token is required',
                'exists' => 'The invite url token does not exists',
            ],
            'table_position_id' => [
                'required' => '出席とリモート参加の参列者に席座を指定してください。',
                'exists' => 'The table position does not exists',
            ],
            'wedding_id' => [
                'required' => '必須項目に入力してください。',
                'numeric' => 'The wedding id is numeric',
                'exists' => 'The wedding id does not exists',
            ],
        ]
    ],
    'customer_task' => [
        'create_success' => 'Customer task create success',
        'create_fail' => 'Customer task create fail',
        'update_success' => 'Customer task update success',
        'update_fail' => 'Customer task update fail',
        'delete_success' => 'Customer task delete success',
        'delete_fail' => 'Customer task delete fail',

        'validation' => [
            'name' => [
                'required' => 'Customer task name is required',
            ],
            'description' => [
                'required' => 'Customer task description is required'
            ]
        ]
    ],
    'policy' => [
        'create_success' => 'The policy upload success',
        'create_fail' => 'The policy upload fail',
        'delete_success' => 'The policy delete success',
        'delete_fail' => 'The policy delete fail',

        'validation' => [
            'file_policy' => [
                'format' => 'PDFのいずれかファイルのみです。'
            ],
            'type' => [
                'numeric' => 'The policy type must be numeric'
            ],
            'status' => [
                'numeric' => 'The status is numeric',
            ],
        ]
    ],
    'channel' => [
        'create_success' => 'The channel upload success',
        'create_fail' => 'The channel upload fail',
        'delete_success' => 'The channel delete success',
        'delete_fail' => 'The channel delete fail',
        'update_success' => 'The channel update success',
        'update_fail' => 'The channel update fail'
    ]
];
