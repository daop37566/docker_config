<?php

use Illuminate\Http\Request;
use App\Mail\SendMail;
use Illuminate\Support\Facades\Mail;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {

    Route::post('/auth/register', 'AuthController@register');
    Route::post('/auth/login', 'AuthController@login');
    Route::post('/auth/customer/login', 'AuthCustomerController@login');
    Route::post('/auth/customer/token-login', 'AuthCustomerController@tokenLogin');
    #API CHANGE PASSWORD
    Route::post('/forgot-password','UsersController@sendEmailResetPassword');
    Route::post('/change-password','UsersController@updatePassword');
    Route::post('/check-token-expired','UsersController@checkExpiredToken');
    Route::post('/check-token-exist','UsersController@checkExistToken');

    #Mobile Live Stream Guest
    Route::post('/guest-online/event', 'EventsController@getWeddingEventLivestream');
    #Guest Web Wedding Card
    Route::get('/guest/wedding-card/get', 'WeddingCardsController@guestGetWeddingCard');
    Route::put('/guest/wedding-card/response', 'WeddingCardsController@guestResponseWeddingCard');
    # Get link policy
    Route::get('/policy/{type}', 'PoliciesController@show');

    Route::group(['middleware' => ['jwtAuth']], function () {

        /* API get me all role */
        Route::get('/users/get-me', 'UsersController@getMe');

        /* Role Admin (STAFF ADMIN & SUPER_ADMIN)*/
        Route::group(['middleware' => 'auth.admin'], function(){
            Route::prefix('/users')->group(function () {
                Route::put('/password-verify/update', 'UsersController@updatePasswordWithVerify');
            });
        });

        Route::group(['middleware' => 'auth.observer'], function(){
            Route::get('/detail-event/{id}', 'EventsController@showObserverEvent');
            Route::put('/update-event', 'EventsController@updateObserverEvent');
        });

        /* Role Staff Admin */
        Route::group(['middleware' => 'auth.admin_staff'], function(){
            Route::put('/event/is_livesteam/update', 'EventsController@updateWeddingIsLivesteam');
            Route::resource('/event','EventsController');
            Route::get('/get-status-event/{id}', 'EventsController@showStatusEvent');
            Route::resource('/time-table', 'WeddingTimeTableController');
            Route::prefix('/users')->group(function () {
                Route::put('/staff-admin/create-or-update', 'UsersController@upadateStaffAdmin');
            });

            Route::prefix('/staff')->group(function () {
                Route::get('/guest-participant/get', 'CustomersController@staffGetGuestInfo');
                Route::get('/wedding-card/detail', 'WeddingCardsController@staffGetWeddingCard');
                Route::post('/wedding-card/status/update', 'WeddingCardsController@updateStatus');
                Route::get('/guest-list', 'CustomersController@staffListGuest');
                Route::get('/guest-list/export', 'CustomersController@staffExportCSV');
                Route::get('/main-guest-list', 'CustomersController@staffListMainGuest');
                Route::post('/guest-content/update', 'CustomersController@updateGuestContent');
                Route::post('/guest-participant/add', 'CustomersController@staffAddGuest');
                Route::post('/guest-participant/update', 'CustomersController@staffUpdateGuestInfo');
                Route::post('/guest-participant/reoder-row', 'CustomersController@staffReoderGuest');
            });

            Route::prefix('/couple')->group(function(){
                Route::post('/reset-password', 'CustomersController@resetCouplePassword');
            });

            Route::get('/table-account-wedding/{wedding_id}', 'TableAccountController@getTableAccountOfWedding');
            Route::resource('/places', 'PlacesController');
        });

        /* Role Super Admin */
        Route::group(['middleware' => 'auth.super_admin'], function(){
            Route::prefix('/users')->group(function () {
                Route::post('/super-admin/invite-admin-staff', 'UsersController@inviteNewAdminStaff');
            });

            /** route staffs */
            Route::get('/restaurants-staffs','UsersController@getStaffAdmin');
            Route::get('/staff','UsersController@getListStaff');
            Route::get('/staff/{user_id}','UsersController@getStaff');
            Route::delete('/staff/{user_id}','UsersController@destroyStaff');

            /** Policy Managerment **/
            Route::prefix('/policy-manager')->group(function () {
                Route::post('/policy', 'PoliciesController@store');
                Route::get('/policy/destroy', 'PoliciesController@destroy');
                Route::get('/get-pre-signed', 'PoliciesController@getPreSigned');
                Route::get('/policy', 'PoliciesController@index');
            });

            /** Template Card **/
            Route::resource('/template-card', 'TemplateCardsController');
        });

        /* Role Couple */
        Route::group(['middleware' => 'auth.couple'], function(){
            Route::get('/couple/wedding-card/get-pre-signed', 'WeddingCardsController@getPreSigned');
            Route::post('/couple/invite-card-status/update', 'EventsController@sendInviteCardToGuest');
            Route::get('/couple/event', 'EventsController@coupleDetailEvent');
            Route::get('/couple/guest-list', 'CustomersController@coupleListGuest');
            Route::get('/couple/guest-list/export', 'CustomersController@coupleExportCSV');
            Route::post('/couple/guest-relatives/deletes', 'CustomersController@deleteMultipleGuest');

            Route::prefix('/couple/event')->group(function () {
                Route::post('/thank-message/update', 'EventsController@updateThankMessage');
                Route::post('/guest-participant/update', 'CustomersController@coupleUpdateGuestInfo');
                Route::post('/guest-participant/update-table', 'CustomersController@coupleUpdateGuestTable');
                Route::post('/participant/update', 'CustomersController@coupleUpdateGuestInfo');
                Route::post('/participant/reoder-row', 'CustomersController@coupleReoderGuest');
            });

            Route::get('/couple/template-card', 'TemplateCardsController@index');
            Route::get('/couple/web-wedding-card/preview', 'WeddingCardsController@previewWeddingCard');
            Route::get('/couple/wedding-card/notify-to-staff', 'WeddingCardsController@notifyToStaff');
            Route::get('/couple/event/notify-to-planner', 'EventsController@notifyToPlanner');
            Route::resource('/couple/wedding-card', 'WeddingCardsController');
            Route::resource('/couple/bank-account', 'BankAccountsController');
            Route::resource('/couple/participant', 'CustomersController');
            Route::resource('/couple/customer-task', 'CustomerTasksController');
            Route::resource('/couple/template-content', 'TemplateContentController');
        });

        /* Role Customer*/
        Route::group(['middleware' => 'auth.customer'], function(){
            Route::get('/customer/event', 'EventsController@getWeddingEventWithBearerToken');
        });

        /* Role Observer */
        Route::group(['middleware' => 'auth.observer'], function(){
            Route::post('/is_speech/update', 'CustomersController@updateSpeechFlag');
            Route::post('/event/speech_mode/update', 'EventsController@updateSpeechMode');
            Route::put('/customer/event/state-livesteam', 'EventsController@updateStateLivesteam');
        });

        Route::post('/auth/logout', 'AuthController@logout');
        Route::get('/places-get-pre-signed', 'PlacesController@getPreSigned')->name('get.getPreSigned');
        Route::resource('/table-positon', 'TablePositionsController');
        Route::resource('/customer', 'CustomersController');
        Route::get('/customer-in-wedding', 'CustomersController@getListCustomerInWedding');
        Route::post('/create-dump-customer', 'CustomersController@createCustomerForWeeding');
        Route::resource('/channel','ChannelsController');
        Route::post('agora/store-rtm','AgoraController@storeRtm');
        Route::post('agora/store-rtc','AgoraController@storeRtc');
    });

    Route::get('/agora/get-token','AgoraController@generateToken')->middleware('cors');

    Route::get('/agora/create-channel', function()  {
        \Artisan::call('command:CreateChannel');

        echo true;
    });

    Route::get('/agora/update-event', function()  {
        \Artisan::call('command:UpdateStatusWedding');

        echo true;
    });

    Route::get('/agora/update-token-channel', function()  {
        \Artisan::call('command:UpdateTokenChannel');

        echo true;
    });

    Route::get('/agora/remove-channel', function()  {
        \DB::table('channels')->delete();

        echo true;
    });

    Route::get('/dump-wedding', function(){
        \Artisan::call('db:seed --class=WeddingSeeder');
    });

    Route::get('/un-dump-wedding', function(){
        \Artisan::call('db:seed --class=UnWeddingSeeder');
    });

    Route::get('/queue', function(){
        \Artisan::call('queue:listen');
    });

});
