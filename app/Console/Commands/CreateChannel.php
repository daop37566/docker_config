<?php

namespace App\Console\Commands;

use App\Constants\ChannelName;
use App\Constants\Common;
use Illuminate\Console\Command;
use App\Constants\CustomerConstant;
use App\Constants\EventConstant;
use App\Constants\Role;
use App\Jobs\SendMailCoupleJob;
use App\Libs\Agora\RtcTokenBuilder;
use App\Models\Customer;
use App\Services\ChannelService;
use App\Services\CustomerService;
use App\Traits\EmailTrait;

class CreateChannel extends Command
{
    use EmailTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:CreateChannel';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'create wedding online guest';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = \Carbon\Carbon::now()->addDays(7)->startOfDay();
        $weddings = \DB::table('weddings')
            ->join('customers', 'weddings.id', '=', 'customers.wedding_id')
            ->where('date', '=', $now)
            ->select('weddings.id', 'weddings.guest_invitation_response_date', 'weddings.place_id', 'date')
            ->groupBy('weddings.id', 'weddings.guest_invitation_response_date', 'weddings.place_id', 'date')
            ->get();
        foreach ($weddings as $wedding) {
            $roleHost = RtcTokenBuilder::RolePublisher;
            $roleMember = RtcTokenBuilder::RoleSubscriber;

            // Get Name Groom
            $groomName = $this->coupleNameForMail($wedding->id, Role::GROOM)->full_name;
            // Get Name Bride
            $brideName = $this->coupleNameForMail($wedding->id, Role::BRIDE)->full_name;
            // DataFooter for SendMail
            $dataFooter = $this->dataFooterRestaurant($wedding->id);

            // create role Observer
            $this->createObserver($wedding->id);
            // create channel Stage
            $this->createChannelStage($wedding->id);
            // create channel Coupe
            $this->createChannelCoupe($wedding->id, $dataFooter);
            // create channel Test
            $this->createChannelTest($wedding->id);

            /**
             * create channel Normal
             * send mail for guest
             */
            $data = [
                'wedding_id' => $wedding->id,
                'data_footer' => $dataFooter,
                'groom_name' => $groomName,
                'bride_name' => $brideName
            ];
            // Create Channel NORMAL
            ChannelService::createChannelTableNomal($data);
        }
    }

    /**
     * GET RoomGuest
     */
    public function getRoomGuest($customers)
    {
        $countDevices = count($customers);
        $limit = 6;
        $rooms = [];
        $max = ceil($countDevices / $limit);
        $stringRoomGuest = 'guest_';
        for ($i = 1; $i <= $max; $i++) {
            $length = $i - 1;
            $room = ['id' => 1, 'username' => $stringRoomGuest . random_str(6) . getDateStringRandom(), 'role' => \App\Libs\Agora\RtcTokenBuilder::RolePublisher];
            array_push($rooms, $room);
        }

        return $rooms;
    }

    /**
     * GET getDefaultRoomWedding
     */
    public function getDefaultRoomWedding($name, $role, $wedding_id, $customer_id = null)
    {
        $data = [
            'wedding_id'    => $wedding_id,
            'name'          => $name,
            'display_name'  => $name,
            'amount'        => 6,
            'status'        => Common::STATUS_TRUE,
            'type'          => $role,
            'start_time'    => null,
            'end_time'      => null,
            'created_at'    => \Carbon\Carbon::now(),
            'updated_at'    => \Carbon\Carbon::now(),
            'role'          => RtcTokenBuilder::RolePublisher,
            'customer_id'   => $customer_id,
            'table_position_id' => null
        ];

        return $data;
    }

    /**
     * Create ROLE::Obverser
     */
    public function createObserver($weddingID)
    {
        // GET customer Role Observer
        $customerObserver = Customer::where([
            ['wedding_id', $weddingID],
            ['role', Role::OBSERVER]
        ])->first();
        // add role OBSERVER
        if (!isset($customerObserver)) {
            CustomerService::createCustomerRole($weddingID, Role::OBSERVER);
        }
    }

    /**
     * Create Channel Couple
     * @param $wedding_id & $dataFooter
     */
    public function createChannelCoupe($weddingID, $dataFooter)
    {
        $checkChannel = \DB::table('channels')
            ->where('name', 'like', ChannelName::COUPLE . '%')
            ->where('wedding_id', $weddingID)
            ->exists();
        if (!$checkChannel) {
            $customers = \DB::table('customers')
                ->where('wedding_id', $weddingID)
                ->whereIn('role', [Role::GROOM, Role::BRIDE, Role::OBSERVER])
                ->where('join_status', CustomerConstant::JOIN_STATUS_APPROVED)
                ->get();

            // List guest join yes mail
            $guestsJoin = \DB::table('customers')
                ->where('wedding_id', $weddingID)
                ->whereIn('role', [Role::GUEST])
                ->whereNotNull('customers.email')
                ->where('join_status', CustomerConstant::JOIN_STATUS_APPROVED)
                ->select('full_name','invitation_url','token')
                ->groupBy('full_name','invitation_url','token')
                ->get();

            // List guest no mail
            $guestsNoJoin = \DB::table('customers')
                ->where('wedding_id', $weddingID)
                ->whereIn('role', [Role::GUEST])
                ->whereNull('customers.email')
                ->where('join_status', CustomerConstant::JOIN_STATUS_APPROVED)
                ->select('full_name','invitation_url','token')
                ->groupBy('full_name','invitation_url','token')
                ->get();

            foreach ($customers as $customer) {
                // DATA for SendMail
                $emailContent = [
                    'groom_name' => $this->coupleNameForMail($weddingID, Role::GROOM)->full_name,
                    'bride_name' => $this->coupleNameForMail($weddingID, Role::BRIDE)->full_name,
                    'guests_join' => $guestsJoin,
                    'guests_no_join' => $guestsNoJoin,
                    'app_url' => config('app.app_url'),
                    'dynamic_link_url' => config('app.dynamic_link_url'),
                    'url' => config('app.url'),
                    'data_footer' => $dataFooter,
                ];

                /**
                 *  Send mail for Couple
                 *  Impact area : cronjob create channel
                 * */
                if ($customer->role == Role::GROOM || $customer->role == Role::BRIDE) {
                    SendMailCoupleJob::dispatch("リモート参加案内を送付しました", $customer->email, $emailContent);
                }
            }

            // Create CustomerRole
            $tableAccountId = CustomerService::createCustomerRole($weddingID, Role::COUPE_TABLE);
            // HANDLE DATA COUPLE
            $channel = $this->getDefaultRoomWedding(ChannelName::COUPLE, EventConstant::TYPE_COUPE, $weddingID, $tableAccountId);
            $id = \DB::table('channels')->insertGetId($channel);

            foreach ($customers as $customer) {
                $customer_channel = [
                    'channel_id'    => $id,
                    'is_host'       => Common::STATUS_TRUE,
                    'is_guest'      => Common::STATUS_FALSE,
                    'customer_id'   => $customer->id,
                    'status'        => Common::STATUS_TRUE,
                    'created_at'    => \Carbon\Carbon::now(),
                    'updated_at'    => \Carbon\Carbon::now(),
                ];

                \DB::table('customer_channel')->insert($customer_channel);
            }
        }
    }

    /**
     * Create Channel Stage
     * @param $wedding_id
     */
    public function createChannelStage($weddingID)
    {
        $checkChannel = \DB::table('channels')
            ->where('name', 'like', ChannelName::MAIN . '%')
            ->where('wedding_id', $weddingID)
            ->exists();
        if (!$checkChannel) {
            $customers = \DB::table('customers')
                ->where('wedding_id', $weddingID)
                ->whereIn('role', [Role::GROOM, Role::BRIDE, Role::GUEST, Role::OBSERVER])
                ->where('join_status', CustomerConstant::JOIN_STATUS_APPROVED)
                ->get();

            // create CustomerRole
            $tableAccountId = CustomerService::createCustomerRole($weddingID, Role::STAGE_TABLE);
            // HANDLE DATA MAIN
            $channel = $this->getDefaultRoomWedding(ChannelName::MAIN, EventConstant::TYPE_STAGE, $weddingID, $tableAccountId);
            $id = \DB::table('channels')->insertGetId($channel);

            foreach ($customers as $customer) {
                $customer_channel = [
                    'channel_id'    => $id,
                    'is_host'       => Common::STATUS_TRUE,
                    'is_guest'      => Common::STATUS_FALSE,
                    'customer_id'   => $customer->id,
                    'status'        => Common::STATUS_TRUE,
                    'created_at'    => \Carbon\Carbon::now(),
                    'updated_at'    => \Carbon\Carbon::now(),
                ];

                \DB::table('customer_channel')->insert($customer_channel);
            }
        }
    }

    /**
     * Create Channel Test
     * @param $wedding_id
     */
    public function createChannelTest($weddingID)
    {
        $checkChannel = \DB::table('channels')
            ->where('name', 'like', ChannelName::SPEECH . '%')
            ->where('wedding_id', $weddingID)
            ->exists();
        if (!$checkChannel) {
            $customers = \DB::table('customers')
                ->where('customers.wedding_id', $weddingID)
                ->whereIn('customers.role', [Role::GUEST, Role::OBSERVER])
                ->where('customers.join_status', CustomerConstant::JOIN_STATUS_APPROVED)
                ->get();

            // create CustomerRole
            $tableAccountId = CustomerService::createCustomerRole($weddingID, Role::SPEECH_TABLE);
            // HANDLE DATA TEST
            $channel = $this->getDefaultRoomWedding(ChannelName::SPEECH, EventConstant::TYPE_TEST, $weddingID, $tableAccountId);
            $id = \DB::table('channels')->insertGetId($channel);

            foreach ($customers as $customer) {
                $customer_channel = [
                    'channel_id'    => $id,
                    'is_host'       => Common::STATUS_TRUE,
                    'is_guest'      => Common::STATUS_FALSE,
                    'customer_id'   => $customer->id,
                    'status'        => Common::STATUS_TRUE,
                    'created_at'    => \Carbon\Carbon::now(),
                    'updated_at'    => \Carbon\Carbon::now(),
                ];

                \DB::table('customer_channel')->insert($customer_channel);
            }
        }
    }

    /**
     * List Data Couple For Mail
     * @param $weddingID & $who
     * @return $data
     * **/
    protected function coupleNameForMail($weddingID, $who)
    {
        $data = Customer::where('wedding_id', $weddingID)
            ->when(isset($who), function($q) use($who){
                $q->where('role', $who);
            })
            ->when(!isset($who), function($q){
                $q->where(function($q){
                    $q->where('role', Role::GROOM);
                    $q->orWhere('role', Role::BRIDE);
                });
            });

        if($who == null){
            $data = $data->get();
        }else{
            $data = $data->first();
        }

        return $data;
    }
}
