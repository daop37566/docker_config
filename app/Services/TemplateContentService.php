<?php
namespace App\Services;

use App\Repositories\TemplateContentRepository;
use App\Repositories\EventRepository;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Constants\TemplateContentConstant;
use Str;

class TemplateContentService
{
    protected $templateContentRepo;
    protected $weddingRepo;

    public function __construct(
        TemplateContentRepository $templateContentRepo,
        EventRepository $weddingRepo
    ){
        $this->templateContentRepo = $templateContentRepo;
        $this->weddingRepo = $weddingRepo;
    }

    /**
     * Get template content list
     * @param $weddingId
     * @return $data
     * **/
    public function getTemplateContents($weddingID)
    {
        $allowRemote = $this->weddingRepo->model->find($weddingID)->allow_remote;

        $data = $this->templateContentRepo
            ->model
            ->where('status', STATUS_TRUE)
            ->when($allowRemote == STATUS_FALSE || empty($allowRemote), function($q){
                $q->where('id', '<>', TemplateContentConstant::END_CONTENT);
            })
            ->select([
                'id', 'content', 'status'
            ])
            ->get();

        return $data;
    }

    /**
     * Store template content file
     * @param $file
     * @return $linkS3Thumbnail
     * **/
    public function storeTemplateContentFile($file)
    {
        $linkS3Thumbnail = null;
        if ($file){
            $nameDirectory = 'templatecontent/';
            $fullName = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $nameFile = Str::random(10) . '_' . $fullName;
            $imgThumb = Image::make($file)->fit(300)->stream();
            $linkS3Thumbnail = Storage::disk('s3')->put(
                $nameDirectory.'thumbnail_' . $nameFile,
                $imgThumb->__toString(),
            );
            $linkS3Thumbnail = $nameDirectory.'thumbnail_' . $nameFile;
        }

        return $linkS3Thumbnail;
    }

    /**
     * Create template content
     * @param $file, $requestData
     * @return $data
     * **/
    public function createTemplateContent($file, $requestData)
    {
        $linkImage = $this->storeTemplateContentFile($file);
        $requestData['preview_image'] = $linkImage;

        return $this->templateContentRepo->model->create($requestData);
    }
}