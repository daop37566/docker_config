<?php
namespace App\Services;

use App\Repositories\WeddingCardRepository;
use App\Repositories\EventRepository;
use App\Repositories\CustomerRepository;
use App\Repositories\BankAccountRepository;
use App\Jobs\SendDoneCardToStaffJob;
use App\Jobs\StaffNotifyCoupleJob;
use App\Jobs\SendMail10Job;
use App\Jobs\NotifyToGuestStaffEditGuestJob;
use App\Jobs\NotifyCoupleStaffAddGuestJob;
use App\Jobs\NotifyGuestWillNotJoinJob;
use Illuminate\Support\Facades\Storage;
use App\Constants\Role;
use App\Constants\InviteSend;
use App\Constants\CustomerConstant;
use App\Constants\NotifyPlannerConstant;
use App\Traits\EmailTrait;
use DB;

class WeddingCardService
{
    use EmailTrait;

    protected $weddingCardRepo;
    protected $weddingRepo;
    protected $customerRepo;
    protected $bankAccountRepo;

    public function __construct(
        WeddingCardRepository $weddingCardRepo,
        EventRepository $weddingRepo,
        CustomerRepository $customerRepo,
        BankAccountRepository $bankAccountRepo
    ){
        $this->weddingCardRepo = $weddingCardRepo;
        $this->weddingRepo = $weddingRepo;
        $this->customerRepo = $customerRepo;
        $this->bankAccountRepo = $bankAccountRepo;
    }

    /**
     * Create wedding card
     * @param $cardData, $weddingId
     * @return boolean
     * **/
    public function createWeddingCard($cardData, $weddingId)
    {
        $wedding = $this->weddingRepo->model->find($weddingId);
        $weddingCard = $wedding->weddingCard();

        $weddingCard = $weddingCard->updateOrCreate(
            ['wedding_id' => $weddingId],
            $cardData
        );

        return true;
    }

    /**
     * Update card content
     * @param $cardContent, $weddingId
     * @return $cardContent
     * **/
    public function updateCardContent($cardContent, $weddingId)
    {
        $this->weddingCardRepo
             ->model
             ->where('wedding_id', $weddingId)
             ->update($cardContent);

        return $cardContent;
    }

    /**
     * Get presigned
     * @param $request
     * @return $data
     * **/
    public function getPreSigned($request)
    {
        $file_paths = null;
        $pre_signed = null;

        $file_info = $request->file_couple;
        $file = explode('.', $file_info);
        $extensionFile = $file[1];
        $nameFile = $file[0];
        $client = Storage::disk('s3')->getDriver()->getAdapter()->getClient();
        $fileName = \Str::random(10) . '_' . $file_info;
        $filePath = 'couple/' . $fileName;

        $command = $client->getCommand('PutObject', [
            'Bucket' => config('filesystems.disks.s3.bucket'),
            'Key' => $filePath,
        ]);

        $request = $client->createPresignedRequest($command, '+20 minutes');

        $filePathArray = [
            'image_path' => $filePath,
            'image_name' => $fileName,
            'full_link_image' => Storage::disk('s3')->url($filePath),
            'extension' =>  $extensionFile,
        ];

        $preSignedArray = [
            'pre_signed' => (string) $request->getUri()
        ];

        $file_paths = $filePathArray;
        $pre_signed = $preSignedArray;

        return  [
            'couple_pre_signeds' => $pre_signed,
            'file_path' => $file_paths
        ];
    }

    /**
     * Show wedding card
     * @param $weddingID
     * @return $data
     * **/
    public function showWeddingCard($weddingId)
    {
        $data = null;
        try {
            $data = $this->weddingCardRepo
                ->model
                ->where('wedding_id', $weddingId)
                ->with(['bankAccounts', 'templateCard', 'wedding'])
                ->first();
        } catch (\Exception $th) {
            throw $th;
        }

        return $data;
    }

    /**
     * @param int $guestID, $weddingID
     * return $data
     * **/
    public function previewWeddingCard($guestID, $weddingID)
    {
        $data = $this->weddingCardRepo->model
            ->where('wedding_id', $weddingID)
            ->select('id', 'couple_photo', 'content', 'wedding_id', 'wedding_price', 'template_card_id')
            ->with(['wedding' => function($q) use($guestID){
                $q->select(
                    'id',
                    'date',
                    'ceremony_time',
                    'ceremony_reception_time',
                    'party_time',
                    'party_reception_time',
                    'place_id',
                    'guest_invitation_response_date'
                );
                $q->with(['place' => function($q){
                    $q->select('id', 'name', 'restaurant_id');
                    $q->with(['restaurant' => function($q){
                        $q->select(
                            'id',
                            'address_1',
                            'address_2',
                            'phone',
                            'link_place'
                        );
                    }]);
                }]);
                $q->with(['customers' => function($q) use($guestID){
                    $q->where('id', $guestID)->select('id', 'full_name', 'wedding_id', 'email');
                    $q->with(['customerInfo' => function($q){
                        $q->select(
                            'id',
                            'first_name',
                            'last_name',
                            'customer_id',
                            'address',
                            'post_code',
                            'task_content',
                            'free_word',
                            'bank_account_id',
                        );
                        $q->with('bankAccount');
                    }]);
                }]);
            }])
            ->with('templateCard')
            ->first();

        return $data;
    }

    /**
     * Notify to staff admin
     * @param $weddingID
     * @return boolean
     * **/
    public function notifyToStaff($weddingID)
    {
        $exits = $this->weddingCardRepo->model
            ->where('wedding_id', $weddingID)
            ->exists();

        if($exits){
            $wedding = $this->weddingRepo->model->find($weddingID);
            $place = $wedding->place()->first();
            $restaurant = $place->restaurant()->first();
            $staff = $restaurant->user()->first();
            $customers = $wedding->customers()
                ->where('role', Role::GROOM)
                ->select('full_name')
                ->first();

            $wedding->update([
                'is_notify_planner' => NotifyPlannerConstant::SENT,
                'is_admin_approve' => STATUS_FALSE
            ]);

            $staffEmail = $staff->email;
            $contentEmail = [
                'contact_name' => $restaurant->contact_name,
                'groom_name' => $customers->full_name,
                'app_url' => config('app.app_url'),
            ];

            $sendEmailJob = new SendDoneCardToStaffJob($staffEmail, $contentEmail);
            dispatch($sendEmailJob);

            return true;
        }

        return false;
    }

    /**
     * GUEST UI - Guest get detail wedding card
     * @param $token
     * return detail card
     * **/
    public function guestGetWeddingCard($token)
    {
        $data = $this->customerRepo->model
            ->where('token', $token)
            ->select('id', 'full_name', 'wedding_id', 'email', 'join_status', 'phone')
            ->with(['wedding' => function($q){
                $q->select(
                    'id',
                    'date',
                    'ceremony_time',
                    'ceremony_reception_time',
                    'party_time',
                    'party_reception_time',
                    'place_id',
                    'guest_invitation_response_date'
                );
                $q->with(['weddingCard' => function($q){
                    $q->select('id', 'wedding_id', 'couple_photo', 'content', 'wedding_price', 'template_card_id');
                    $q->with('templateCard');
                }]);
                $q->with(['place' => function($q){
                    $q->select('id', 'name', 'restaurant_id');
                    $q->with(['restaurant' => function($q){
                        $q->select('id', 'address_1', 'address_2', 'phone', 'link_place');
                    }]);
                }]);
            }])
            ->with(['customerInfo' => function($q){
                $q->select(
                    'id',
                    'first_name',
                    'last_name',
                    'customer_id',
                    'address',
                    'post_code',
                    'task_content',
                    'free_word',
                    'bank_account_id',
                );
                $q->with('bankAccount');
            }])
            ->first();

        $guestRelatives = $this->customerRepo->model
            ->where('relative_id', $data->id)
            ->select('id', 'full_name', 'email', 'join_status')
            ->with(['customerInfo' => function($q){
                $q->select(
                    'id',
                    'customer_id',
                    'first_name',
                    'last_name',
                    'post_code',
                    'address',
                    'phone'
                );
            }])
            ->get();

        return [
            'data' => $data,
            'guest_relatives' => $guestRelatives
        ];
    }

    /**
     * Staff get wedding card
     * @param $guestID
     * @return $data
     * **/
    public function staffGetWeddingCard($guestID)
    {
        $guest = $this->customerRepo->model
            ->where('id', $guestID)
            ->select('id', 'full_name', 'email', 'wedding_id')
            ->with(['customerInfo' => function($q){
                $q->select(
                    'id', 'customer_id',
                    'post_code', 'phone', 'address',
                    'first_name', 'last_name', 'free_word',
                    'task_content'
                );
            }])
            ->first();

        $wedding = $guest->wedding()
            ->select(
                'id', 'thank_you_message', 'greeting_message',
                'date', 'ceremony_reception_time', 'ceremony_time',
                'party_reception_time', 'party_time', 'place_id'
            )
            ->first();

        $place = $wedding->place()
            ->select('id', 'name', 'restaurant_id')
            ->with(['restaurant' => function($q){
                $q->select('id', 'address_1', 'address_2', 'phone');
            }])
            ->first();

        $weddingCard = $wedding->weddingCard()
            ->select('id', 'wedding_price', 'content', 'couple_photo', 'template_card_id')
            ->first();

        $templateCard = $weddingCard->templateCard()->select('card_path')->first();

        $couple = $wedding->customers()
            ->where('role', Role::GROOM)
            ->orWhere('role', Role::BRIDE)
            ->select('full_name')
            ->get();

        return [
            'guest' => $guest,
            'wedding' => $wedding,
            'wedding_card' => $weddingCard,
            'template_card' => $templateCard,
            'place' => $place,
            'couple_name' => $couple
        ];
    }

    /**
     * @param $guestInfo
     * implement UI Guest Response Wedding Card
     * **/
    public function editResponseWebCard($guestInfo, $freeWord)
    {
        $guest = $this->customerRepo->model->find($guestInfo['id']);

        $guest->update([
            'full_name' => $guestInfo['first_name'] . " " . $guestInfo['last_name'],
            'email' => $guestInfo['email'],
            'join_status' => $guestInfo['join_status']
        ]);

        $guest->customerInfo()->updateOrCreate(
            ['customer_id' => $guest->id],
            [
                'first_name' => $guestInfo['first_name'],
                'last_name' => $guestInfo['last_name'],
                'post_code' => $guestInfo['post_code'] ?? null,
                'address' => $guestInfo['address'] ?? null,
                'phone' => $guestInfo['phone'] ?? null,
                'guest_free_word' => $freeWord ?? null
            ]
        );
    }

    /**
     * UI Guest - Guest Response Wedding Card
     *
     * @param  Request  $mainGuest, $relativeGuest
     * @return bool
     */
    public function guestResponseWeddingCard($mainGuest, $relativeGuest)
    {
        DB::beginTransaction();
        try {
            $joinStatus = $this->customerRepo->model
                ->where('token', $mainGuest['token'])
                ->whereNotNull('join_status');

            if(!$joinStatus->exists()){
                $weddingID = $this->customerRepo->model
                    ->where('token', $mainGuest['token'])->first()->wedding_id;
                $mainGuest['id'] = $this->customerRepo->model
                    ->where('token', $mainGuest['token'])->first()->id;
                $freeWord = $mainGuest['free_word'] ?? null;

                $guest = [collect($mainGuest)];
                $guests = array_merge($guest, $relativeGuest);

                #Update guest
                foreach($guests as $guest){
                    $this->editResponseWebCard($guest, $freeWord);
                }

                #Notify to each guest
                $groomName = $this->coupleNameForMail($weddingID, Role::GROOM)->full_name;
                $brideName = $this->coupleNameForMail($weddingID, Role::BRIDE)->full_name;
                $dataFooter = $this->dataFooterRestaurant($weddingID);
                $index = 0;
                foreach($guests as $guest)
                {
                    $guestName =  $guest['first_name']. " " . $guest['last_name'];
                    $guests[$index]['full_name'] = $guestName;
                    $notifyJob = null;
                    if(isset($guest['email'])){
                        if($guest['join_status'] == CustomerConstant::JOIN_STATUS_JOIN_OFFLINE){
                            #Send mail 10
                            $emailContent = [
                                'guest_name' => $guestName,
                                'url' => env('USER_URL'),
                                'data_footer' => $this->dataFooterRestaurant($weddingID)
                            ];
                            $subject = "$groomName 様、$brideName 様ご結婚式　ご出席のご連絡ありがとうございます";
                            $notifyJob = new SendMail10Job($guest['email'], $emailContent, $subject);
                        }
                        else if($guest['join_status'] == CustomerConstant::JOIN_STATUS_APPROVED){
                            $guestInfo = $this->customerRepo->model->find($guest['id'])->customerInfo()->first();
                            if(isset($guestInfo->bank_account_id)){ #Send mail 11a
                                $bankAccount = $this->bankAccountRepo->model->whereId($guestInfo->bank_account_id)->first();
                                $weddingCard = $this->weddingCardRepo->model->where('wedding_id', $weddingID)->first();
                                $wedding = $this->weddingRepo->model->find($weddingID);
                                $emailContent = [
                                    'guest_name' => $guestName,
                                    'bank_account' => $bankAccount,
                                    'wedding_price' => $weddingCard->wedding_price,
                                    'guest_invitation_response_date' => $wedding->guest_invitation_response_date,
                                    'data_footer' => $dataFooter
                                ];

                                $notifyJob = new NotifyToGuestStaffEditGuestJob(
                                    "$groomName 様、$brideName 様ご結婚式　ご出席のご連絡ありがとうございます", //Email subject,
                                    "notify_to_guest_11a", //TemplateName,
                                    $guest['email'], //Send to
                                    $emailContent //Email content
                                );
                            }else{ #Send mail 11b
                                $emailContent = [
                                    'guest_name' => $guestName,
                                    'url' => env('USER_URL'),
                                    'data_footer' => $dataFooter
                                ];

                                $notifyJob = new NotifyToGuestStaffEditGuestJob(
                                    "$groomName 様、$brideName 様ご結婚式　ご出席のご連絡ありがとうございます", //Email subject,, //Email subject,
                                    "notify_to_guest_11b", //TemplateName,
                                    $guest['email'], //Send to
                                    $emailContent //Email content
                                );
                            }
                        }
                        //Choise will not join
                        else if($guest['join_status'] == CustomerConstant::JOIN_STATUS_CANCEL){
                            $subject = "$groomName 様、$brideName 様ご結婚式に関して";
                            $emailContent = [
                                'guest_name' => $guestName,
                                'data_footer' => $dataFooter,
                                'url' => env('USER_URL')
                            ];
                            $notifyJob = new NotifyGuestWillNotJoinJob($subject, $guest['email'], $emailContent);
                        }
                    }
                    dispatch($notifyJob);
                    $index++;
                }
                #Convert join status to Japanese text
                for($i = 0; $i < count($guests); $i++){
                    $guests[$i]['join_status'] = $this->joinStatusText($guests[$i]['join_status']);
                }
                #Notify to couple
                $couple = $this->coupleNameForMail($weddingID, NULL);
                foreach($couple as $value){
                    $emailContent = [
                        'groom_name' => $this->coupleNameForMail($weddingID, Role::GROOM)->full_name,
                        'bride_name' => $this->coupleNameForMail($weddingID, Role::BRIDE)->full_name,
                        'guests' => $guests,
                        'url' => env('USER_URL'),
                        'data_footer' => $this->dataFooterRestaurant($weddingID)
                    ];

                    $notify = new NotifyCoupleStaffAddGuestJob($value['email'], $emailContent);
                    dispatch($notify);
                }

                DB::commit();
                return true;
            }

            DB::rollback();
            return false;
        } catch (\Exception $th) {
            DB::rollback();
            throw $th;
        }
    }

    /**
     * UI STAFF - STAFF UPDATE STATUS CARD
     * @param $request data
     * @return boolean
     * **/
    public function updateStatus($data)
    {
        $wedding = $this->weddingRepo->model->whereId($data['id']);
        # Update is notify planner status
        $wedding->update([
            'is_notify_planner' => $data['status']
        ]);
        # Staff admin check
        if($data['status'] == NotifyPlannerConstant::CHECKED) #3
        {
            # Notify to couple
            $coupleData = $this->coupleNameForMail($data['id'], null);
            foreach($coupleData as $item)
            {
                $mailData = [
                    'groom_name' => $this->coupleNameForMail($data['id'], Role::GROOM)->full_name ?? "undefine",
                    'bride_name' => $this->coupleNameForMail($data['id'], Role::BRIDE)->full_name ?? "undefine",
                    'pic_name' => $this->weddingRepo->model->find($data['id'])->pic_name ?? "undefine",
                    'url' => env('USER_URL'),
                    'data_footer' => $this->dataFooterRestaurant($data['id'])
                ];
                $staffNotifyCoupleJob = new StaffNotifyCoupleJob($item['email'], $mailData);
                dispatch($staffNotifyCoupleJob);
            }
            # Update isAdminApprove
            $wedding->update(['is_admin_approve' => STATUS_TRUE]);
        }
        else if($data['status'] == NotifyPlannerConstant::UNLOCK) #2
        {
            $wedding->update(['is_admin_approve' => STATUS_FALSE]);
        }

        return true;
    }
}
