<?php
namespace App\Services;

use App\Models\User;
use App\Constants\Role;
use App\Models\Company;
use App\Models\Restaurant;
use App\Models\Customer;
use App\Jobs\SendMailResetPasswordJob;
use App\Jobs\SendMailInviteStaffJob;
use Mail;
use Hash;
use Str;
use Carbon\Carbon;
use App\Constants\Common;

class UserService
{
    /**
     * Get all
     * @param $request
     * @return $list
     * **/
    public function getAllByRestaurant($request)
    {
        $orderBy = isset($request['order_by']) ? explode('|', $request['order_by']) : [];
        $keyword = !empty($request['keyword']) ? escape_like($request['keyword']) : null;
        $paginate = !empty($request['paginate']) ? $request['paginate'] : Common::PAGINATE;

        $staffs = User::staff()
            ->when(count($orderBy) > 1, function($q) use ($orderBy) {
                $q->orderBy($orderBy[0], $orderBy[1]);
            })
            ->when(!empty($keyword), function($q) use ($keyword) {
                $q->where(function($q) use ($keyword){
                    $q->where('email', 'like', '%'.$keyword.'%')
                        ->orWhereHas('restaurant', function($q) use ($keyword) {
                            $q->where('name', 'like', '%'.$keyword.'%')
                                ->orWhere('company_name', 'like', '%'.$keyword.'%');
                        });
                });
            })
            ->where(function($q) {
                $q->whereHas('company' ,function($q){
                    $q->whereIsActive(Common::STATUS_TRUE);
                })->orWhere('company_id', null);
            })
            ->with(['company' => function($q){
                $q->select('id', 'name', 'description');
            }])
            ->with(['restaurant' => function($q){
                $q->select('id', 'name', 'company_name');
            }])
            ->orderBy('created_at', 'desc');

            if($paginate != Common::PAGINATE_ALL){
               $staffs = $staffs->paginate($paginate);
            } else {
                $staffs = $staffs->get();
            }
        return $staffs;
    }

    /**
     * Get detail staff
     * @param $staff_id
     * @return $detail
     * **/
    public function getStaff($staff_id)
    {
        return $this->findDetail($staff_id);
    }

    /**
     * Destroy staff
     * @param $staff_id
     * @return $status
     * **/
    public function destroyStaff($staff_id)
    {
        $user = User::staff()->find($staff_id);
        if ($user){
            User::whereId($staff_id)
                ->with(['restaurant' => function($q){
                    $q->with(['places' => function($q){
                        $q->with(['weddings' => function($q){
                            $q->with(['customers' => function($q){
                                $q->delete();
                            }]);
                        }]);
                    }]);
                }])
                ->first();

            return $user->delete();
        }

        return null;
    }

    /**
     * Check exist user
     * @param $id
     * @return boolean
     * **/
    public function existUser($id)
    {
        $user = User::staff()->where('id', $id)->exists();
        if($user){
            return true;
        }

        return false;
    }

    /**
     * Create remember mail
     * @param $email, $token
     * @return void
     * **/
    public function createRememberMail($email, $token)
    {
        User::where('email', $email)
            ->update([
                'remember_token' => $token,
                'email_at' => Carbon::now()
            ]);
    }

    /**
     * Check expired token
     * @param $token
     * @return boolean
     * **/
    public function checkExpiredToken($token){
        $startTime = User::where('remember_token', $token)->get('email_at')
                            ->first()
                            ->email_at;
        $endTime = Carbon::parse($startTime)->addHours(1);
        if(Carbon::now() < $endTime){
            return true;
        }

        return false;
    }

    /**
     * Check exist token
     * @param $token
     * @return boolean
     * **/
    public function checkExistToken($token)
    {
        if(User::where('remember_token', $token)){
            return true;
        }

        return false;
    }

    /**
     * Change password
     * @param $token, $password
     * @return boolean
     * **/
    public function changePassword($token, $password)
    {
        if($this->checkExpiredToken($token)){
            return User::where('remember_token', $token)
                        ->update([
                            'password' => $password,
                            'remember_token' => null
                        ]);
        }

        return false;
    }

    /**
     * Update password login
     * @param $password, $email
     * @return $status
     * **/
    public function updatePasswordLogin($password, $email)
    {
        return User::where('email', $email)
                ->update([
                    'password' => $password,
                    'remember_token' => null
                ]);
    }

    /**
     * Update password verify
     * @param @oldPass, $userPass, $newPass, $email
     * @return boolean
     * **/
    public function updatePasswordVerify($oldPass, $userPass, $newPass, $email)
    {
        if(Hash::check($oldPass, $userPass)){
            return $this->updatePasswordLogin($newPass, $email);
        }

        return false;
    }

    /**
     * Send email to reset staff / super admin password
     * @param $email
     * @return boolean
     * **/
    public function sendMailToReset($email)
    {
        $resetPasswordJob = null;
        $templateName = null;
        $token = Str::random(100);
        $user = User::where('email', $email)->first();
        $emailInfo = [
            'token' => $token,
            'app_url' => env('ADMIN_URL')
        ];

        if($user->role == Role::SUPER_ADMIN){
            $templateName = "reset_super_admin_password";
        }else if($user->role == Role::STAFF_ADMIN){
            $emailInfo['contact_name'] = $user->restaurant()->first()->contact_name ?? "No name";
            $templateName = "reset_staff_admin_password";
        }

        $this->createRememberMail($email, $token);
        $resetPasswordJob = new SendMailResetPasswordJob($email, $emailInfo, $templateName);
        dispatch($resetPasswordJob);

        return true;
    }

    /**
     * Find detail
     * @param $id
     * @return $user
     * **/
    public function findDetail($id)
    {
        $user = User::whereId($id)
            ->with(['company', 'restaurant'])
            ->first();

        if ($user) return $user;

        return null;
    }

    /**
     * Get me customer
     * @param $id
     * @return $detail
     * **/
    public function getMeCustomer($id)
    {
        return Customer::whereId($id)->first();
    }

    /**
     * Invite new admin staff
     * @param $email
     * @return boolean
     * **/
    public function inviteNewAdminStaff($email)
    {
        $email = Str::lower($email);
        $token = Str::random(100);
        $emailInfo = [
            'app_url' => env('ADMIN_URL'),
            'token' => $token
        ];

        User::create([
            'email' => $email,
            'remember_token' => $token,
            'role' => Role::STAFF_ADMIN,
            'username' => random_str(20),
            'password' => random_str(200)
        ]);

        $inviteStaffJob = new SendMailInviteStaffJob($email, $emailInfo);
        dispatch($inviteStaffJob);

        return true;
    }

    /**
     * Check belong to restaurant
     * @param $id
     * @return boolean
     * **/
    public function checkBelongToRestaurant($id)
    {
        $user = User::where('id', $id)
                    ->get('restaurant_id')
                    ->first()->restaurant_id;
        if($user !== null){
            return true;
        }

        return false;
    }

    /**
     * Staff admin info update
     * @param $data
     * @return $userId
     * **/
    public function staffAdminInfoUpdate($data, $userId)
    {
        $user = User::find($userId);
        if(!$user) return false;

        $dataRestaurant = [
            'name' => $data['restaurant_name'],
            'phone' => $data['phone'],
            'contact_name' => $data['contact_name'],
            'contact_email' => Str::lower($data['contact_email']),
            'post_code' => $data['post_code'],
            'address_1' => $data['address_1'],
            'address_2' => $data['address_2'],
            'company_name' => $data['company_name'],
            'guest_invitation_response_num' => $data['guest_invitation_response_num'],
            'couple_edit_num' => $data['couple_edit_num'],
            'link_place' => $data['link_place'] ?? null
        ];

        /*
        | Check user is belong to restaurant ???
        | If user IS NOT belong to restaurant, we CREATE new restaurant
        | Else we UPDATE restaurant info where user belong to
        */

        if(!$this->checkBelongToRestaurant($userId)){
            $restaurant = Restaurant::create($dataRestaurant);
            $user->restaurant_id = $restaurant->id;
            $user->save();
        }else{
            $user->restaurant()->update($dataRestaurant);
        }

        /*
        | If STAFF_ADMIN update info, we look it IS FIRST LOGIN
        */

        if($user->role === Role::STAFF_ADMIN){
            $user->update([
                'is_first_login' => Common::STATUS_TRUE
            ]);
        }

        return $this->findDetail($userId);
    }
}
