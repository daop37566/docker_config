<?php
namespace App\Services;

use App\Constants\ChannelName;
use Illuminate\Support\Facades\Auth;
use App\Repositories\CustomerRepository;
use App\Repositories\WeddingCardRepository;
use App\Repositories\EventRepository;
use App\Repositories\BankAccountRepository;
use App\Constants\Common;
use App\Constants\CustomerConstant;
use App\Constants\Role;
use App\Models\Customer;
use App\Jobs\ResetCouplePasswordJob;
use App\Traits\EmailTrait;
use App\Traits\TableTrait;
use Exception;
use Faker\Factory;
use App\Constants\InviteSend;
use App\Libs\Agora\RtcTokenBuilder;
use App\Models\CustomerTable;
use App\Models\TablePosition;
use Str;
use DB;
use Faker\Factory as Faker;

class CustomerService
{
    use EmailTrait, TableTrait;

    protected $customerRepo;
    protected $weddingCardRepo;
    protected $weddingRepo;
    protected $bankAccountRepo;

    public function __construct(
        CustomerRepository $customerRepo,
        WeddingCardRepository $weddingCardRepo,
        EventRepository $weddingRepo,
        BankAccountRepository $bankAccountRepo
    ){
        $this->customerRepo = $customerRepo;
        $this->weddingCardRepo = $weddingCardRepo;
        $this->weddingRepo = $weddingRepo;
        $this->bankAccountRepo = $bankAccountRepo;
    }

    /**
     * Get list customer in wedding
     * @param $data
     * @return $list
     * **/
    public function getListCustomerInWedding(array $data)
    {
        $orderBy = isset($data['order_by']) ? explode('|', $data['order_by']) : [];
        $keyword = !empty($data['keyword']) ? escape_like($data['keyword']) : null;
        $paginate = !empty($data['paginate']) ? $data['paginate'] : Common::PAGINATE;
        $auth = Auth::guard('customer')->user();
        $tablePositionId = $data['table_position_id'] ?? null;
        $roleWeddingIds = [Role::GUEST, Role::BRIDE, Role::GROOM];
        $roleTableIds = [Role::STAGE_TABLE, Role::COUPE_TABLE, Role::SPEECH_TABLE, Role::NORMAL_TABLE];
        $guestPositionTableId = Auth::guard('customer')->user()->tablePosition->first()->id ?? null;
        $getList = $this->customerRepo->model
            ->when(isset($auth->role) && in_array($auth->role, $roleWeddingIds), function($q) use ($auth) {
                $q->whereHas('wedding', function($q) use ($auth){
                    $q->whereId($auth->wedding_id)
                        ->where(function($q) {
                            $q->where('is_close', Common::STATUS_FALSE)
                                ->orWhere('is_close', null);
                        });

                });
            })
            // ->when(isset($auth->role) && in_array($auth->role, $roleTableIds), function($q) use ($auth) {
            //     $q->whereHas('wedding', function($q) use ($auth){
            //         $q->where('place_id', $auth->place_id)
            //             ->where('is_close', Common::STATUS_FALSE);
            //     });
            // });
            ->when(isset($data['in_table']) && isset($guestPositionTableId),function($q) use ($guestPositionTableId) {
                $q->whereHas('tablePosition', function($q) use ($guestPositionTableId){
                    $q->whereId($guestPositionTableId);
                })
                ->whereRole(Role::GUEST);
            })
            ->when(!empty($tablePositionId), function($q) use ($tablePositionId) {
                $q->whereHas('tablePosition', function($q) use ($tablePositionId){
                    $q->whereId($tablePositionId);
                });
            });


        if($paginate != Common::PAGINATE_ALL){
            $getList = $getList->paginate($paginate);
        } else {
            if(isset($data['in_table']) && $guestPositionTableId == null){
                $getList = collect([]);
            } else {
                $getList = $getList->get();
            }
        }

        return $getList;
    }

    /**
     * UI COUPLE EVENT DETAIL - LIST GUEST - [U063]
     * @param $weddingID, $request
     * **/
    public function staffCoupleGetListGuest($weddingID, $request)
    {
        $keyword = (isset($request['keyword'])) ? escape_like($request['keyword']) : NULL;
        $orderBy = (isset($request['order_by'])) ? explode('|', $request['order_by']) : [];
        $status = [];

        if(!empty($keyword)){
            $status = getArrayIndex($keyword, CustomerConstant::RESPONSE_CARD_STATUS);
        }

        $customerParticipant =  $this->customerRepo->model
            ->where(function($q) use($weddingID, $keyword){
                $q->where('wedding_id', $weddingID);
                $q->where('role', Role::GUEST);
            })
            ->where(function($q) use($keyword, $status){
                $q->orWhere('full_name', 'like', '%'.$keyword.'%');
                $q->orWhere('email', 'like', '%'.$keyword.'%');
                $q->orWhere(function($q) use($keyword){
                    $q->whereHas('tablePosition', function($q) use($keyword){
                        $q->where('position', 'like', '%'.$keyword.'%');
                    });
                });
                $q->orWhere(function($q) use($keyword){
                    $q->whereHas('customerInfo', function($q) use($keyword){
                        $q->where('relationship_couple', 'like', '%'.$keyword.'%');
                    });
                });
                $q->orWhereIn('join_status', $status);
            })
            ->select(['id', 'full_name', 'email', 'join_status', 'order', 'invitation_url', 'is_newest', 'token'])
            ->with(['tablePosition' => function($q){
                $q->select(['id', 'position']);
            }])
            ->with(['customerInfo' => function($q){
                $q->select('id', 'relationship_couple', 'customer_id');
            }])
            ->when(count($orderBy) > 0, function ($q) use($orderBy){
                return $q->orderBy($orderBy[0], $orderBy[1]);
            })
            ->get();

        $wedding = $this->weddingRepo->model->where('id', $weddingID)
            ->select('guest_invitation_response_date', 'couple_edit_date', 'is_notify_planner')
            ->first();

        $tableList = $this->weddingRepo->model->find($weddingID)
            ->place()->first()
            ->tablePositions()->get();

        return [
            'guest_invitation_response_date' => $wedding->guest_invitation_response_date,
            'couple_edit_date' => $wedding->couple_edit_date,
            'is_notify_planner' => $wedding->is_notify_planner,
            'customer_participant' => $customerParticipant,
            'table_list' => $tableList,
            'couter' => $this->guestJoinCounter($weddingID)
        ];
    }

    /**
     * Count guest status join
     * @param $weddingID
     * return array of counter
     * **/
    public function guestJoinCounter($weddingID)
    {
        $guest = $this->customerRepo->model
            ->where('wedding_id', $weddingID)
            ->where('role', Role::GUEST)
            ->get();

        $counter = [
            'sent' => $this->customerRepo->model
                ->where('wedding_id', $weddingID)
                ->where('role', Role::GUEST)
                ->whereNotNull('invitation_url')
                ->whereHas('customerInfo', function($q){
                    $q->where('is_send_wedding_card', STATUS_TRUE);
                })
                ->count(),
            'join_cancel' => $guest->whereNotNull('join_status')->where('join_status', CustomerConstant::JOIN_STATUS_CANCEL)->count(),
            'join_offline' => $guest->where('join_status', CustomerConstant::JOIN_STATUS_JOIN_OFFLINE)->count(),
            'join_remote' => $guest->where('join_status', CustomerConstant::JOIN_STATUS_APPROVED)->count(),
            'wait_confirm' => $guest->whereNull('join_status')->count()
        ];

        return $counter;
    }

    /**
     * Get Bank ID
     * @param $bankOrder, $weddingID
     * @return $bankAccountID
     * **/
    public function getBankID(int $bankOrder, int $weddingId)
    {
        $bankAccountId = null;
        if($bankOrder != 0){
            $weddingCard = $this->weddingCardRepo
                ->model
                ->where('wedding_id', $weddingId)
                ->whereHas('bankAccounts', function($q) use($bankOrder){
                    $q->where('bank_order', $bankOrder);
                })
                ->first();

            $bankAccount = $weddingCard->bankAccounts()
                ->where('wedding_card_id', $weddingCard->id)
                ->where('bank_order', $bankOrder)
                ->first();

            $bankAccountId = $bankAccount->id;
        }

        return $bankAccountId;
    }

    /**
     * Transmission status get email_status
     * @param $isSendWeddingCard, $email
     * @return $emailSTATUS
     * **/
    public function transmissionStatus($isSendWeddingCard, $email)
    {
        $emailStatus = InviteSend::UNSEND;
        if($isSendWeddingCard && !isset($email)){
            $emailStatus = InviteSend::NOT_EMAIL;
        }else if(!$isSendWeddingCard){
            $emailStatus = InviteSend::DO_NOT_SEND;
        }

        return $emailStatus;
    }

    /**
     * Get max current order
     * of Weding
     * @return int order
     * **/
    public function maxOrderCurrent($weddingID)
    {
        $curentOrder = $this->customerRepo->model
            ->where('wedding_id', $weddingID)
            ->where('role', Role::GUEST)
            ->max('order');

        return $curentOrder;
    }

    /**
     * Couple Create Guest
     * @return boolean
     * **/
    public function createParticipant($requestData, $weddingID)
    {
        $email = (isset($requestData['email'])) ? Str::lower($requestData['email']) : null;
        $emailStatus = $this->transmissionStatus(
            $requestData['is_send_wedding_card'],
            $email,
        );
        $username = random_str_az(8) . random_str_number(4);
        $password = random_str_az(8) . random_str_number(4);
        $fullname = $requestData['first_name'] . " " . $requestData['last_name'];
        $bankID = $this->getBankID($requestData['bank_order'], $weddingID);

        $customer = $this->customerRepo->create([
            'username' => $username,
            'password' => $password,
            'email' => $email,
            'role' => Role::GUEST,
            'full_name' => $fullname,
            'wedding_id' => $weddingID,
            'order' => $this->maxOrderCurrent($weddingID) + 1,
            'token' => Str::random(CustomerConstant::TOKEN_LENGHT)
        ]);

        $customerInfo = $customer->customerInfo()->create([
            'is_only_party' => $requestData['is_only_party'],
            'first_name' => $requestData['first_name'],
            'last_name' => $requestData['last_name'],
            'relationship_couple' => $requestData['relationship_couple'],
            'post_code' => $requestData['post_code'],
            'address' => $requestData['address'],
            'phone' => $requestData['phone'],
            'free_word' => $requestData['free_word'],
            'task_content' => $requestData['task_content'],
            'is_send_wedding_card' => $requestData['is_send_wedding_card'],
            'customer_type' => $requestData['customer_type'],
            'bank_account_id' => $bankID,
            'email_status' => $emailStatus
        ]);

        if(
            isset($requestData['customer_relatives']) &&
            count($requestData['customer_relatives']) > 0
        ){
            foreach ($requestData['customer_relatives'] as $relGuest) {
                $relGuestItem = $this->customerRepo->model->create([
                    'username' => random_str_az(8) . random_str_number(4),
                    'password' => random_str_az(8) . random_str_number(4),
                    'full_name' => $relGuest['first_name'] . " " . $relGuest['last_name'],
                    'order' => $this->maxOrderCurrent($weddingID) + 1,
                    'wedding_id' => $weddingID,
                    'relative_id' => $customer->id,
                    'role' => Role::GUEST,
                    'token' => Str::random(CustomerConstant::TOKEN_LENGHT)
                ]);

                $relGuestItem->customerInfo()->create([
                    'first_name' => $relGuest['first_name'],
                    'last_name' => $relGuest['last_name'],
                    'relationship_couple' => $relGuest['relationship_couple'],
                    'bank_account_id' => $bankID
                ]);
            }
        }

        return true;
    }

    /**
     * UI Staff Registry Guest Participant
     * @return boolean
     * **/
    public function staffCreateGuest($requestData)
    {
        $email = (isset($requestData['email'])) ? Str::lower($requestData['email']) : null;
        $username = random_str_az(8) . random_str_number(4);
        $password = random_str_az(8) . random_str_number(4);
        $fullname = $requestData['first_name'] . " " . $requestData['last_name'];

        $customer = $this->customerRepo->create([
            'username' => $username,
            'password' => $password,
            'email' => $email ?? null,
            'role' => Role::GUEST,
            'full_name' => $fullname,
            'wedding_id' => $requestData['wedding_id'],
            'order' => $this->maxOrderCurrent($requestData['wedding_id']) + 1,
            'join_status' => $requestData['join_status'],
            'token' => Str::random(CustomerConstant::TOKEN_LENGHT)
        ]);

        $customerInfo = $customer->customerInfo()->create([
            'first_name' => $requestData['first_name'],
            'last_name' => $requestData['last_name'],
            'relationship_couple' => $requestData['relationship_couple'],
            'post_code' => $requestData['post_code'] ?? null,
            'address' => $requestData['address'] ?? null,
            'phone' => $requestData['phone'] ?? null
        ]);

        #Join this guest to table
        if(
            isset($requestData['table_position_id']) AND
            $requestData['join_status'] != CustomerConstant::JOIN_STATUS_CANCEL
        ){
            $this->customerJoinTable([
                'id' => $customer->id,
                'table_position_id' => $requestData['table_position_id']
            ]);
            // Delete Channel Normal
            $param = [
                'wedding_id' => $requestData['wedding_id']
            ];
            // Check if channel exists
            $checkChannel = \DB::table('channels')
            ->where('wedding_id', $param['wedding_id'])
            ->exists();

            if($checkChannel){
                // Delete Channel Normal
                ChannelService::deleteChannelNotExists($param['wedding_id']);
                // Create Channel Normal
                ChannelService::createChannelTableNomal($param);
            }
        }

        #Notify To Couple
        $this->notifyToCouple($customer->wedding_id, $requestData);

        return true;
    }

    /**
     * Get detail support email
     * **/
    public function getDetailForGuestEmail(int $weddingID, $guest)
    {
        $data = $this->weddingRepo->model->find($weddingID)
            ->with(['customers' => function($q){
                $q->select('id', 'wedding_id', 'full_name', 'role')
                    ->where(function($q){
                        $q->where('role', Role::GROOM);
                        $q->orWhere('role', Role::BRIDE);
                    })->get();
            }])
            ->with(['weddingCard' => function($q){
                $q->select('id', 'wedding_id', 'wedding_price')->first();
                $q->with(['bankAccounts' => function($q){
                    $q->select('*')->get();
                }]);
            }])
            ->with(['place' => function($q){
                $q->select('id', 'name', 'restaurant_id');
                $q->with(['restaurant' => function($q){
                    $q->select('id', 'name', 'address_1', 'address_2', 'contact_email', 'phone')
                        ->first();
                }]);
            }])
            ->select(
                'id', 'party_time', 'party_reception_time',
                'ceremony_time', 'ceremony_reception_time',
                'date', 'place_id', 'guest_invitation_response_date'
            )
            ->first()
            ->toArray();

        $data['full_name'] = $guest['first_name'] . " " . $guest['last_name'];

        return $data;
    }

    /**
     * Get list guest participant
     * @param $data, $weddingID
     * @return $data
     * **/
    public function getListParticipant($data, $weddingId)
    {
        $participants = $this->customerRepo->model
            ->where('wedding_id', $weddingId)
            ->where('role', Role::GUEST)
            ->with(['customerInfo' => function($q){
                $q->select(
                    'id', 'first_name', 'last_name', 'is_send_wedding_card',
                    'is_only_party', 'customer_id', 'email_status', 'relationship_couple', 'guest_free_word'
                );
            }])
            ->leftJoin(DB::raw("
                (SELECT count(id) as customer_relatives_count, relative_id
                FROM customers
                WHERE relative_id is not null
                GROUP BY relative_id) as guestRela"),
                function($join){
                    $join->on('guestRela.relative_id', '=', 'id');
                }
            )
            ->whereNull('customers.relative_id')
            ->select(
                'id',
                'full_name',
                'email',
                'invitation_url',
                'customer_relatives_count',
                'send_card_status',
                'join_status',
                'invitation_url',
                'token',
                'is_newest',
            );

        $wedding = $this->weddingRepo
            ->model
            ->where('id', $weddingId)
            ->select('guest_invitation_response_date', 'couple_edit_date', 'is_notify_planner')
            ->first();

        return [
            'participants' => (isset($data['paginate']))
                ? $participants->paginate($data['paginate'])
                : $participants->get(),
            'guest_invitation_response_date' => $wedding->guest_invitation_response_date,
            'couple_edit_date' => $wedding->couple_edit_date,
            'is_notify_planner' => $wedding->is_notify_planner
        ];
    }

    /**
     * Delete one guest
     * @param $id, $weddingID
     * @return boolean
     * **/
    public function deleteOneGuest($id, $weddingId)
    {
        $participant = $this->customerRepo
            ->model
            ->where('id', $id)
            ->where('role', Role::GUEST)
            ->where('wedding_id', $weddingId)
            ->first();

        if($participant){
            $order = $participant->order;
            $maxOrder = $this->customerRepo->model->max('order');
            for($i = $order + 1; $i <= $maxOrder ; $i++){
                $this->customerRepo->model
                    ->where('wedding_id', $weddingId)
                    ->where('order', $i)
                    ->update(['order' => $i - 1]);
            }

            $participant->delete();
            return true;
        }

        return false;
    }

    /**
     * Delete multiple guest
     * @param $ids, $weddingID
     * @return boolean
     * **/
    public function deleteMultipleGuest($IDs, $weddingID)
    {
        for($i = 0; $i < count($IDs); $i++){
            $this->deleteOneGuest($IDs[$i], $weddingID);
        }

        return true;
    }

    /**
     * Main delete guest
     * @param $mainGuestID
     * return list of guest id (main guest id, relatives guest id)
     * **/
    public function deleteGuests($mainGuestID, $weddingID)
    {
        $idList = [];
        $mainGuest = $this->customerRepo->model
            ->where('id', $mainGuestID)
            ->where('role', Role::GUEST)
            ->where('wedding_id', $weddingID)
            ->first();

        if($mainGuest){
            $idList = [$mainGuest->id];
            $relGuest = $this->customerRepo->model
                ->where('relative_id', $mainGuest->id)
                ->select('id')
                ->get();

            for($index = 0; $index < count($relGuest); $index++){
                $idList[$index + 1] = $relGuest[$index]['id'];
            }

            $this->deleteMultipleGuest($idList, $weddingID);
            return true;
        }

        return false;
    }

    /**
     * Update guest participant info
     * @param $data, $weddingId
     * @return boolean
     * **/
    public function updateParticipantInfo($data, $weddingId)
    {
        $email = (isset($data['email'])) ? Str::lower($data['email']) : null;
        $customer = $this->customerRepo->model->find($data['id']);
        $bankId = $this->getBankID($data['bank_order'], $weddingId);
        $emailStatus = $this->transmissionStatus(
            $data['is_send_wedding_card'],
            $data['email'],
        );

        $customer->update([
            'email' => $email,
            'full_name' => $data['first_name'] . " " . $data['last_name'],
        ]);

        $customer->customerInfo()->updateOrCreate(
            ['customer_id' => $customer->id],
            [
                'is_only_party' => $data['is_only_party'],
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'relationship_couple' => $data['relationship_couple'],
                'post_code' => $data['post_code'],
                'address' => $data['address'],
                'phone' => $data['phone'],
                'customer_type' => $data['customer_type'],
                'task_content' => $data['task_content'],
                'free_word' => $data['free_word'],
                'is_send_wedding_card' => $data['is_send_wedding_card'],
                'email_status' => $emailStatus,
                'bank_account_id' => $bankId
            ]
        );

        foreach ($data['customer_relatives'] as $value)
        {
            $relGuestID = (isset($value['id'])) ? $value['id'] : null;
            $relItem = $this->customerRepo->model->where('id', $relGuestID);

            if($relItem->exists()){
                $relItem->update([
                    'full_name' => $value['first_name'] . " " . $value['last_name']
                ]);
                $relItem->first()->customerInfo()->update([
                    'first_name' => $value['first_name'],
                    'last_name' => $value['last_name'],
                    'relationship_couple' => $value['relationship_couple']
                ]);
            }else{
                $guestRel = $this->customerRepo->model->create([
                    'username' => random_str_az(8) . random_str_az(4),
                    'password' => random_str_az(8) . random_str_az(4),
                    'role' => Role::GUEST,
                    'full_name' => $value['first_name'] . " " . $value['last_name'],
                    'wedding_id' => $weddingId,
                    'order' => $this->maxOrderCurrent($weddingId) + 1,
                    'relative_id' => $customer->id
                ]);
                $guestRel->customerInfo()->create([
                    'first_name' => $value['first_name'],
                    'last_name' => $value['last_name'],
                    'relationship_couple' => $value['relationship_couple']
                ]);
            }
        }

        return true;
    }

    /**
     * Get detail guest participant
     * @param $id, $weddingID, $customerID
     * @return boolean
     * **/
    public function detailParticipant($id, $weddingId, $customerId)
    {
        $participant = $this->customerRepo->model
            ->where('id', $id)
            ->where('wedding_id', $weddingId)
            ->where('role', Role::GUEST)
            ->select('id', 'email', 'wedding_id', 'join_status', 'relative_id', 'is_newest', 'invitation_url')
            ->first();

        $isNotifyPlanner = $this->weddingRepo->model->find($weddingId)->is_notify_planner ?? null;

        if(isset($participant) && is_numeric($id)){
            $participantInfo = $participant->customerInfo()
                ->select(
                    'id', 'is_only_party', 'first_name', 'last_name', 'relationship_couple',
                    'phone', 'post_code', 'address', 'customer_type', 'task_content',
                    'free_word', 'is_send_wedding_card', 'bank_account_id'
                )
                ->first();

            $bankAccount = $this->bankAccountRepo->model
                ->find($participantInfo->bank_account_id);

            if(isset($bankAccount)){
                $participantInfo['bank_order'] = $bankAccount->bank_order;
            }else{
                $participantInfo['bank_order'] = 0;
            }

            $guestRelatives = $this->customerRepo->model
                ->where('relative_id', $participant->id)
                ->select('id', 'full_name')
                ->with(['customerInfo' => function($q){
                    $q->select('id', 'customer_id', 'first_name', 'last_name', 'relationship_couple');
                }])
                ->get();

            return [
                'participant' => $participant,
                'participant_info' => $participantInfo,
                'participant_relatives' => $guestRelatives,
                'is_notify_planner' => $isNotifyPlanner
            ];
        }

        return false;
    }

    /**
     * UI STAFF - [AS170]
     * @param $id of guest participant id
     * **/
    public function staffGetParticipant($id)
    {
        $participant = $this->customerRepo->model
            ->where('id', $id)
            ->where('role', Role::GUEST)
            ->select('id', 'email', 'join_status', 'wedding_id', 'full_name')
            ->with(['tablePosition' => function($q){
                $q->select('id', 'position')->first();
            }])
            ->first();

        $participantInfo = $participant->customerInfo()
            ->select(
                'id', 'is_only_party', 'first_name', 'last_name', 'relationship_couple',
                'phone', 'post_code', 'address', 'task_content'
            )
            ->first();

        $participantRelatives = $this->customerRepo->model
            ->where('role', Role::GUEST)
            ->where('relative_id', $participant->id)
            ->select('id', 'full_name')
            ->with(['customerInfo' => function($q){
                $q->select('id', 'customer_id', 'first_name', 'last_name', 'relationship_couple');
            }])
            ->get();

        $weddingCardContent = $this->weddingCardRepo->model
            ->where('wedding_id', $participant->wedding_id)
            ->select('id', 'content')
            ->first();

        return [
            'participant' => $participant,
            'participant_info' => $participantInfo,
            'participant_relative' => $participantRelatives,
            'wedding_card_content' => $weddingCardContent,
            'count_sent' => $this->countSendCardStatus($participant->wedding_id)
        ];
    }

    /**
     * UI COUPLE - [U064]
     * @param $requestData as $data
     * @return boolean
     * **/
    public function coupleUpdateGuestInfo($data)
    {
        $email = (isset($data['email'])) ? Str::lower($data['email']) : null;
        $guest = $this->customerRepo->model->find($data['id']);
        $guestInfo = $guest->customerInfo()->first();
        $guestName = $data['first_name'] . " " . $data['last_name'];

        #Notify to COUPLE, GUEST
        $this->notifyWhenUpdateGuest($guest, $guestInfo, $guestName, $data);

        #Update guest info
        $guest->update([
            'email' => $email,
            'full_name' => $data['first_name'] . " " . $data['last_name'],
            'join_status' => $data['join_status'],
        ]);
        # Update guest info
        $guest->customerInfo()->update([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'relationship_couple' => $data['relationship_couple'],
            'post_code' => $data['post_code'] ?? null,
            'phone' => $data['phone'] ?? null,
            'address' => $data['address'] ?? null
        ]);
        # Delete guest out of table when max (remote, join cancel, join offline)
        # Amount guest offline in table
        $amountGuestOffline = $this->amountGuestWithJoinStatus(
            CustomerConstant::JOIN_STATUS_JOIN_OFFLINE,
            $guest->tablePosition()->first()->id ?? null,
            $guest->wedding_id ?? null
        );
        # Amount  guest online in table
        $amountGuestOnline = $this->amountGuestWithJoinStatus(
            CustomerConstant::JOIN_STATUS_APPROVED,
            $guest->tablePosition()->first()->id ?? null,
            $guest->wedding_id ?? null
        );
        # Amount chair in table
        $amountChair = $guest->tablePosition()->first()->amount_chair ?? 0;
        # If join cancel or max chair
        if(
            $data['join_status'] == CustomerConstant::JOIN_STATUS_CANCEL OR
            ($amountGuestOffline >= $amountChair AND $data['join_status'] == CustomerConstant::JOIN_STATUS_JOIN_OFFLINE) OR
            ($amountGuestOnline >= Common::MAX_ONLINE_TABLE AND $data['join_status'] == CustomerConstant::JOIN_STATUS_APPROVED)
        ){
            $guest->tablePosition()->detach();
        }

        return true;
    }

    /**
     * Join Guest To Table
     * @param @guestID, $tableID, $joinStatus
     * @return void
     * **/
    public function joinGuestToTable($guestID, $tableID, $joinStatus)
    {
        $guest = $this->customerRepo->model->find($guestID);
        if(
            isset($tableID) AND
            $joinStatus != CustomerConstant::JOIN_STATUS_CANCEL
        ){
            $guest->tablePosition()->sync([
                $tableID => ['status' => STATUS_TRUE]
            ]);
        }
        else
        {
            $guest->tablePosition()->detach();
        }
    }

    /**
     * Staff Edit Guest Info
     * @param $requestData as $data
     * @return boolean
     * **/
    public function staffUpdateGuestInfo($data)
    {
        $email = (isset($data['email'])) ? Str::lower($data['email']) : null;
        $guest = $this->customerRepo->model->find($data['id']);
        $guestInfo = $guest->customerInfo()->first();
        $guestName = $data['first_name'] . " " . $data['last_name'];

        #Notify to COUPLE, GUEST
        $this->notifyWhenUpdateGuest($guest, $guestInfo, $guestName, $data);

        $guest->update([
            'email' => $email,
            'full_name' => $guestName,
            'join_status' => $data['join_status'],
        ]);

        $guest->customerInfo()->update([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'relationship_couple' => $data['relationship_couple'],
            'post_code' => $data['post_code'] ?? null,
            'phone' => $data['phone'] ?? null,
            'address' => $data['address'] ?? null
        ]);

        #Join guest table
        $this->joinGuestToTable(
            $data['id'],
            $data['table_position_id'] ?? null,
            $data['join_status']
        );
        // id table_position guest current
        $current_table_position_id = $guest->tablePosition->first()->id ?? null;

        if($current_table_position_id OR ($data['join_status'] == CustomerConstant::JOIN_STATUS_CANCEL)){
             // Delete Channel Normal
            $param = [
                'wedding_id' => $data['wedding_id']
            ];
            // Check if channel exists
            $checkChannel = \DB::table('channels')
            ->where('wedding_id', $param['wedding_id'])
            ->exists();

            if($checkChannel){
                // Delete Channel Normal
                ChannelService::deleteChannelNotExists($param['wedding_id']);
                // Create Channel Nomal
                ChannelService::createChannelTableNomal($param);
            }
        }

        return true;
    }

    /**
     * UI COUPLE - [U063] Couple Event Detail
     * @param $request
     * **/
    public function customerJoinTable($data)
    {
        $guestID = $data['id'];
        $tableID = $data['table_position_id'];

        $this->customerRepo->model
            ->find($guestID)
            ->tablePosition()
            ->sync([$tableID => ['status' => STATUS_TRUE]]);

        return true;
    }

     /* UI COUPLE - [U063.1] reorder
     * @param $request
     * **/
    public function reoderGuest($weddingID, $requestData)
    {
        DB::beginTransaction();
        try {
            $customerID = $requestData['id'];
            $currentPosition = $this->customerRepo->model->find($customerID)->order;
            $updatedPosition = $requestData['updated_position'];
            $customer = $this->customerRepo->model;

            if($currentPosition < $updatedPosition){
                for($i = $currentPosition + 1; $i <= $updatedPosition; $i++){
                    $customer::where('wedding_id', $weddingID)
                        ->where('order', $i)
                        ->update(['order' => $i - 1]);
                }
            }else{
                for($i = $currentPosition; $i >= $updatedPosition; $i--){
                    $customer::where('wedding_id', $weddingID)
                        ->where('order', $i)
                        ->update(['order' => $i + 1]);
                }
            }
            $customer::where('id', $customerID)->update(['order' => $updatedPosition]);
            DB::commit();
            return true;
        } catch (\Exception $th) {
            DB::rollback();
            throw $th;
        }
    }

    /**
     * Dump data customer
     * @param $weddingId, $role, $quantity, $tablePosition
     * @return boolean
     * **/
    public function dumpDataCustomer($weddingId, $role = Role::NORMAL_TABLE, $quantity = 1, $tablePositionId = NULL) : bool
    {
        try {
            $arrayAccept = [
                Role::GROOM,
                Role::BRIDE,
                Role::GUEST,
                Role::STAGE_TABLE,
                Role::COUPE_TABLE,
                Role::SPEECH_TABLE,
                Role::NORMAL_TABLE,
                Role::OBSERVER,
            ];
            if(in_array($role, $arrayAccept)) {
                $faker = Factory::create();
                //Create NORMAL_TABLE account
                for($i = 0; $i < $quantity ; $i++) {
                    $timestamp = \Carbon\Carbon::now()->timestamp;
                    $customerId = Customer::create([
                        'username' => $faker->unique()->userName.'-'.$timestamp,
                        'full_name' => $faker->name,
                        'phone' => $faker->phoneNumber(),
                        'address' => $faker->address(),
                        'email' => $faker->email,
                        'token' => random_str_az(8),
                        'password' => $faker->regexify('[A-Za-z0-9]{6}'),
                        'role' => $role,
                        'join_status' => CustomerConstant::JOIN_STATUS_APPROVED,
                        "wedding_id" => $weddingId,
                    ]);
                    // Create position customer_table
                    if($role == Role::GUEST && isset($tablePositionId)){
                        \DB::table('customer_table')->insert([
                            'customer_id'           => $customerId->id,
                            'table_position_id'     => $tablePositionId,
                            'chair_name'            => $faker->name,
                            'status'                => '1'
                        ]);
                    }
                }
                return true;
            }
            throw new Exception('Role not exists!', 400);
        } catch (\Exception $th) {
            throw $th;
        }
    }

    /**
     * UI STAFF - Button reset couple password (orange)
     * return message
     * **/
    public function resetCouplePassword($weddingID)
    {
        DB::beginTransaction();
        try {
            $couple = $this->customerRepo->model
                ->where('wedding_id', $weddingID)
                ->where(function($q){
                    $q->orWhere('role', Role::GROOM);
                    $q->orWhere('role', Role::BRIDE);
                })
                ->select('id', 'email', 'username', 'password')
                ->get();

            foreach($couple as $couple)
            {
                $password = random_str_az(8) . random_str_number(4);
                $groomName = $this->coupleNameForMail($weddingID, Role::GROOM)->full_name;
                $brideName = $this->coupleNameForMail($weddingID, Role::BRIDE)->full_name;

                $emailInfo = [
                    'username' => $couple['username'],
                    'password' => $password,
                    'groom_name' => $groomName,
                    'bride_name' => $brideName,
                    'url' => env('USER_URL'),
                    'pic_name' => $this->weddingRepo->model->find($weddingID)->pic_name,
                    'data_footer' => $this->dataFooterRestaurant($weddingID)
                ];

                $this->customerRepo->model
                    ->where('id', $couple['id'])
                    ->update([
                        'password' => $password
                    ]);

                $resetPasswordJob = new ResetCouplePasswordJob($couple['email'], $emailInfo);
                dispatch($resetPasswordJob);

                //Destroy couple token
                $couple->customerTokens()->delete();
            }

            DB::commit();
            return true;
        } catch (\Exception $th) {
            DB::rollback();
            throw $th;
        }
    }

    /**
     * Count Guest Send Card
     * return int $count
     * **/
    public function countSendCardStatus($weddingID){
        $total = $this->customerRepo->model
            ->where('role', Role::GUEST)
            ->where('wedding_id', $weddingID)
            ->whereHas('customerInfo', function($q){
                $q->where('email_status', InviteSend::SENT);
            })
            ->count();

        return $total;
    }

    /**
     * UI STAFF - Staff update guest content
     * return message
     * **/
    public function updateGuestContent($data)
    {
        #return $this->countSentStatus(1);
        $guest = $this->customerRepo->model->find($data['id']);
        $guest->customerInfo()
            ->update([
                'task_content' => $data['task_content']
            ]);

        if($this->countSendCardStatus($guest->wedding_id) < 1){
            $guest->wedding()->first()->weddingCard()->update([
                'content' => $data['content']
            ]);
        }

        return true;
    }

    /**
     * Custom data list guest
     * @param $list
     * @return $list
     * **/
    public function customData($data)
    {
        $list = [];
        for($i = 0 ; $i < count($data); $i++){
            $postCode = $data[$i]->post_code;
            $list[$i]['first_name_1'] = $data[$i]->first_name;
            $list[$i]['last_name_1'] = $data[$i]->last_name;
            $list[$i]['first_name_2'] = "";
            $list[$i]['last_name_2'] = "";
            $list[$i]['post_code'] = "'$postCode'";
            $list[$i]['address_1'] = $data[$i]->address;
            $list[$i]['address_2'] = "";
            $list[$i]['relationship_couple_1'] = $data[$i]->relationship_couple;
            $list[$i]['relationship_couple_2'] = "";
            $list[$i]['relationship_couple_3'] = "";
            $list[$i]['relationship_couple_4'] = "";
        }

        return $list;
    }

    /**
     * Export data to download all guest csv
     * @param $weddingID
     * @return array data
     * **/
    public function dataGuestCSV($weddingID, $type)
    {
        $fileName = null;
        $guestList = $this->customerRepo->model
            ->where('wedding_id', $weddingID)
            ->where('role', Role::GUEST)
            ->leftJoin('customer_infos', 'customer_infos.customer_id', '=', 'customers.id')
            ->select(
                'first_name',
                'last_name',
                'post_code',
                'customer_infos.address',
                'relationship_couple'
            )
            ->orderBy('order', 'asc');

        if($type == "all"){
            $guestList = $guestList->get();
            $fileName = "参加者リスト" . ".csv";
        }else{
            $guestList = $guestList->whereNull('relative_id')->get();
            $fileName = "招待状リスト" . ".csv";
        }
        $customeList = $this->customData($guestList);

        return [
            'list' => $customeList,
            'file_name' => $fileName
        ];
    }

    /**
     * UPDATE IS_SPEECH FLAG
     * @param Request $speech_status
     * return messages
     * **/
    public function updateSpeechFlag($data)
    {
        $this->customerRepo->model
            ->whereId($data['guest_id'])
            ->update([
                'is_speech' => $data['is_speech']
            ]);

        return true;
    }

    /**
     * Create customer ROLE
     * Return tableAccountId
     *
     */
    static public function createCustomerRole($wedding_id, $role, $tablePositionId = null, $email = NULL)
    {
        $faker = Faker::create();
        if(isset($tablePositionId)){
            $full_name = TablePosition::find($tablePositionId)->position;
        }else{
            switch ($role)
            {
                case Role::STAGE_TABLE :
                    $full_name = ChannelName::MAIN;
                    break;
                case Role::COUPE_TABLE :
                    $full_name = ChannelName::COUPLE;
                    break;
                case Role::SPEECH_TABLE :
                    $full_name = ChannelName::SPEECH;
                    break;
                default:
                    $full_name = 'OBSERVER';
                    break;
            }
        }

        $tableAccount = Customer::create([
            'username'      => random_str_az(8) . random_str_number(4),
            'full_name'     => $full_name,
            'email'         => null,
            'join_status'   => CustomerConstant::JOIN_STATUS_APPROVED,
            'token'         => $faker->regexify('[A-Za-z0-9]{20}'),
            'password'      => $faker->regexify('[A-Za-z0-9]{6}'),
            'role'          => $role,
            'wedding_id'    => $wedding_id,
        ]);

        if ($role == Role::GUEST) {
            CustomerTable::create([
                'customer_id' => $tableAccount->id,
                'table_position_id' => $tablePositionId,
                'chair_name' => $faker->name,
                'status' => 1
            ]);
        }

        $tableAccountId = $tableAccount->id ?? null;
        return $tableAccountId;
    }

    /**
     * Store customer token to DB
     * @param $customerId, $token
     * @return boolean
     * **/
    public function updateCustomerToken($customerID, $token)
    {
        $customer = $this->customerRepo->model->find($customerID);
        $customer->customerTokens()->updateOrCreate(
            ['customer_id' => $customerID],
            ['token' => $token]
        );

        return true;
    }

}
