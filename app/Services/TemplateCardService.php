<?php
namespace App\Services;

use App\Repositories\TemplateCardRepository;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Str;

class TemplateCardService
{

    protected $templateCardRepo;

    public function __construct(TemplateCardRepository $templateCardRepo)
    {
        $this->templateCardRepo = $templateCardRepo;
    }

    /**
     * Get template card
     * @param $type
     * @return $data
     * **/
    public function getTemplateCards($type)
    {
        $data = $this->templateCardRepo
                     ->model
                     ->where('type', $type)
                     ->select(['id', 'name', 'card_path', 'card_thumb_path', 'type'])
                     ->get();

        return $data;
    }

    /**
     * Store file wedding template card
     * @param $name, $fileBig, $fileThumb
     * @return $data
     * **/
    public function storeFileWeddingTemplateCard($name, $fileBig, $fileThumb)
    {
        $basePath = "template_cards/" . $name . "/";
        $nameFileBig = "big";
        $nameFileThumb = "thumb";
        $diskS3 = Storage::disk('s3');
        if($fileBig && $fileThumb)
        {
            # Name file
            $nameFileBig = $nameFileBig . "." . $fileBig->getClientOriginalExtension();
            $nameFileThumb = $nameFileThumb . "." . $fileThumb->getClientOriginalExtension();
            $bigFilePath = $basePath . $nameFileBig;
            $thumbFilePath = $basePath . $nameFileThumb;
            # Upload and get link
            $diskS3->put($bigFilePath, file_get_contents($fileBig));
            $diskS3->put($thumbFilePath, file_get_contents($fileThumb));
        }

        return [
            'card_path' => $bigFilePath,
            'card_thumb_path' => $thumbFilePath,
        ];
    }

    /**
     * Create template card
     * @param @requestData, $fileBig, $fileThumb
     * @return $data
     * **/
    public function createTemplateCard($requestData, $fileBig, $fileThumb)
    {
        $imageLink = $this->storeFileWeddingTemplateCard($requestData['name'], $fileBig, $fileThumb);
        $requestData = array_merge($requestData, $imageLink);

        return $this->templateCardRepo->model->create($requestData);
    }

}