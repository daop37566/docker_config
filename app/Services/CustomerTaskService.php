<?php
namespace App\Services;

use App\Repositories\CustomerTaskRepository;

class CustomerTaskService
{
    protected $customerTaskRepo;

    public function __construct(CustomerTaskRepository $customerTaskRepo)
    {
        $this->customerTaskRepo = $customerTaskRepo;
    }

    /**
     * Get list customer task
     * @param null
     * @return $list
     * **/
    public function getListCustomerTask()
    {
        return $this->customerTaskRepo->model->all();
    }

    /**
     * Create customer task
     * @param $data
     * @return $data
     * **/
    public function createCustomerTask($data)
    {
        return $this->customerTaskRepo->model->create($data);
    }
}