<?php
namespace App\Services;

use App\Repositories\PolicyRepository;
use App\Constants\PolicyConstants;
use App\Constants\Role;
use Storage;
use Str;
use DB;

class PolicyService
{
    protected $policyRepo;

    public function __construct(PolicyRepository $policyRepo)
    {
        $this->policyRepo = $policyRepo;
    }

    /**
     * GET presigned url for upload file to S3
     * @param $request
     * @return array data path
     * **/
    public function getPreSigned($request)
    {
        $file_paths = null;
        $pre_signed = null;

        $file_info = $request['file_policy'];
        $file = explode('.', $file_info);
        $extensionFile = $file[1];
        $nameFile = $file[0];
        $client = Storage::disk('s3')->getDriver()->getAdapter()->getClient();
        $fileName = Str::random(10) . '_' . $file_info;
        $filePath = 'policy/' . $fileName;

        $command = $client->getCommand('PutObject', [
            'Bucket' => config('filesystems.disks.s3.bucket'),
            'Key' => $filePath,
        ]);

        $request = $client->createPresignedRequest($command, '+20 minutes');

        $filePathArray = [
            'file_path' => $filePath,
            'file_name' => $fileName,
            'full_link_file' => Storage::disk('s3')->url($filePath),
            'extension' =>  $extensionFile,
        ];

        $preSignedArray = [
            'pre_signed' => (string) $request->getUri()
        ];

        $file_paths = $filePathArray;
        $pre_signed = $preSignedArray;

        return  [
            'policy_pre_signeds' => $pre_signed,
            'file_path' => $file_paths
        ];
    }

    /**
     * STORE link policy file to DB
     * @param $requestData as $data
     * @return boolean
     * **/
    public function store($data)
    {
        $this->policyRepo->model->create($data);
        return true;
    }

    /**
     * Destroy (changes status policy)
     * @param $request['column'] and $request['value']
     * @return boolean
     * **/
    public function destroy($request)
    {
        $this->policyRepo->model->where($request['column'], $request['value'])->update([
            'status' => STATUS_FALSE
        ]);

        return true;
    }

    /**
     * Get list policy file
     * @return $list
     * **/
    public function list()
    {
        $list = $this->policyRepo->model
            ->selectRaw('max(policies.id) as id, type, status, file_path, name')
            ->where('status', STATUS_TRUE)
            ->groupBy('type', 'status', 'file_path', 'name')
            ->orderBy('type', 'asc')
            ->take(PolicyConstants::AMOUNT) //4
            ->get();

        return $list;
    }

    /**
     * Get newest policy by type
     * @param $type
     * @return $link
     * **/
    public function show($type)
    {
        $policyLink = $this->policyRepo->model
            ->select('file_path')
            ->where('status', STATUS_TRUE)
            ->where('type', $type)
            ->orderBy('created_at', 'desc')
            ->first();

        return $policyLink;
    }

    /**
     * Show policy by role
     * @param $role
     * @return $policy link
     * **/
    public function showByRole($role, $type)
    {
        $link = null;
        if($role == Role::STAFF_ADMIN && $type == PolicyConstants::USE_OF_PLACE)
        {
            $link = $this->show($type);
        }
        else if(in_array($role, [Role::GROOM, Role::BRIDE]) && $type == PolicyConstants::USE_OF_COUPLE)
        {
            $link = $this->show($type);
        }
        else if(in_array($type, [PolicyConstants::PRIVACY_PLACE, PolicyConstants::PRIVACY_COUPLE]))
        {
            $link = $this->show($type);
        }

        return $link;
    }
}
