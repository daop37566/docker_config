<?php
namespace App\Services;

use App\Jobs\SendEventEmailJob;
use App\Jobs\SendDoneSeatJob;
use App\Jobs\SendInviteCardToGuestJob;
use App\Constants\Role;
use App\Constants\EventConstant;
use App\Constants\NotifyPlannerConstant;
use App\Constants\CustomerConstant;
use App\Constants\InviteSend;
use App\Repositories\EventRepository;
use App\Repositories\CustomerRepository;
use App\Repositories\ChannelRepository;
use Carbon\Carbon;
use Auth;
use Str;
use DB;
use App\Constants\Common;
use App\Traits\EmailTrait;

class EventService
{
    use EmailTrait;

    protected $eventRepo;
    protected $customerRepo;
    protected $channelRepo;

    public function __construct(
        EventRepository $eventRepo,
        CustomerRepository $customerRepo,
        ChannelRepository $channelRepo
    ){
        $this->eventRepo = $eventRepo;
        $this->customerRepo = $customerRepo;
        $this->channelRepo = $channelRepo;
    }

    /**
     * Get event list
     * @param $request
     * @return $list
     * **/
    public function eventList($request)
    {
        $keyword = (isset($request['keyword'])) ? escape_like(toEngStringDate($request['keyword'])) : NULL;
        $orderBy = (isset($request['order_by'])) ? explode('|', $request['order_by']) : [];
        $paginate = (isset($request['paginate'])) ? $request['paginate'] : EventConstant::PAGINATE;

        // if(isJapaneseDate($keyword)){
        //     $keyword = Carbon::createFromFormat('Y年m月d日', $keyword)->format('Y-m-d');
        // }

        return $this->eventRepo->model
            ->with(['observer' => function($q) {
                $q->select('id', 'username', 'role', 'token', 'wedding_id');
            }])
            ->whereHas('place', function($q){
                $q->whereHas('restaurant.user', function($q){
                    $q->whereId(Auth::user()->id);
                });
            })
            ->when(isset($keyword), function($q) use($keyword){
                $q->whereHas('place', function($q) use($keyword){
                    $q->where("name", "like", '%'.$keyword.'%')
                        ->where('status', Common::STATUS_TRUE)
                        ->orWhere("title", "like", '%'.$keyword.'%');
                    $q->orWhere("pic_name", "like", '%'.$keyword.'%');
                    $q->orWhere('date', "like", '%'.$keyword.'%');
                });
            })
            ->when(count($orderBy) > 0, function ($q) use($orderBy){
                return $q->orderBy($orderBy[0], $orderBy[1]);
            })
            ->select(['id', 'title', 'pic_name', 'date', 'place_id', 'ceremony_reception_time', 'ceremony_time', 'party_reception_time', 'party_time', 'is_livestream', 'is_close'])
            ->with(['place' => function($q){
                $q->select('id', 'name');
            }])
            ->orderBy('date', 'desc')
            ->paginate($paginate);
    }

    /**
     * Update thank messages
     * @param $eventID, $data
     * @param boolean
     * **/
    public function updateThankMessage($eventId, $data)
    {
        $data = $this->eventRepo->model->where('id', $eventId)->update($data);

        if($data){
            return $data;
        }

        return false;
    }

    /**
     * Make couple
     * @param $couple, $weddingId
     * @return void
     * **/
    public function makeCouple($couple, $weddingEventId)
    {
        $item = [];
        $groomName = $couple[0]['full_name'];
        $birdeName = $couple[1]['full_name'];
        $picName = $this->eventRepo->model->find($weddingEventId)->pic_name;

        foreach ($couple as $value)
        {
            $username = random_str_az(8).random_str_number(4);
            $password = random_str_az(8).random_str_number(4);

            $coupleContent = [
                'username' => $username,
                'password' => $password,
                'email'    => $value['email'],
                'wedding_id' => $weddingEventId,
                'role' => $value['role'],
                'join_status' => CustomerConstant::JOIN_STATUS_APPROVED,
                'updated_at' => Carbon::now(),
                'created_at' => Carbon::now(),
                'full_name' => $value['full_name']
            ];
            array_push($item, $coupleContent);
            $sendEmailJob = new SendEventEmailJob($value['email'], [
                'groom_name' => $groomName,
                'bride_name' => $birdeName,
                'url' => env('USER_URL'),
                'username' => $username,
                'password' => $password,
                'pic_name' => $picName,
                'data_footer' => $this->dataFooterRestaurant($weddingEventId)
            ]);
            dispatch($sendEmailJob);
        }
        $this->customerRepo->model->insert($item);
    }

    /**
     * Create event
     * @param $data
     * @return $detail
     * **/
    public function createEvent($data)
    {
        $data['ceremony_reception_time'] = (isset($data['ceremony_reception_time']))
                                            ? implode('-', $data['ceremony_reception_time'])
                                            : null;
        $data['ceremony_time'] = implode('-', $data['ceremony_time']);
        $data['party_reception_time'] = (isset($data['party_reception_time']))
                                        ? implode('-', $data['party_reception_time'])
                                        : null;
        $data['party_time'] = (count($data['party_time']) > 1)
                              ? implode('-', $data['party_time'])
                              : $data['party_time'][0];

        $data['groom_email'] = Str::lower($data['groom_email']);
        $data['bride_email'] = Str::lower($data['bride_email']);

        $event = $this->eventRepo->model->create($data);
        #Make couple
        $couple = [
            [
                'email' => $data['groom_email'],
                'full_name' => $data['groom_name'],
                'role' => Role::GROOM
            ],
            [
                'email' => $data['bride_email'],
                'full_name' => $data['bride_name'],
                'role' => Role::BRIDE
            ]
        ];
        $this->makeCouple($couple, $event->id);

        return $this->detailEvent($event->id);
    }

    /**
     * Get detail event
     * @param $eventId
     * @return $data
     * **/
    public function detailEvent($eventId)
    {
        return $this->eventRepo->model->where('id', $eventId)
            ->with(['eventTimes', 'customers'])
            ->first();
    }

    /**
     * Couple get detail event
     * @param $weddingId, $coupleId
     * @return $data
     * **/
    public function coupleDetailEvent($weddingId, $coupleId)
    {
        $weddingDetail = $this->eventRepo->model
            ->where('id', $weddingId)->whereHas('customers', function($q) use($coupleId){
                $q->where('id', $coupleId);
            })
            ->select(
                'id', 'thank_you_message', 'greeting_message', 'date', 'pic_name',
                'ceremony_reception_time','ceremony_time', 'party_reception_time',
                'party_time', 'place_id', 'couple_edit_date', 'guest_invitation_response_date',
                'is_notify_planner', 'send_invite_card_status'
            )
            ->first();

        $weddingTimes = $weddingDetail->eventTimes()
            ->select('id', 'start', 'end', 'description')
            ->get();

        $place = $weddingDetail->place()
            ->select('id', 'restaurant_id', 'name', 'image')
            ->first();

        $restaurant = $place->restaurant()
            ->select('id', 'contact_name')
            ->first();

        $weddingDetail['table_map_image'] = $place->image;

        return [
            'wedding_detail' => $weddingDetail,
            'wedding_times' => $weddingTimes,
            'place_name' => $place->name,
            'contact_name' => $restaurant->contact_name
        ];
    }

    /*
    | Staff event detail for update UI[AS-150]
    | @param int $staffID
    | @param int $eventID
    */
    public function staffEventDetail($staffID, $weddingID)
    {
        $weddingDetail = $this->eventRepo->model
            ->where('id', $weddingID)
            ->whereHas('place.restaurant.user', function($q) use($staffID){
                $q->where('id', $staffID);
            })
            ->with(['place' => function($q){
                $q->select('id', 'name', 'image');
            }])
            ->with(['customers' => function($q){
                $q->where('role', Role::GROOM)
                  ->orWhere('role', Role::BRIDE)
                  ->select('full_name', 'email', 'role', 'wedding_id');
            }])
            ->first();

        return $weddingDetail;
    }

    /**
     * Status event detail
     *
     * @param int $staffID
     * @param int $eventID
     */
    public function statusEventDetail($staffID, $weddingID)
    {
        $statusWeddingDetail = $this->eventRepo->model
            ->where('id', $weddingID)
            ->whereHas('place.restaurant.user', function($q) use($staffID){
                $q->where('id', $staffID);
            })
            ->select(
                'is_livestream', 'is_join_table', 'speech_mode'
            )
            ->first();

        return $statusWeddingDetail;
    }
    /*
    | Staff event detail for update UI[AS-150]
    | @param int $staffID
    | @param int $eventID
    */
    public function observerEventDetail($observerID, $weddingID)
    {
        $weddingDetail = $this->eventRepo->model
            ->where('id', $weddingID)
            ->whereHas('customers', function($q) use($observerID){
                $q->where('id', $observerID);
            })
            ->select(
                'id', 'title', 'pic_name', 'date',
                'ceremony_reception_time', 'ceremony_time',
                'party_reception_time', 'party_time', 'table_map_image',
                'guest_invitation_response_date', 'couple_edit_date',
                'place_id'
            )
            ->with(['place' => function($q){
                $q->select('id', 'name');
            }])
            ->with(['customers' => function($q){
                $q->where('role', Role::GROOM)
                  ->orWhere('role', Role::BRIDE)
                  ->select('full_name', 'email', 'role', 'wedding_id');
            }])
            ->first();

        return $weddingDetail;
    }

    /**
     * Get detail event with bearerToken
     * @param $customerId
     * @return $data
     */
    public function getWeddingEventWithBearerToken($customerId)
    {
        $tablePosition = $this->customerRepo->model->where('id', $customerId)
            ->select('id', 'full_name')
            ->with(['tablePosition' => function($q){
                $q->select('id', 'position');
            }])->first();

        $weddingId = $this->customerRepo
            ->model
            ->where('id', $customerId)
            ->select('wedding_id')->first()->wedding_id;

        $data = $this->eventRepo->model->whereHas('customers', function($q) use($customerId){
                        $q->where('id', $customerId);
                    })
                    ->with(['place' => function($q) use($weddingId){
                        $q->select('id', 'name')
                          ->with(['tablePositions' => function($q) use($weddingId){
                                $q->select('place_id', 'id', 'position')
                                  ->where('status', Common::STATUS_TRUE)
                                  ->with(['customers' => function($q) use($weddingId){
                                        $q->select('full_name', 'id', 'join_status', 'is_speech')
                                          ->with(['customerInfo' => function($q){
                                              $q->select('id', 'customer_id', 'relationship_couple', 'first_name', 'last_name');
                                          }])
                                          ->where('role', Role::GUEST)
                                          ->where('wedding_id', $weddingId);
                            }]);
                        }]);
                    }, 'eventTimes' => function($q){
                        $q->select(['id', 'event_id', 'start', 'end', 'description']);
                    }])
                    ->first();

        $data['customer_detail'] = $tablePosition;
        $data['channel_table'] = null;
        // Get channel table
        $tablePositionId = Auth::guard('customer')->user()->tablePosition()->first()->id ?? null;
        $customerId =  Auth::guard('customer')->user()->id ?? null;
        if($weddingId) {
            $query = $this->channelRepo
                ->model
                ->whereWeddingId($weddingId);

            if($tablePositionId){
                $channel_table = $query->where(function($q) use ($tablePositionId){
                            $q->whereTablePositionId($tablePositionId);
                });
            }else{
                $channel_table = $query->where(function($q) use ($customerId) {
                            $q->where('customer_id', $customerId);
                });
            }
            $data['channel_table'] = $channel_table->with(['tableAccount' => function($q) {
                $q->select('id', 'full_name', 'username');
            }])
            ->first();
        }
        return $data;
    }

    /**
     * Update event
     * @param $id, $data
     * @return $detailEvent
     * **/
    public function updateEvent($id, $data)
    {
        $data['ceremony_reception_time'] = (isset($data['ceremony_reception_time']))
                                            ? implode('-', $data['ceremony_reception_time'])
                                            : null;
        $data['ceremony_time'] = implode('-', $data['ceremony_time']);
        $data['party_reception_time'] = (isset($data['party_reception_time']))
                                        ? implode('-', $data['party_reception_time'])
                                        : null;
        $data['party_time'] = (count($data['party_time']) > 1)
                              ? implode('-', $data['party_time'])
                              : $data['party_time'][0];

        $event = $this->eventRepo->model->find($id);
        $event->update([
            'title' => $data['title'],
            'pic_name' => $data['pic_name'],
            'ceremony_reception_time' => $data['ceremony_reception_time'],
            'ceremony_time' => $data['ceremony_time'],
            'party_reception_time' => $data['party_reception_time'],
            'party_time' => $data['party_time'],
            'place_id' => $data['place_id']
        ]);

        return $this->detailEvent($id);
    }

    /**
     * Update state live stream
     * @param $request
     * @return boolean
     * **/
    public function updateStateLivesteam($request)
    {
        $authId = Auth::guard('customer')->user()->id;
        $customer = $this->customerRepo->model
            ->where('id', $authId)
            ->first();
        if($customer){
            $event = $this->eventRepo->model->find($customer->wedding_id);
            $data = [];
            if(isset($request['is_join_table'])){
                $isJoinTable = is_numeric($request['is_join_table']) ? $request['is_join_table'] : 0;
                $data['is_join_table'] = $isJoinTable;
            }
            if(isset($request['speech_mode'])){
                $speechMode = is_numeric($request['speech_mode']) ? $request['speech_mode'] : 0;
                $data['speech_mode'] = $speechMode;
            }

            $event->update($data);

            return $this->detailEvent($customer->wedding_id);
        }

        return false;
    }

    /**
     * Notify to planner
     * @param $weddingID
     * @return boolean
     * **/
    public function notifyToPlanner($weddingID)
    {
        $wedding = $this->eventRepo->model->find($weddingID);
        $place = $wedding->place()->first();
        $restaurant = $place->restaurant()->first();
        $staff = $restaurant->user()->first();
        # Update status wedding
        $wedding->update([
            'is_notify_planner' => NotifyPlannerConstant::SEAT_REQUEST,
            'is_admin_approve' => STATUS_FALSE,
            'is_notify_position' => STATUS_TRUE
        ]);
        # Send mail
        $staffEmail = $staff->email;
        $contentEmail = [
            'contact_name' => $restaurant->contact_name,
            'groom_name' => $this->coupleNameForMail($weddingID, Role::GROOM)->full_name ?? "undefine",
            'app_url' => env('ADMIN_URL'),
        ];
        $sendEmailJob = new SendDoneSeatJob($staffEmail, $contentEmail);
        dispatch($sendEmailJob);

        return true;
    }

    /**
     * Update wedding speech_mode
     * @param Request $data
     * @return boolean
     * **/
    public function updateSpeechMode($data)
    {
        $this->eventRepo->model
            ->whereId($data['id'])
            ->update([
                'speech_mode' => $data['speech_mode']
            ]);

        return true;
    }

    /**
     * SET THE WEDDING IS SEND INVITE CARD
     * @param $requestData as $data
     * @return boolean
     *
     * **/
    public function sendWeddingCardToGuest($data)
    {
        DB::beginTransaction();
        try {
            $this->eventRepo->model->whereId($data['id'])->update([
                'send_invite_card_status' => STATUS_TRUE,
                'is_admin_approve' => STATUS_FALSE
            ]);

            #Change guest email_status
            $guests = $this->customerRepo->model
                ->where('wedding_id', $data['id'])
                ->where('role', Role::GUEST)
                ->whereNull('relative_id')
                ->whereNull('invitation_url')
                ->whereHas('customerInfo', function($q){
                    $q->where('is_send_wedding_card', '<>', STATUS_FALSE);
                })
                ->get();

            foreach($guests as $guest){
                $token = $guest->token;
                $inviteUrl = env('USER_URL') . "/respond-wedding-card?token=" . $token;
                $guestInfo = $guest->customerInfo()->first();
                $groomName = $this->coupleNameForMail($data['id'], Role::GROOM)->full_name;
                $brideName = $this->coupleNameForMail($data['id'], Role::BRIDE)->full_name;
                $emailStatus = $guest->customerInfo()->first()->email_status;

                #If has email, select send (status = 2)
                if($emailStatus == InviteSend::UNSEND){
                    $emailData = [
                        'groom_name' => $groomName,
                        'bride_name' => $brideName,
                        'guest_name' => $guestInfo->first_name . " " . $guestInfo->last_name,
                        'data_footer' => $this->dataFooterRestaurant($data['id']),
                        'deadline_guest' => $this->eventRepo->model->find($data['id'])->guest_invitation_response_date,
                        'url' => $inviteUrl
                    ];
                    $sendWeddingCard = new SendInviteCardToGuestJob(
                        "$groomName 様、$brideName 様ご結婚式　招待状に関して",
                        $guest->email,
                        $emailData
                    );
                    $guest->customerInfo()->update([
                        'email_status' => InviteSend::SENT,
                    ]);
                    dispatch($sendWeddingCard);
                }

                $guest->update([
                    'invitation_url' => $inviteUrl
                ]);
            }

            #Set flag guest is old
            $this->customerRepo->model->where('wedding_id', $data['id'])
                ->where('role', Role::GUEST)
                ->whereNull('relative_id')
                ->update([
                    'is_newest' => STATUS_FALSE
                ]);

            DB::commit();
            return true;
        } catch (\Exception $th) {
            DB::rollback();
            throw $th;
        }
    }

    /**
     * Update wedding is_livestream
     * @param Request $data
     * @return boolean
     * **/
    public function updateIsLivestream($data)
    {
        $dataUpdate = [];

        $dataUpdate['is_livestream'] = 1;
        if(isset($data['is_livestream']) && $data['is_livestream'] == 0) {
            $dataUpdate['is_close'] = 1;
        }

        $this->eventRepo->model
            ->whereId($data['wedding_id'])
            ->update($dataUpdate);

        return true;
    }
}
