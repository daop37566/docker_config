<?php
namespace App\Services\Response;

use App\Constants\Role;
use Illuminate\Database\Eloquent\Collection;
use App\Models\Customer;

class TableAccountResponse
{
    /**
     * Clean data table account
     * @param $data
     * @return $data_clear
     * **/
    public static function cleanDataGetListTableAccount(Collection $data)
    {
        try {
            $data_clear = [];
            if($data && count($data)) {
                foreach($data as $value) {
                    if($value->role != 9 && in_array($value->role, [Role::COUPE_TABLE, Role::SPEECH_TABLE, Role::STAGE_TABLE])){
                        $data_clear[] = [
                            'id' => $value->id,
                            'role' => $value->role,
                            'name' => Customer::TABLE_ACCOUNT[$value->role],
                            'token' => $value->token
                        ];

                    }else{
                        if(isset($value->channel->tablePosition)) {
                            $data_clear[] = [
                                'id' => $value->id,
                                'role' => $value->role,
                                'name' => $value->channel->tablePosition->position,
                                'token' => $value->token
                            ];
                        }
                    }
                }
            }
            return $data_clear;
        } catch (\Exception $th) {
            throw $th;
        }
        return $data_clear;
    }
}
