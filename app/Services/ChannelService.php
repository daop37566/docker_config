<?php

namespace App\Services;

use DB;
use App\Constants\Role;
use App\Constants\EventConstant;
use App\Constants\CustomerConstant;
use App\Repositories\ChannelRepository;
use Illuminate\Support\Facades\Auth;
use App\Libs\Agora\RtcTokenBuilder;
use App\Constants\Common;
use App\Repositories\CustomerRepository;
use App\Jobs\SendMailForGuestAcceptJob;

class ChannelService
{
    protected $channelRepo;

    public function __construct(ChannelRepository $channelRepo, CustomerRepository $customerRepo)
    {
        $this->channelRepo = $channelRepo;
        $this->customerRepo = $customerRepo;
    }

    /**
     * Show detail channel
     * @param $id
     * @return $channel
     * **/
    public function showDetail($id)
    {
        $channel = $this->channelRepo->model->whereId($id)->first();

        return $channel;
    }

    /**
     * Get list channel
     * @param $request
     * @return $list
     * **/
    public function getAll($request)
    {
        $orderBy = isset($request['order_by']) ? explode('|', $request['order_by']) : [];
        $keyword = !empty($request['keyword']) ? $request['keyword'] : null;
        $paginate = !empty($request['paginate']) ? $request['paginate'] : Common::PAGINATE;
        $list = $this->channelRepo->model;
        if (Auth::guard('customer')->check()) {
            $list = $list->whereWeddingId(auth()->guard('customer')->user()->wedding_id)
                ->when(!empty($keyword), function ($q) use ($keyword) {
                    $q->where('name', 'like', '%' . $keyword . '%');
                })
                ->where('status', Common::STATUS_TRUE)
                ->with(['tableAccount' => function ($q) {
                    $q->select('id', 'full_name', 'username');
                }]);
        }
        if ($paginate != Common::PAGINATE_ALL) {
            $list = $list->with(['tablePosition' => function ($q) {
                $q->select('id', 'position', 'status');
            }])->paginate($paginate);
        } else {
            $list = $list->with(['tablePosition' => function ($q) {
                $q->select('id', 'position', 'status');
            }])->get();
        }
        return $list;
    }

    /**
     * Update channel
     * @param $id, array $request
     * @return $channel
     * **/
    public function updateChannel(int $id, array $request)
    {
        $channel = $this->showDetail($id);
        $data =  [];
        if ($channel) {
            if (is_numeric($request['status'])) {
                $data['status'] = $request['status'];
            }
            $channel->update($data);
        }
        return $channel;
    }

    /**
     * CREATE CHANNEL NOMAL
     * @param Request $data
     * return true
     * **/
    static public function createChannelTableNomal($data)
    {
        // Find wedding ID
        $wedding = \DB::table('weddings')
            ->where('id', $data['wedding_id'])
            ->first();
        // List Table of WeddingID
        $tables = \DB::table('table_positions')
            ->join('customer_table', 'customer_table.table_position_id', '=', 'table_positions.id')
            ->join('customers', 'customers.id', '=', 'customer_table.customer_id')
            ->where('customers.wedding_id', $wedding->id)
            ->where('table_positions.place_id', $wedding->place_id)
            ->select('table_positions.id', 'table_positions.position')
            ->groupBy('table_positions.id', 'table_positions.position')
            ->get();

        if(count($tables)) {
            foreach ($tables as $key => $table) {
                // Check channel position
                $checkChannel = \DB::table('channels')
                    ->where('name', 'like', $table->position)
                    ->where('wedding_id', $data['wedding_id'])
                    ->exists();
                if (!$checkChannel) {
                    $customers = \DB::table('customers')
                        ->join('customer_table', 'customer_table.customer_id', '=', 'customers.id')
                        ->where('table_position_id', $table->id)
                        ->where('wedding_id', $data['wedding_id'])
                        ->where('role', [Role::GUEST])
                        ->whereIn('join_status', [CustomerConstant::JOIN_STATUS_APPROVED, CustomerConstant::JOIN_STATUS_JOIN_OFFLINE])
                        ->get();

                    if(count($customers)) {
                        // CREATE CustomerRole
                        $tableAccountId = CustomerService::createCustomerRole($wedding->id, Role::NORMAL_TABLE, $table->id);
                        // CREATE Channel Normal
                        $channel = [
                            'wedding_id'    => $data['wedding_id'],
                            'name'          => $table->position,
                            'display_name'  => $table->position,
                            'amount'        => 6,
                            'status'        => Common::STATUS_TRUE,
                            'type'          => EventConstant::TYPE_GUEST,
                            'start_time'    => null,
                            'end_time'      => null,
                            'created_at'    => \Carbon\Carbon::now(),
                            'updated_at'    => \Carbon\Carbon::now(),
                            'role'          => RtcTokenBuilder::RolePublisher,
                            'customer_id'   => $tableAccountId ?? null,
                            'table_position_id'   => $table->id ?? null,
                        ];
                        $id = \DB::table('channels')->insertGetId($channel);
                        if ($id) {
                            // Get Guest Join online send mail
                            $customersJoinOnline = \DB::table('customers')
                                ->join('customer_table', 'customer_table.customer_id', '=', 'customers.id')
                                ->join('customer_infos', 'customer_infos.customer_id', '=', 'customers.id')
                                ->where('table_position_id', $table->id)
                                ->where('customer_infos.is_send_wedding_card', CustomerConstant::IS_SEND_WEDDING_CARD)
                                ->where('wedding_id', $data['wedding_id'])
                                ->where('role', [Role::GUEST])
                                ->whereNotNull('customers.email')
                                ->where('join_status', [CustomerConstant::JOIN_STATUS_APPROVED])
                                ->get();
                            // GET role::OBSERVER
                            $customerObserver = \DB::table('customers')
                                ->where('wedding_id', $data['wedding_id'])
                                ->where('role', [Role::OBSERVER])
                                ->first();

                            // add Observer
                            $customers->push($customerObserver);

                            /**
                             *  Send mail for guest
                             *  Impact area : cronjob create channel
                             * */
                            if (isset($data['groom_name'])) {
                                if (count($customersJoinOnline) > 0) {
                                    foreach ($customersJoinOnline as $customer) {
                                        if ($customer->role == Role::GUEST) {
                                            $subject = $data['groom_name'] . "様、" . $data['bride_name'] . "様ご結婚式に関して";
                                            $guestName =  $customer->full_name;
                                            $guestToken =  $customer->token;
                                            $contentEmail = [
                                                'guest_name' => $guestName,
                                                'guest_token' => $guestToken,
                                                'data_footer' => $data['data_footer'],
                                                'app_url' => config('app.app_url'),
                                                'dynamic_link_url' => config('app.dynamic_link_url'),
                                                'url' => config('app.url'),
                                            ];
                                            SendMailForGuestAcceptJob::dispatch($subject, $customer->email, $contentEmail);
                                        }
                                    }
                                }
                            }

                            $customer_join_channels = [];
                            foreach ($customers as $customer) {
                                $customer_channel = [
                                    'channel_id'    => $id,
                                    'is_host'       => Common::STATUS_FALSE,
                                    'is_guest'      => Common::STATUS_TRUE,
                                    'customer_id'   => $customer->id,
                                    'status'        => Common::STATUS_TRUE,
                                    'created_at'    => \Carbon\Carbon::now(),
                                    'updated_at'    => \Carbon\Carbon::now(),
                                ];

                                array_push($customer_join_channels, $customer_channel);
                            }

                            \DB::table('customer_channel')->insert($customer_join_channels);
                        }
                    }
                }
            }
        }
        return true;
    }

    /**
     * DELETE NOT EXISTS CHANNEL NOMAL
     * @param Request $wedding_id
     * return true
     * **/
    public static function deleteChannelNotExists($weddingId)
    {
        // DELETE customer TableNormal
        \DB::table('customers')
            ->join('channels', 'customers.id', '=', 'channels.customer_id')
            ->whereNotNull('table_position_id')
            ->where('channels.wedding_id', $weddingId)
            ->delete();

        // DELETE channel Normal
        \DB::table('channels')
            ->where('wedding_id', $weddingId)
            ->whereNotNull('table_position_id')
            ->delete();

        // DELETE customer_channel not Exists
        \DB::table('customer_channel')
            ->whereNotExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('channels')
                    ->whereRaw('customer_channel.channel_id = channels.id');
            })
            ->delete();

        return true;
    }
}
