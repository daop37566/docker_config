<?php
namespace App\Services;

use App\Constants\Role;
use App\Repositories\Customer\TableAccountRepository;
use App\Repositories\EventRepository;
use App\Services\Response\TableAccountResponse;
use Illuminate\Support\Facades\Auth;

class TableAccountService
{
    protected $tableAccountRepository;
    protected $weddingRepository;

    public function __construct(
        TableAccountRepository $tableAccountRepository,
        EventRepository $weddingRepository
    ){
        $this->tableAccountRepository = $tableAccountRepository;
        $this->weddingRepository = $weddingRepository;
    }

    /**
     * Get list table account of Restaurant
     */
    public function getListOfWedding(string $restaurantId)
    {
        try {
            $data =  $this->tableAccountRepository->getListOfWedding($restaurantId);
            return  TableAccountResponse::cleanDataGetListTableAccount($data);
        } catch (\Exception $th) {
            throw $th;
        }
    }

    /**
     * Check list of staff
     * @param @weddingID
     * @return $status
     * **/
    public function existsList($weddingID)
    {
        $exist = $this->weddingRepository->model->whereId($weddingID)
            ->whereHas('place.restaurant.user', function($q){
                $q->whereId(Auth::user()->id);
            })
            ->exists();

        return $exist;
    }
}