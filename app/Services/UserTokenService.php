<?php
namespace App\Services;

use App\Repositories\UserTokenRepository;

class UserTokenService
{
    protected $userTokenRepo;

    public function __construct(UserTokenRepository $userTokenRepo)
    {
        $this->userTokenRepo = $userTokenRepo;
    }

    /**
     * Creat token
     * @param $userId, $token
     * @return boolean
     * **/
    public function createToken($userID, $token)
    {
        $this->userTokenRepo->model->create([
            'token' => $token,
            'user_id' => $userID
        ]);

        return true;
    }

    /**
     * Destroy user token
     * @param $userID
     * @return boolean
     * **/
    public function destroyUserToken($userID)
    {
        $this->userTokenRepo->model->where('user_id', $userID)->delete();
        return true;
    }

    /**
     * Destroy token
     * @param $token
     * @return void
     * **/
    public function destroyToken($token)
    {
        $this->userTokenRepo->model->where('token', $token)->delete();
    }
}