<?php

namespace App\Notifications;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Queue\SerializesModels;

class SendMailNotification extends Notification implements ShouldQueue
{
    use Queueable;

    use Dispatchable, InteractsWithSockets, SerializesModels;

    protected $user;

    public function __construct($mess)
    {
        $this->user = $mess;
    }

    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }


    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        dd($notifiable);
        return [
            //
        ];
    }

    public function toBroadcast($notifiable)
    {
        dd($notifiable);
        return [
            'notifiable' => $notifiable,
            'data' => $this->user,
        ];
    }
}
