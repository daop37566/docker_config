<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerTable extends Model
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'customer_table';

    /**
     * fillable
     */
    protected $fillable = [
        'customer_id',
        'table_position_id',
        'chair_name',
        'status'
    ];
}
