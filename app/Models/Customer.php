<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends AuthJWT
{
    use SoftDeletes;

    CONST TYPE_ROLE = [
        '1' => 'SUPER_ADMIN',
        '2' => 'STAFF_ADMIN',
        '3' => 'GROOM',
        '4' => 'BRIDE',
        '5' => 'GUEST',
        '6' => 'STAGE_TABLE',
        '7' => 'COUPE_TABLE',
        '8' => 'SPEECH_TABLE',
        '9' => 'NORMAL_TABLE',
        '10'=> 'OBSERVER'
    ];

    CONST TABLE_ACCOUNT = [
        '6' => 'メイン映像配信端末',
        '7' => '新郎新婦タブレット',
        '8' => '来賓用タブレット',
    ];

    protected $fillable = [
        'username',
        'email',
        'password',
        'wedding_id',
        'role',
        'phone',
        'address',
        'full_name',
        'token',
        'invitation_url',
        // 'table_position_id',
        'join_status',
        'confirmed_at',
        'order',
        'relative_id',
        'send_card_status',
        'is_speech',
        'is_newest',
        'remember_token'
    ];

    protected $hidden = [
        'password', 'remember_token', 'deleted_at'
    ];

    protected $table = 'customers';

    public function wedding()
    {
        return $this->belongsTo(Wedding::class, 'wedding_id');
    }

    public function tablePosition()
    {
        return $this->belongsToMany(
            TablePosition::class, 'customer_table', 'customer_id', 'table_position_id'
        )->withPivot('chair_name', 'status')->withTimestamps();
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = $value;
    }

    public function customerInfo()
    {
        return $this->hasOne(CustomerInfo::class, 'customer_id', 'id');
    }

    public function channel()
    {
        return $this->hasOne(Channel::class, 'customer_id', 'id');
    }

    public function customerTokens()
    {
        return $this->hasMany(CustomerToken::class, 'customer_id', 'id');
    }
}
