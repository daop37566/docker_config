<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerToken extends Model
{
    protected $table = "customer_tokens";
    protected $fillable = [
        'id',
        'customer_token',
        'token',
        'customer_id'
    ];

    /**
     * Get the user that owns the CustomerToken
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }
}
