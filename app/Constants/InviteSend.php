<?php
namespace App\Constants;

class InviteSend
{
    const SENT = 1;
    const UNSEND = 2;
    const NOT_EMAIL = 3;
    const DO_NOT_SEND = 4;
}