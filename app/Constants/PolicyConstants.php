<?php
namespace App\Constants;

class PolicyConstants
{
    const USE_OF_PLACE = 1; # STAFF ADMIN
    const USE_OF_COUPLE = 2; # COUPLE
    const PRIVACY_PLACE = 3; # STAFF ADMIN & SUPER ADMIN (public)
    const PRIVACY_COUPLE = 4; # COUPLE (public)

    const AMOUNT = 4;
}