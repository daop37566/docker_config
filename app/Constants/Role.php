<?php
namespace App\Constants;

class Role
{
    const SUPER_ADMIN = 1;
    const STAFF_ADMIN = 2;
    const GROOM = 3;
    const BRIDE = 4;
    const GUEST = 5;
    // const TABLE_ACCOUNT = 6;
    const STAGE_TABLE = 6;
    const COUPE_TABLE = 7;
    const SPEECH_TABLE = 8;
    const NORMAL_TABLE = 9;
    const OBSERVER = 10;
}