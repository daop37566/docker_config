<?php
namespace App\Constants;

class Common
{
    const PAGINATE_ALL = 'all';
    const PAGINATE = 10;
    const STATUS_TRUE = 1;
    const STATUS_FALSE = 0;
    const MAX_ONLINE_TABLE = 6;
}