<?php
namespace App\Constants;

class NotifyPlannerConstant
{
    const UNSENT = 0; #Lock Admin, Open User
    const SENT = 1; #Open Admin, Lock User
    const UNLOCK = 2; #Admin unlock
    const CHECKED = 3; #Admin checked, Couple can send card
    const SEAT_REQUEST = 4; #Request staff checked seat
}