<?php
namespace App\Constants;

class ChannelName
{
    const MAIN = "メイン映像配信端末";    // Name : Stage
    const COUPLE = "新郎新婦タブレット";  // Name : Couple
    const SPEECH = "来賓用タブレット";    // Name : Test
}
