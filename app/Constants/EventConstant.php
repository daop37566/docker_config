<?php
namespace App\Constants;

class EventConstant
{
    const PAGINATE = 10;
    const TYPE_COUPE = 1;
    const TYPE_STAGE = 2;
    const TYPE_TEST = 3;
    const TYPE_GUEST = 4;
}