<?php
namespace App\Constants;

class FileConstant
{
    const TYPE_FILE_PLACE = ['png', 'jpg', 'jpeg'];
    const TYPE_FILE_COUPLE = ['png', 'jpg'];
    const TYPE_FILE_POLICY = ['pdf'];
}