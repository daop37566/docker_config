<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CustomerExport implements FromCollection, WithHeadings
{
    protected $guestList;

    public function __construct($guestList)
    {
        $this->guestList = $guestList;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return collect($this->guestList);
    }

    public function headings() :array
    {
        return [
            '姓(漢字)',
            '名(漢字)',
            '姓(ふりがな)',
            '名(ふりがな)',
            '郵便番号',
            '住所（番地まで）',
            '住所(マンション名等)',
            '肩書き（1行目）',
            '肩書き（2行目）',
            '肩書き（3行目）',
            '肩書き（4行目）',
        ];
    }
}
