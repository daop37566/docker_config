<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Mail;

class NotifyToGuestStaffEditGuestJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $sendTo;
    private $content;
    private $templateName;
    private $subject;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($subject, $templateName, $sendTo, $content)
    {
        $this->subject = $subject;
        $this->templateName = $templateName;
        $this->sendTo = $sendTo;
        $this->content = $content;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $sendTo = $this->sendTo;
        $content = $this->content;
        $templateName = $this->templateName;
        $subject = $this->subject;

        Mail::send('mails/' . $templateName, $content, function($msg) use($sendTo, $subject){
            $msg->to($sendTo)->subject($subject);
        });
    }
}
