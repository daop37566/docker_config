<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Mail;

class SendMailResetPasswordJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $toEmail;
    private $emailInfo;
    private $templateName;

    public function __construct($toEmail, $emailInfo, $templateName)
    {
        $this->toEmail = $toEmail;
        $this->emailInfo = $emailInfo;
        $this->templateName = $templateName;
    }

    public function handle()
    {
        $toEmail = $this->toEmail;
        $emailInfo = $this->emailInfo;
        $templateName = $this->templateName;

        Mail::send('mails/'. $templateName, $emailInfo, function($msg) use($toEmail){
            $msg->to($toEmail)->subject("パスワードリセット");
        });
    }
}
