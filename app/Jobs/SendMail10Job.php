<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Mail;

class SendMail10Job implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $toEmail;
    private $content;
    private $subject;

    public function __construct($toEmail, $content, $subject)
    {
        $this->toEmail = $toEmail;
        $this->content = $content;
        $this->subject = $subject;
    }

    public function handle()
    {
        $toEmail = $this->toEmail;
        $content = $this->content;
        $subject = $this->subject;

        Mail::send('mails/notify_to_guest_mail_10', $content, function($msg) use($toEmail, $subject){
            $msg->to($toEmail)->subject($subject);
        });
    }
}
