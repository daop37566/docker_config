<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Mail;

class NotifyGuestWillNotJoinJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $sendTo;
    private $content;
    private $subject;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($subject, $sendTo, $content)
    {
        $this->subject = $subject;
        $this->sendTo = $sendTo;
        $this->content = $content;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $sendTo = $this->sendTo;
        $content = $this->content;
        $subject = $this->subject;

        Mail::send('mails/notify_not_join_12', $content, function($msg) use($sendTo, $subject){
            $msg->to($sendTo)->subject($subject);
        });
    }
}
