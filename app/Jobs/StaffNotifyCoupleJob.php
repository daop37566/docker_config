<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Mail;

class StaffNotifyCoupleJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $toEmail;
    private $emailInfo;

    public function __construct($toEmail, $emailInfo)
    {
        $this->toEmail = $toEmail;
        $this->emailInfo = $emailInfo;
    }

    public function handle()
    {
        $toEmail = $this->toEmail;
        $emailInfo = $this->emailInfo;

        Mail::send('mails/staff_notify_couple', $emailInfo, function($msg) use($toEmail){
            $msg->to($toEmail)->subject("WEB招待状の確認が完了しました");
        });
    }
}
