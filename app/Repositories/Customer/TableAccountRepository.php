<?php

namespace App\Repositories\Customer;

use App\Constants\Role;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Auth;

class TableAccountRepository extends BaseRepository
{
    public function getModel()
    {
        return \App\Models\Customer::class;
    }

    /**
     * Get list table account of wedding
     */
    public function getListOfWedding(string $weddingId)
    {
        // table account => with channel => lấy được bàn
        $data = $this->model
            ->select('id', 'token', 'role')
            ->with([
                'channel.tablePosition'
            ])
            ->whereIn('role', [
                Role::NORMAL_TABLE,
                Role::STAGE_TABLE,
                Role::COUPE_TABLE,
                Role::SPEECH_TABLE,
            ])
            ->whereHas('wedding', function($q) use ($weddingId) {
                $q->where('id', $weddingId);
                $q->whereHas('place.restaurant.user', function($q){
                    $q->whereId(Auth::user()->id);
                });
            })
            ->orderBy('role')
            ->get();

        return $data;
    }
}
