<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

class PolicyRepository extends BaseRepository
{
    public $model;

    public function getModel()
    {
        return \App\Models\Policy::class;
    }
}
