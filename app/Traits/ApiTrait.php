<?php

namespace App\Traits;

trait ApiTrait
{
    /**
     * Respond success status
     * @param $data
     * @return json $data
     * **/
    protected function respondSuccess($data)
    {
        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }

    /**
     * Respond error
     * @param $code, $message
     * @return json $data
     * **/
    protected function respondError($code, $message)
    {
        return response()->json([
            'success' => false,
            'data' => [
                'code' => $code,
                'message' => $message,
            ]
        ]);
    }
}
