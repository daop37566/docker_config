<?php

namespace App\Traits;

use App\Models\Restaurant;
use App\Models\Wedding;
use App\Constants\CustomerConstant;
use App\Constants\Role;
use App\Jobs\NotifyCoupleStaffAddGuestJob;
use App\Jobs\NotifyToGuestStaffEditGuestJob;
use App\Jobs\SendMail10Job;

trait EmailTrait
{
    /**
     * Get data footer restauran
     * @param @wedddingID
     * @return $dataFooterRestaurant array
     * **/
    protected function dataFooterRestaurant(int $weddingID)
    {
        $res = Restaurant::whereHas('places.weddings', function($q) use($weddingID){
            $q->whereId($weddingID);
        })
        ->select(
            "id",
            "name",
            "post_code",
            "address_1",
            "address_2",
            "phone",
            "contact_email"
        )
        ->first();

        $wed = Wedding::whereId($weddingID)->select("id", "pic_name")->first();

        return [
            'res_name' => $res->name,
            'res_post_code' => $res->post_code,
            'res_address_1' => $res->address_1,
            'res_address_2' => $res->address_2 ?? "",
            'res_phone' => $res->phone,
            'res_email' => $res->contact_email,
            'pic_name' => $wed->pic_name
        ];
    }

    /**
     * Convert join status code to join status JP text
     * @param int $joinStataus
     * @return string $string
     * **/
    protected function joinStatusText(int $joinStatus)
    {
        $string = null;
        switch ($joinStatus)
        {
            case CustomerConstant::JOIN_STATUS_CANCEL:
                $string = "欠席";
                break;
            case CustomerConstant::JOIN_STATUS_APPROVED:
                $string = "リモート参加";
                break;
            case CustomerConstant::JOIN_STATUS_JOIN_OFFLINE:
                $string = "出席";
                break;
            default:
                # code...
                break;
        }

        return $string;
    }

    /**
     * Get couple name for mail
     * @param $weddingID, $who
     * @return coupleName
     * **/
    protected function coupleNameForMail($weddingID, $who)
    {
        $data = $this->customerRepo->model
            ->where('wedding_id', $weddingID)
            ->when(isset($who), function($q) use($who){
                $q->where('role', $who);
            })
            ->when(!isset($who), function($q){
                $q->where(function($q){
                    $q->where('role', Role::GROOM);
                    $q->orWhere('role', Role::BRIDE);
                });
            });

        if($who == null)
        {
            $data = $data->get();
        }
        else
        {
            $data = $data->first();
        }

        return $data;
    }

    /**
     * Notify to guest bank account condition
     * @param $guest object, $guestInfo object, $guestName, $email
     * @return void
     * **/
    public function notifyToGuestBankCondition($guest, $guestInfo, $guestName, $email)
    {
        $groomName = $this->coupleNameForMail($guest->wedding_id, Role::GROOM)->full_name ?? null;
        $brideName = $this->coupleNameForMail($guest->wedding_id, Role::BRIDE)->full_name ?? null;
        $dataFooter = $this->dataFooterRestaurant($guest->wedding_id);
        $notifyQueue = null;
        if($guestInfo->is_send_wedding_card == STATUS_TRUE)
        {
            #If selected bank
            if(isset($guestInfo->bank_account_id)) #Send email 11a
            {
                $bankAccount = $this->bankAccountRepo->model->whereId($guestInfo->bank_account_id)->first();
                $weddingCard = $this->weddingCardRepo->model->where('wedding_id', $guest->wedding_id)->first();
                $wedding = $this->weddingRepo->model->find($guest->wedding_id);
                $emailContent = [
                    'guest_name' => $guestName,
                    'bank_account' => $bankAccount,
                    'wedding_price' => $weddingCard->wedding_price,
                    'guest_invitation_response_date' => $wedding->guest_invitation_response_date,
                    'data_footer' => $dataFooter
                ];

                $notifyQueue = new NotifyToGuestStaffEditGuestJob(
                    "$groomName 様、$brideName 様ご結婚式　ご出席のご連絡ありがとうございます", //Email subject,
                    "notify_to_guest_11a", //TemplateName,
                    $email, //Send to
                    $emailContent //Email content
                );
            }
            else
            { #Send email 11b
                $emailContent = [
                    'guest_name' => $guestName,
                    'url' => env('USER_URL'),
                    'data_footer' => $dataFooter
                ];

                $notifyQueue = new NotifyToGuestStaffEditGuestJob(
                    "$groomName 様、$brideName 様ご結婚式　ご出席のご連絡ありがとうございます", //Email subject,
                    "notify_to_guest_11b", //TemplateName,
                    $email, //Send to
                    $emailContent //Email content
                );
            }
            #Dispath email
            dispatch($notifyQueue);
        }
    }

    /**
     * Notify email 10
     * @param @guest, $guestName, $email
     * @return void
     * **/
    public function notifyEmail10($guest, $guestName, $email)
    {
        $groomName = $this->coupleNameForMail($guest->wedding_id, Role::GROOM)->full_name ?? null;
        $brideName = $this->coupleNameForMail($guest->wedding_id, Role::BRIDE)->full_name ?? null;

        $emailContent = [
            'guest_name' => $guestName,
            'url' => env('USER_URL'),
            'data_footer' => $this->dataFooterRestaurant($guest->wedding_id)
        ];
        $subject = "$groomName 様、$brideName 様ご結婚式　ご出席のご連絡ありがとうございます";
        dispatch(new SendMail10Job($email, $emailContent, $subject));
    }

    /**
     * SEND MAIL 8 NOTIFY TO COUPLE
     * @param @weddingID, $requestData
     * @return void
     * **/
    public function notifyToCouple($weddingID, $requestData)
    {
        #Notify to Couple
        $couple = $this->coupleNameForMail($weddingID, NULL);
        foreach($couple as $value)
        {
            $guest = collect([
                'full_name' => $requestData['first_name'] . " " . $requestData['last_name'],
                'join_status' => $this->joinStatusText($requestData['join_status'])
            ]);

            $emailContent = [
                'groom_name' => $this->coupleNameForMail($weddingID, Role::GROOM)->full_name,
                'bride_name' => $this->coupleNameForMail($weddingID, Role::BRIDE)->full_name,
                'guests' => [$guest],
                'url' => env('USER_URL'),
                'data_footer' => $this->dataFooterRestaurant($weddingID)
            ];

            dispatch(new NotifyCoupleStaffAddGuestJob($value['email'], $emailContent));
        }
    }

    /**
     * Notify update guest email
     * @return void
     * **/
    public function notifyWhenUpdateGuest($guest, $guestInfo, $guestName, $data)
    {
        #Notify to guest bank account condition
        if(
            $guest->join_status != $data['join_status'] &&
            $data['join_status'] == CustomerConstant::JOIN_STATUS_APPROVED &&
            isset($data['email'])
        )
        {
            $this->notifyToGuestBankCondition($guest, $guestInfo, $guestName, $data['email']);
        }

        #Notify email 10
        if(
            isset($data['email']) AND
            $data['join_status'] == CustomerConstant::JOIN_STATUS_JOIN_OFFLINE AND
            $guestInfo->is_send_wedding_card == STATUS_TRUE
        ){
            $this->notifyEmail10($guest, $guestName, $data['email']);
        }

        #Notify to couple email 8
        if($guest->join_status != $data['join_status'])
        {
            $this->notifyToCouple($guest->wedding_id, $data);
        }
    }
}
