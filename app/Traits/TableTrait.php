<?php

namespace App\Traits;

use App\Models\TablePosition;
use App\Constants\Role;

trait TableTrait
{
    /**
     * Count amount guest in table
     * @param $joinStatusConstant as $joinStatus, $tableID, $weddingID
     * @return $amountGuestInDBTable
     * **/
    protected function amountGuestWithJoinStatus($joinStatus, $tableID, $weddingID)
    {
        $tablePosition = TablePosition::find($tableID);
        if($tablePosition)
        {
            return $tablePosition->customers()
                ->where('role', Role::GUEST)
                ->where('join_status', $joinStatus)
                ->where('wedding_id', $weddingID)
                ->count();
        }

        return 0;
    }
}