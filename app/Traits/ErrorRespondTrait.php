<?php

namespace App\Traits;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;

trait ErrorRespondTrait
{
    /**
     * Respond error messages validate
     * @param $messages
     * @return $response
     * **/
    protected function respondErrorMessage(string $messages)
    {
        $response = $this->respondError(
            Response::HTTP_BAD_REQUEST,
            $messages
        );

        throw new HttpResponseException($response);
    }

    /**
     * Respond not found messages validate
     * @param $messages
     * @return $response
     * **/
    protected function respondNotFoundValidate(string $messages)
    {
        $response = $this->respondError(
            Response::HTTP_NOT_FOUND,
            $messages
        );

        throw new HttpResponseException($response);
    }
}