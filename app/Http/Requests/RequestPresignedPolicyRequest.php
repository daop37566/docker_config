<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Constants\FileConstant;

class RequestPresignedPolicyRequest extends ApiRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file_policy' => [
                'required', 
                function ($attribute, $value, $fail) {
                    $valueArr = explode('.', $value);
                    if(strtolower($valueArr[count($valueArr) - 1]) !== $valueArr[count($valueArr) - 1]) {
                        return $fail('The ' . $attribute . ' not strtolower');
                    }else if (!in_array($valueArr[count($valueArr) - 1],  FileConstant::TYPE_FILE_POLICY)) {
                        return $fail(__('messages.policy.validation.file_policy.format'));
                    }
                }
            ],
        ];
    }
}
