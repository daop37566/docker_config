<?php

namespace App\Http\Requests;

use App\Http\Requests\ApiRequest;
use Illuminate\Support\Facades\Auth;
use App\Models\Wedding;

class ExportCsvRequest extends ApiRequest
{
   
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => [
                'required',
                function($attribute, $value, $fail){
                    $wedding = Wedding::whereHas('place.restaurant.user', function($q){
                        $q->whereId(Auth::user()->id);
                    });

                    if(!$wedding->exists()){
                        __('messages.event.validation.id.exists');
                    }
                }
            ],
        ];
    }

    public function messages()
    {
        return [
           'id.required' => __('messages.event.validation.id.required')
        ];
    }
}
