<?php

namespace App\Http\Requests;

use App\Http\Requests\ApiRequest;
use App\Models\Customer;
use App\Models\TablePosition;
use App\Constants\Role;
use App\Constants\CustomerConstant;
use App\Constants\Common;
use App\Traits\ErrorRespondTrait;
use App\Traits\TableTrait;
use Illuminate\Support\Facades\Auth;

class CoupleUpdateGuestTableRequest extends ApiRequest
{
    use ErrorRespondTrait, TableTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => [
                'required',
                function($attribute, $value, $fail)
                {
                    $guestID = request()->id;
                    $weddingID = Customer::find($guestID)->wedding_id ?? null;
                    $tableID = request()->table_position_id;
                    $guest = Customer::where('id', $guestID)
                        ->where('role', Role::GUEST);
                    #If exists guest
                    if($guest->exists())
                    {
                        $joinStatus = $guest->select('join_status')
                            ->first()->join_status;
                        if($joinStatus == CustomerConstant::JOIN_STATUS_APPROVED)
                        {
                            $amoutGuest = $this->amountGuestWithJoinStatus(
                                CustomerConstant::JOIN_STATUS_APPROVED,
                                $tableID,
                                $weddingID
                            );
                            if($amoutGuest >= Common::MAX_ONLINE_TABLE){
                                return $this->respondErrorMessage(__('messages.participant.max_remote'));
                            }
                        }
                        else if($joinStatus == CustomerConstant::JOIN_STATUS_JOIN_OFFLINE)
                        {
                            $amoutGuest = $this->amountGuestWithJoinStatus(
                                CustomerConstant::JOIN_STATUS_JOIN_OFFLINE,
                                $tableID,
                                $weddingID
                            );
                            $amountChair = TablePosition::find($tableID)->amount_chair ?? 0;
                            if($amoutGuest  >= $amountChair){
                                return $this->respondErrorMessage(__('messages.participant.max_offline'));
                            }
                        }
                    }
                    #If guest wedding id != couple wedding id
                    if($weddingID != Auth::guard('customer')->user()->wedding_id)
                    {
                        return $this->respondErrorMessage(__('messages.participant.validation.id.exists'));
                    }
                }
            ]
        ];
    }
}
