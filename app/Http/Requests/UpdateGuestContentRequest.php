<?php

namespace App\Http\Requests;

use App\Http\Requests\ApiRequest;
use Illuminate\Support\Facades\Auth;
use App\Models\Customer;
use App\Models\User;
use App\Constants\Role;

class UpdateGuestContentRequest extends ApiRequest
{
   

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => [
                'required',
                function($attribute, $value, $fail){
                    $guest = Customer::where('role', Role::GUEST)
                        ->where('id', $value);

                    if($guest->exists()){
                        $weddingID = $guest->first()->wedding_id;
                        $user = User::whereHas('restaurant.places.weddings', function($q) use($weddingID){
                            $q->whereId($weddingID);
                        });
                        if(!$user->exists()){
                            $fail(__('messages.participant.validation.id.exists'));
                        }
                    }else{
                        $fail(__('messages.participant.validation.id.exists'));
                    }
                }
            ],
            'task_content' => 'nullable|string|max:400',
            'content' => 'required|string|max:500',
        ];
    }

    public function messages()
    {
        return [
            'id.required' => __('messages.participant.validation.id.required'),
            'task_content.max' => __('messages.participant.validation.task_content.max'),
            'content.required' => __('messages.wedding_card.validation.content.required'),
            'content.max' => __('messages.wedding_card.validation.content.max'),
        ];
    }
}
