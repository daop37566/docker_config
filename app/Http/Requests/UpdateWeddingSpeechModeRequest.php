<?php

namespace App\Http\Requests;

use App\Http\Requests\ApiRequest;
use App\Models\Wedding;
use Illuminate\Support\Facades\Auth;

class UpdateWeddingSpeechModeRequest extends ApiRequest
{
    public function rules()
    {
        return [
            'id' => [
                function($attribute, $value, $fail)
                {
                    $observerID = Auth::guard('customer')->user()->id;
                    $wedding = Wedding::whereHas('customers', function($q) use($observerID){
                        $q->whereId($observerID);
                    });

                    if(!$wedding->exists()){
                        $fail(__('messages.participant.validation.id.exists'));
                    }
                }
            ],
            'speech_mode' => 'numeric|required'
        ];
    }

    public function messages()
    {
        return [
            'speech_mode.numeric' => __('messages.event.validation.speech_mode.numeric'),
            'speech_mode.required' => __('messages.event.validation.speech_mode.required'),
        ];
    }
}