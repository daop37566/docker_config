<?php

namespace App\Http\Requests;

use App\Http\Requests\ApiRequest;
use App\Models\User;

class StaffGetListGuestRequest extends ApiRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => [
                'required',
                function($attribute, $value, $fail)
                {
                    $user = User::whereHas('restaurant.places.weddings', function($q){
                        $q->whereId(request()->id);
                    });

                    if(!$user->exists()){
                        $fail(__('messages.event.validation.id.exists'));
                    }
                }
            ]
        ];
    }

    public function messages()
    {
        return [
            'id.required' => __('messages.event.validation.id.required')
        ];
    }
}
