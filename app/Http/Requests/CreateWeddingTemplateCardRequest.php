<?php

namespace App\Http\Requests;

use App\Http\Requests\ApiRequest;

class CreateWeddingTemplateCardRequest extends ApiRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|unique:template_cards,name',
            'type' => 'required',
            'image_big' => 'required|mimes:jpg,png|max:10240',
            'image_thumb' => 'required|mimes:jpg,png|max:10240'
        ];
    }
}
