<?php

namespace App\Http\Requests;

use App\Http\Requests\ApiRequest;
use Illuminate\Support\Facades\Auth;
use App\Models\Customer;

class UpdateIsSpeechRequest extends ApiRequest
{
   

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'is_speech' => 'numeric',
            'guest_id' => [
                'numeric',
                function($attribute, $value, $fail)
                {
                    $weddingID = Auth::guard('customer')->user()->wedding_id;
                    $guest = Customer::whereId($value)
                        ->where('wedding_id', $weddingID);

                    if(!$guest->exists()){
                        $fail(__('messages.participant.validation.id.exists'));
                    }
                }
            ]
        ];
    }
}
