<?php

namespace App\Http\Requests;

use App\Http\Requests\ApiRequest;
use Illuminate\Support\Facades\Auth;
use App\Models\Wedding;

class UpdateWeddingIsLivestreamRequest extends ApiRequest
{
    public function rules()
    {
        return [
            "wedding_id" => [
                'required',
                function($attribute, $value, $fail)
                {
                    $staffID = Auth::user()->id ?? null;
                    $wedding = Wedding::whereId($value)->whereHas('place.restaurant.user', function($q) use($staffID){
                        $q->whereId($staffID);
                    });

                    if(!$wedding->exists()){
                        return $fail(__('messages.event.validation.id.exists'));
                    }
                }
            ],
            'is_livestream' => 'nullable|numeric',
            'is_close' => 'nullable|numeric'
        ];
    }

    public function messages()
    {
        return [
            'is_livestream.numeric' => __('messages.event.validation.is_livestream.numeric'),
            'is_close.numeric' => __('messages.event.validation.is_close.numeric'),
        ];
    }
}
