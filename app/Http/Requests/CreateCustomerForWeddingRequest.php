<?php

namespace App\Http\Requests;

use App\Models\TablePosition;
use App\Http\Requests\ApiRequest;

class CreateCustomerForWeddingRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected $tablePostionID;
    protected $weddingID;

    public function authorize()
    {
        return true;
    }
    public function __construct()
    {
        $this->tablePostionID = request()->table_postion_id;
        $this->weddingID = request()->wedding_id;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'wedding_id' => 'required|exists:weddings,id',
            'role' => 'required',
            'quantity' => ['required','integer', 'max:6', 'min:1'],
            'table_postion_id' => [
                'required',
                function($attribute, $value, $fail)
                {
                    $this->tablePostionID = $value;
                    $table_postion = TablePosition::whereId($this->tablePostionID)->whereHas('place.weddings', function($q){
                        $q->whereId($this->weddingID);
                    });
                    if(!$table_postion->exists()){
                        $fail(__('messages.participant.validation.table_position_id.exists'));
                    }
                }
            ],
        ];
    }
    public function messages()
    {
        return [
            'wedding_id.required' => __('messages.participant.validation.wedding_id.required'),
            'wedding_id.exists' => __('messages.participant.validation.wedding_id.exists'),
            'role.required' => __('messages.participant.validation.role.required'),

            'quantity.required' => __('messages.participant.validation.quantity.required'),
            'quantity.integer' => __('messages.participant.validation.quantity.integer'),
            'quantity.max' => __('messages.participant.validation.quantity.max'),
            'quantity.min' => __('messages.participant.validation.quantity.min'),

            'table_postion_id.required' => __('messages.participant.validation.table_postion_id.required'),
            'table_postion_id.exsits' => __('messages.participant.validation.table_postion_id.exsits'),
        ];
    }
}
