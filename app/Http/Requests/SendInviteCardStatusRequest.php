<?php

namespace App\Http\Requests;

use App\Http\Requests\ApiRequest;
use Illuminate\Support\Facades\Auth;
use App\Models\Wedding;

class SendInviteCardStatusRequest extends ApiRequest
{
   

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => [
                function($attribute, $value, $fail)
                {
                    $weddingID = Auth::guard('customer')->user()->wedding_id;
                    $wedding = Wedding::whereId($weddingID);

                    if(!$wedding->exists()){
                        $fail(__('messages.event.validation.id.exists'));
                    }
                }
            ],
            'send_invite_card_status' => 'numeric'
        ];
    }
}
