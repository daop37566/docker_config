<?php

namespace App\Http\Requests;

use App\Http\Requests\ApiRequest;
use App\Traits\ErrorRespondTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;

class StaffGetListTableAccountRequest extends ApiRequest
{
    use ErrorRespondTrait;
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'wedding_id' => [
                'required',
                function($attribute, $value, $fail)
                {
                    $weddingID = $value;
                    $exist = Auth::user()->whereHas('restaurant.places.weddings', function($q) use($weddingID) {
                        $q->whereId($weddingID);
                    })->exists();

                    if(!$exist){
                        return $this->respondNotFoundValidate(__('messages.event.validation.id.exists'));
                    }
                }
            ]
        ];
    }
}
