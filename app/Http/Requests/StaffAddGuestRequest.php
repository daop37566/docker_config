<?php

namespace App\Http\Requests;

use App\Http\Requests\ApiRequest;
use Illuminate\Support\Facades\Auth;
use App\Models\Customer;
use App\Models\TablePosition;
use App\Models\Wedding;
use App\Constants\Role;
use App\Constants\CustomerConstant;
use App\Constants\Common;
use App\Traits\ErrorRespondTrait;
use App\Traits\TableTrait;

class StaffAddGuestRequest extends ApiRequest
{
    use ErrorRespondTrait, TableTrait;
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'join_status' => 'required|numeric',
            'first_name' => 'required|max:10',
            'last_name' => 'required|max:10',
            'relationship_couple' => 'required|max:50',
            'email' => 'nullable|max:50|email|regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix',
            'post_code' => 'nullable|digits:7|numeric',
            'phone' => 'nullable|digits_between:10,11',
            'address' => 'nullable|max:200|string',
            'wedding_id' => [
                'required',
                'numeric',
                function($attribute, $value, $fail)
                {
                    $weddingID = $value;
                    $staffID = Auth::user()->id;

                    $wedding = Wedding::whereId($weddingID)
                        ->whereHas('place.restaurant.user', function($q) use($staffID){
                            $q->whereId($staffID);
                        });

                    if(!$wedding->exists()){
                        return $fail(__('messages.participant.validation.wedding_id.exists'));
                    }
                }
            ],
            'table_position_id' => [
                function($attribute, $value, $fail)
                {
                    $tableID = request()->table_position_id;
                    $joinStatus = request()->join_status;
                    $weddingID = request()->wedding_id;

                    # If join remote or join offline
                    if(
                        $joinStatus == CustomerConstant::JOIN_STATUS_JOIN_OFFLINE OR
                        $joinStatus == CustomerConstant::JOIN_STATUS_APPROVED
                    ){
                        # Require table id
                        if(!isset($tableID))
                        {
                            $fail(__('messages.participant.validation.table_position_id.required'));
                        }
                        # If exist table id
                        else
                        {
                            $exists = TablePosition::whereId($tableID)
                                ->whereHas('place.restaurant.user', function($q){
                                    $q->whereId(Auth::user()->id);
                                })->exists();

                            if($exists)
                            {
                                # If join remote limit person on table
                                if($joinStatus == CustomerConstant::JOIN_STATUS_APPROVED)
                                {
                                    #Count amout guest in table
                                    $amoutGuest = $this->amountGuestWithJoinStatus(
                                        CustomerConstant::JOIN_STATUS_APPROVED,
                                        $tableID,
                                        $weddingID
                                    );
                                    #Online guest can not be greater than 6 person in table
                                    if($amoutGuest >= Common::MAX_ONLINE_TABLE){
                                        return $fail(__('messages.participant.max_remote'));
                                    }
                                }
                                else if($joinStatus == CustomerConstant::JOIN_STATUS_JOIN_OFFLINE)
                                {
                                    $amoutGuest = $this->amountGuestWithJoinStatus(
                                        CustomerConstant::JOIN_STATUS_JOIN_OFFLINE,
                                        $tableID,
                                        $weddingID
                                    );
                                    $amountChair = TablePosition::find($tableID)->amount_chair ?? 0;
                                    if($amoutGuest  >= $amountChair){
                                        return $fail(__('messages.participant.max_offline'));
                                    }
                                }
                            }
                            # Does not exists table in this place
                            else
                            {
                                return $fail(__('messages.participant.validation.table_position_id.exists'));
                            }
                        }
                    }
                }
            ],
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'join_status.required' => __('messages.participant.validation.join_status.required'),
            'join_status.numeric' => __('messages.participant.validation.join_status.numeric'),

            'first_name.required' => __('messages.participant.validation.first_name.required'),
            'first_name.max' => __('messages.participant.validation.first_name.max'),

            'last_name.required' => __('messages.participant.validation.last_name.required'),
            'last_name.max' => __('messages.participant.validation.last_name.max'),

            'relationship_couple.required' => __('messages.participant.validation.relationship_couple.required'),
            'relationship_couple.max' => __('messages.participant.validation.relationship_couple.max'),

            'email.max' => __('messages.participant.validation.email.max'),
            'email.regex' => __('messages.participant.validation.email.regex'),
            'email.email' => __('messages.participant.validation.email.regex'),

            'post_code.digits' => __('messages.participant.validation.post_code.digits'),
            'post_code.numeric' => __('messages.participant.validation.post_code.numeric'),

            'address.max' => __('messages.participant.validation.address.max'),

            'table_position_id.required' => __('messages.participant.validation.table_position_id.required'),

            'wedding_id.required' => __('messages.participant.validation.wedding_id.required'),
            'wedding_id.exists' => __('messages.participant.validation.wedding_id.exists'),
        ];
    }
}