<?php

namespace App\Http\Requests;

use App\Http\Requests\ApiRequest;
use App\Models\Customer;

class GuestResponseCardRequest extends ApiRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'token' => 'required|exists:customers,token',
            'first_name' => 'required|max:10',
            'last_name' => 'required|max:10',
            'email' => 'required|max:50|email|regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix',
            'post_code' => 'nullable|digits:7|numeric',
            'address' => 'nullable|max:200|string',
            'phone' => 'nullable|digits_between:10,11',
            'join_status' => 'required|numeric|min:0|max:2',
            'free_word' => 'nullable|string|max:400',
            'guest_relative.*.id' => [
                'required',
                function($attribute, $value, $fail)
                {
                    $guestRelativeID = $value;
                    $mainGuestURL = request()->token;
                    $mainGuest = Customer::where('token', $mainGuestURL)
                        ->select('wedding_id', 'id')
                        ->first();

                    $exists = Customer::where('id', $guestRelativeID)
                        ->where('wedding_id', $mainGuest->wedding_id)
                        ->where('relative_id', $mainGuest->id)
                        ->exists();

                    if(!$exists){
                        return $fail(__('messages.participant.validation.id.exists'));
                    }
                }
            ],
            'guest_relative.*.first_name' => 'required|max:10',
            'guest_relative.*.last_name' => 'required|max:10',
            'guest_relative.*.email' => 'required|max:50|email|regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix',
            'guest_relative.*.post_code' => 'nullable|digits:7|numeric',
            'guest_relative.*.address' => 'nullable|max:200|string',
            'guest_relative.*.phone' => 'nullable|digits_between:10,11',
            'guest_relative.*.join_status' => 'required|numeric|min:0|max:2',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            #Main Guests
            'first_name.required' => __('messages.participant.validation.first_name.required'),
            'first_name.max' => __('messages.participant.validation.first_name.max'),

            'last_name.required' => __('messages.participant.validation.last_name.required'),
            'last_name.max' => __('messages.participant.validation.last_name.max'),

            'email.max' => __('messages.participant.validation.email.max'),
            'email.regex' => __('messages.participant.validation.email.regex'),
            'email.email' => __('messages.participant.validation.email.regex'),

            'post_code.required' => __('messages.participant.validation.post_code.required'),
            'post_code.digits' => __('messages.participant.validation.post_code.digits'),
            'post_code.numeric' => __('messages.participant.validation.post_code.numeric'),

            'address.required' => __('messages.participant.validation.address.required'),
            'address.max' => __('messages.participant.validation.address.max'),

            'phone.required' => __('messages.participant.validation.phone.required'),
            'phone.digits_between' => __('messages.participant.validation.phone.digits_between'),

            'join_status.min' => __('messages.participant.validation.join_status.min'),
            'join_status.max' => __('messages.participant.validation.join_status.max'),
            'join_status.numeric' => __('messages.participant.validation.join_status.numeric'),
            'join_status.required' => __('messages.participant.validation.join_status.required'),

            'token.required' => __('messages.participant.validation.remember_token.required'),
            'token.exists' => __('messages.participant.validation.remember_token.exists'),

            'free_word.max' => __('messages.participant.validation.free_word.max'),

            #Relative Guests
            'guest_relative.*.first_name.required' => __('messages.participant.validation.first_name.required'),
            'guest_relative.*.first_name.max' => __('messages.participant.validation.first_name.max'),

            'guest_relative.*.last_name.required' => __('messages.participant.validation.last_name.required'),
            'guest_relative.*.last_name.max' => __('messages.participant.validation.last_name.max'),

            'guest_relative.*.email.max' => __('messages.participant.validation.email.max'),
            'guest_relative.*.email.regex' => __('messages.participant.validation.email.regex'),
            'guest_relative.*.email.email' => __('messages.participant.validation.email.regex'),

            'guest_relative.*.post_code.required' => __('messages.participant.validation.post_code.required'),
            'guest_relative.*.post_code.digits' => __('messages.participant.validation.post_code.digits'),
            'guest_relative.*.post_code.numeric' => __('messages.participant.validation.post_code.numeric'),

            'guest_relative.*.address.required' => __('messages.participant.validation.address.required'),
            'guest_relative.*.address.max' => __('messages.participant.validation.address.max'),

            'guest_relative.*.phone.required' => __('messages.participant.validation.phone.required'),
            'guest_relative.*.phone.digits_between' => __('messages.participant.validation.phone.digits_between'),

            'guest_relative.*.join_status.min' => __('messages.participant.validation.join_status.min'),
            'guest_relative.*.join_status.max' => __('messages.participant.validation.join_status.max'),
            'guest_relative.*.join_status.numeric' => __('messages.participant.validation.join_status.numeric'),
            'guest_relative.*.join_status.required' => __('messages.participant.validation.join_status.required'),
        ];
    }
}
