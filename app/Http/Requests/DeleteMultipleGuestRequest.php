<?php

namespace App\Http\Requests;

use App\Http\Requests\ApiRequest;
use Illuminate\Support\Facades\Auth;
use App\Models\Customer;
use App\Constants\Role;

class DeleteMultipleGuestRequest extends ApiRequest
{
   
    private $weddingID;

    public function __construct()
    {
        $this->weddingID = Auth::guard('customer')->user()->wedding_id;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'id' => [
                'array'
            ],
            'id.*' => [
                'numeric',
                function($attribute, $value, $fail)
                {
                    $exist = Customer::where('id', $value)
                        ->where('wedding_id', $this->weddingID)
                        ->where('role', Role::GUEST)
                        ->exists();

                    if(!$exist){
                        $fail(__('messages.participant.validation.id.exists'));
                    }
                }
            ]
        ];

        return $rules;
    }
}
