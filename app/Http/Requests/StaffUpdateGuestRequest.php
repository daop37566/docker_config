<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use App\Constants\Role;
use App\Constants\CustomerConstant;
use App\Constants\Common;
use App\Models\Customer;
use App\Models\Wedding;
use App\Models\TablePosition;
use App\Traits\TableTrait;
use Carbon\Carbon;

class StaffUpdateGuestRequest extends ApiRequest
{
    use TableTrait;
    protected $staffUser;
    protected $guestUser;

    public function __construct()
    {
        $this->staffUser = Auth::user();
        $this->guestUser = Customer::find(request()->id);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => [
                'required',
                function($attribute, $value, $fail)
                {
                    $staffID = $this->staffUser->id;
                    $weddingID = $this->guestUser->wedding_id;
                    $exist = Wedding::where('id', $weddingID)
                        ->whereHas('place.restaurant.user', function($q) use($staffID){
                            $q->where('id', $staffID);
                        })
                        ->exists();
                    if(!$exist){
                        $fail(__('messages.participant.validation.id.exists'));
                    }
                }
            ],
            'join_status' => 'required|numeric',
            'first_name' => 'required|max:10',
            'last_name' => 'required|max:10',
            'relationship_couple' => 'required|max:50',
            'email' => 'nullable|max:50|email|regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix',
            'post_code' => 'nullable|digits:7|numeric',
            'phone' => 'nullable|digits_between:10,11',
            'address' => 'nullable|max:200|string',
            'table_position_id' => [
                function($attribute, $value, $fail)
                {
                    $guestID = request()->id;
                    $tableID = request()->table_position_id;
                    $guest = Customer::where('id', $guestID)
                        ->where('role', Role::GUEST);
                    $guestTableID = $guest->first()->tablePosition()->first()->id ?? null;
                    $joinStatus = request()->join_status;
                    $weddingID = Customer::find($guestID)->wedding_id ?? null;
                    #Check exists offline
                    $existOffline = TablePosition::find($tableID)->whereHas('customers', function($q) use($guestID){
                        $q->whereId($guestID);
                        $q->where('join_status', CustomerConstant::JOIN_STATUS_JOIN_OFFLINE);
                    })->exists();
                    #Check exists online
                    $existOnline = TablePosition::find($tableID)->whereHas('customers', function($q) use($guestID){
                        $q->whereId($guestID);
                        $q->where('join_status', CustomerConstant::JOIN_STATUS_APPROVED);
                    })->exists();
                    # Join status online (1)
                    if(
                        ($joinStatus == CustomerConstant::JOIN_STATUS_APPROVED AND !$existOnline) OR
                        ($joinStatus == CustomerConstant::JOIN_STATUS_APPROVED AND $guestTableID != $tableID)
                    )
                    {
                        $amoutGuestOnline = $this->amountGuestWithJoinStatus(
                            CustomerConstant::JOIN_STATUS_APPROVED,
                            $tableID,
                            $weddingID
                        );
                        if($amoutGuestOnline >= Common::MAX_ONLINE_TABLE)
                        {
                            return $fail(__('messages.participant.max_remote'));
                        }
                    }
                    # Join status offline (2)
                    if(
                        ($joinStatus == CustomerConstant::JOIN_STATUS_JOIN_OFFLINE AND !$existOffline) OR
                        ($joinStatus == CustomerConstant::JOIN_STATUS_JOIN_OFFLINE AND $guestTableID != $tableID)
                    ){
                        $amoutGuestOffline = $this->amountGuestWithJoinStatus(
                            CustomerConstant::JOIN_STATUS_JOIN_OFFLINE,
                            $tableID,
                            $weddingID
                        );
                        $amountChair = TablePosition::find($tableID)->amount_chair ?? 0;
                        if($amoutGuestOffline >= $amountChair)
                        {
                            return $fail(__('messages.participant.max_offline'));
                        }
                    }
                    # Require join remote and join offline
                    if(
                        (request()->join_status == CustomerConstant::JOIN_STATUS_APPROVED OR
                        request()->join_status == CustomerConstant::JOIN_STATUS_JOIN_OFFLINE) AND
                        !isset(request()->table_position_id)
                    ){
                        return $fail(__('messages.participant.validation.table_position_id.required'));
                    }
                }
            ],
        ];
    }

    public function messages()
    {
        return [
            'id.required' => __('messages.participant.validation.id.required'),

            'join_status.required' => __('messages.participant.validation.join_status.required'),
            'join_status.numeric' => __('messages.participant.validation.join_status.numeric'),

            'first_name.required' => __('messages.participant.validation.first_name.required'),
            'first_name.max' => __('messages.participant.validation.first_name.max'),

            'last_name.required' => __('messages.participant.validation.last_name.required'),
            'last_name.max' => __('messages.participant.validation.last_name.max'),

            'relationship_couple.required' => __('messages.participant.validation.relationship_couple.required'),
            'relationship_couple.max' => __('messages.participant.validation.relationship_couple.max'),

            'email.max' => __('messages.participant.validation.email.max'),
            'email.regex' => __('messages.participant.validation.email.regex'),
            'email.email' => __('messages.participant.validation.email.regex'),

            'post_code.required' => __('messages.participant.validation.post_code.required'),
            'post_code.digits' => __('messages.participant.validation.post_code.digits'),
            'post_code.numeric' => __('messages.participant.validation.post_code.numeric'),

            'address.required' => __('messages.participant.validation.address.required'),
            'address.max' => __('messages.participant.validation.address.max'),

            'phone.required' => __('messages.participant.validation.phone.required'),
            'phone.digits_between' => __('messages.participant.validation.phone.digits_between'),

            'table_position_id' => __('messages.participant.validation.table_position_id.required'),
        ];
    }
}