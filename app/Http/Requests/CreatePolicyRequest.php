<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Constants\FileConstant;

class CreatePolicyRequest extends ApiRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'numeric',
            'file_path' => [
                'string',
                function ($attribute, $value, $fail) {
                    $valueArr = explode('.', $value);
                    $maxLength = count($valueArr) - 1;
                    if(!in_array($valueArr[$maxLength], FileConstant::TYPE_FILE_POLICY)){
                        return $fail(__('messages.policy.validation.file_policy.format'));
                    }
                }
            ],
            'status' => 'numeric',
            'name' => 'nullable|string'
        ];
    }

    public function messages()
    {
        return [
            'type.numeric' => __('messages.policy.validation.type.numeric'),
            'status.numeric' => __('messages.policy.validation.status.numeric'),
        ];
    }
}
