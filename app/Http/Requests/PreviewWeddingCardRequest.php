<?php

namespace App\Http\Requests;

use App\Http\Requests\ApiRequest;
use Illuminate\Support\Facades\Auth;
use App\Constants\Role;
use App\Models\Customer;

class PreviewWeddingCardRequest extends ApiRequest
{
   
    protected $coupleUser;

    public function __construct()
    {
        $this->coupleUser = Auth::guard('customer')->user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => [
                'required',
                function($attribute, $value, $fail)
                {
                    $guestID = request()->id;
                    $weddingID = $this->coupleUser->wedding_id;

                    $exist = Customer::where('id', $guestID)
                        ->where('wedding_id', $weddingID)
                        ->exists();

                    if(!$exist){
                        $fail(__('messages.participant.validation.id.exists'));
                    }
                }
            ]
        ];
    }
}