<?php

namespace App\Http\Requests;

use App\Http\Requests\ApiRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use App\Models\Wedding;

class ResetCouplePasswordRequest extends ApiRequest
{
   
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'wedding_id' => [
                'required',
                function($attribute, $value, $fail)
                {
                    $staffID = Auth::user()->id;
                    $wedding = Wedding::where('id', $value)
                        ->whereHas('place.restaurant.user', function($q) use($staffID){
                            $q->whereId($staffID);
                        });

                    if(!$wedding->exists()){
                        $fail(__('messages.event.not_found'));
                    }
                }
            ],
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $response = $this->respondError(
            Response::HTTP_BAD_REQUEST, 
            __('messages.event.not_found')
        );

        throw new HttpResponseException($response);
    }

    
}
