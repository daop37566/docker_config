<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Repositories\PlaceRepository;
use App\Services\PlaceService;
use App\Http\Requests\CreatePlaceRequest;
use App\Http\Requests\UpdatePlaceRequest;
use App\Http\Requests\UploadPreSignedRequest;
use App\Models\Place;
use App\Services\File\Tasks\UploadPreSignedTask;

class PlacesController extends Controller
{
    protected $placeRepo;
    protected $placeService;

    public function __construct(PlaceRepository $placeRepo, PlaceService $placeService)
    {
        $this->placeRepo = $placeRepo;
        $this->placeService = $placeService;
    }

    /**
     * Admin staff create place
     * @param $request
     * @return $status
     * **/
    public function store(CreatePlaceRequest $request)
    {
        \DB::beginTransaction();
        try {
            $place = $this->placeService->storePlace($request);
            \DB::commit();

            return $this->respondSuccess([
                'place' => $place,
                'message' => __('messages.place.create_success')
            ]);
        }  catch (\Exception $e) {
            \DB::rollback();
            throw $e;
            return $this->respondError(Response::HTTP_BAD_REQUEST, $e->getMessage());
        }
    }

    /**
     * Get place list
     * @param $request
     * @return $places
     * **/
    public function index(Request $request)
    {
        $places = $this->placeService->getAll($request);
        return $this->respondSuccess($places);
    }

    /**
     * Show detail place
     * @param $id
     * @return $place
     * **/
    public function show($id)
    {
        $place = $this->placeService->showDetail($id);
        return $this->respondSuccess($place);
    }

    /**
     * Update place
     * @param $request, $id
     * @return $status
     * **/
    public function update(UpdatePlaceRequest $request, $id)
    {
        \DB::beginTransaction();
        try {

            $place = $this->placeService->updatePlace($id, $request);
            $isWedding = $this->placeService->isPlaceWedding($id);

            if($isWedding && !($request->status)){
                return $this->respondError(Response::HTTP_NOT_IMPLEMENTED, __('messages.place.wedding_exists'));
            }

            if ($place) {
                \DB::commit();

                return $this->respondSuccess([
                    'place' => $place,
                    'message' => __('messages.place.update_success')
                ]);
            }

            \DB::rollback();
            return $this->respondError(Response::HTTP_NOT_FOUND, __('messages.place.update_fail'));
        }  catch (\Exception $e) {
            \DB::rollback();
            throw $e;
            return $this->respondError(Response::HTTP_BAD_REQUEST, $e->getMessage());
        }
    }

    /**
     * Destroy place
     * @param $id
     * @return $status
     * **/
    public function destroy($id)
    {
        $isDel = $this->placeRepo->delete($id);
        if ($isDel) return $this->respondSuccess([
            'status' => true,
            'message' => __('messages.place.delete_sucess')
        ]);

        return $this->respondError(Response::HTTP_NOT_IMPLEMENTED, __('messages.place.delete_fail'));
    }

    /**
     * Get presigned place
     * @param $request
     * @return $status
     * **/
    public function getPreSigned(UploadPreSignedRequest $request)
    {
        try {
            $place = $this->placeService->getPreSigned($request);

            return $this->respondSuccess($place);
        }  catch (\Exception $e) {
            throw $e;
            return $this->respondError(Response::HTTP_BAD_REQUEST, $e->getMessage());
        }
    }
}
