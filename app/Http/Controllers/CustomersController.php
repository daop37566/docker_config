<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\CreateParticipantRequest;
use App\Http\Requests\UpdateParticipantRequest;
use App\Http\Requests\StaffGetListGuestRequest;
use App\Http\Requests\StaffAddGuestRequest;
use App\Http\Requests\CoupleUpdateGuestRequest;
use App\Http\Requests\StaffUpdateGuestRequest;
use App\Http\Requests\CoupleReoderGuestRequest;
use App\Http\Requests\StaffReoderGuestRequest;
use App\Http\Requests\StaffGetGuestRequest;
use App\Http\Requests\CoupleUpdateGuestTableRequest;
use App\Http\Requests\DeleteMultipleGuestRequest;
use App\Http\Requests\ResetCouplePasswordRequest;
use App\Http\Requests\UpdateGuestContentRequest;
use App\Http\Requests\ExportCsvRequest;
use App\Exports\CustomerExport;
use App\Http\Requests\CreateCustomerForWeddingRequest;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Requests\UpdateIsSpeechRequest;
use App\Services\CustomerService;
use Auth;
use DB;

class CustomersController extends Controller
{
    protected $customerService;
    protected $customer;

    public function __construct(CustomerService $customerService)
    {
        $this->customerService = $customerService;
        $this->customer = Auth::guard('customer')->user();

        /** Middleware Deadline Couple Function **/
        $this->middleware('DeadlineCouple')->only([
            'coupleUpdateGuestTable',
            'store',
            'coupleUpdateGuestInfo',
            'destroy',
            'update'
        ]);

        /** Middleware Deadline Guest For Couple **/
        $this->middleware('DeadlineGuestForCouple')->only([
            'store',
            'update',
            'destroy'
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $weddingId = $this->customer->wedding_id;
        $requestData = $request->only('paginate');
        $data = $this->customerService->getListParticipant($requestData, $weddingId);

        if($data){
            return $this->respondSuccess($data);
        }

        return $this->respondError(
            Response::HTTP_BAD_REQUEST, __('messages.participant.list_fail')
        );
    }

    /**
     * Couple get list guest
     * @param $request
     * @return $data
     * **/
    public function coupleListGuest(Request $request)
    {
        $weddingID = $this->customer->wedding_id;
        $data = $this->customerService->staffCoupleGetListGuest($weddingID, $request);

        if($data){
            return $this->respondSuccess($data);
        }

        return $this->respondError(Response::HTTP_BAD_REQUEST, __('messages.event.list_null'));
    }

    /**
     * UI staff - Staff all guest list
     * return list all guest with table
     * **/
    public function staffListGuest(StaffGetListGuestRequest $request)
    {
        $weddingID = $request->id;
        $data = $this->customerService->staffCoupleGetListGuest($weddingID, $request);

        if($data){
            return $this->respondSuccess($data);
        }

        return $this->respondError(Response::HTTP_BAD_REQUEST, __('messages.event.list_null'));
    }

    /**
     * UI staff - Staff unlock
     * return List main guest
     * **/
    public function staffListMainGuest(StaffGetListGuestRequest $request)
    {
        $weddingID = $request->id;
        $data = $this->customerService->getListParticipant($request, $weddingID);

        if($data){
            return $this->respondSuccess($data);
        }

        return $this->respondError(Response::HTTP_BAD_REQUEST, __('messages.participant.list_fail'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * ==> Couple Create Participant Of The Wedding <==
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateParticipantRequest $request)
    {
        $requestData = $request->all();
        $weddingId = $this->customer->wedding_id;

        DB::beginTransaction();
        try {
            $data = $this->customerService->createParticipant($requestData, $weddingId);
            if($data){
                DB::commit();
                return $this->respondSuccess([
                    'message' => __('messages.participant.create_success')
                ]);
            }

            DB::rollback();
        } catch (\Exception $th) {
            DB::rollback();
            throw $th;
            return $this->respondError(
                Response::HTTP_BAD_REQUEST, __('messages.participant.create_fail')
            );
        }
    }

    /**
     * UI STAFF - STAFF ADD GUEST
     * return MESSAGES
     * @param $request
     * **/
    public function staffAddGuest(StaffAddGuestRequest $request)
    {
        $requestData = $request->all();
        DB::beginTransaction();

        try {
            $data = $this->customerService->staffCreateGuest($requestData);
            if($data){
                DB::commit();
                return $this->respondSuccess([
                    'message' => __('messages.participant.create_success')
                ]);
            }

            DB::rollback();
        } catch (\Exception $th) {
            DB::rollback();
            throw $th;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $weddingId = $this->customer->wedding_id;
        $customerId = $this->customer->id;
        $data = $this->customerService->detailParticipant($id, $weddingId, $customerId);

        if($data){
            return $this->respondSuccess($data);
        }else{
            return $this->respondError(
                Response::HTTP_NOT_FOUND, __('messages.participant.not_found')
            );
        }

        return $this->respondError(
            Response::HTTP_BAD_REQUEST, __('messages.participant.detail_fail')
        );
    }

    /**
     * Staff get detail guest info
     * @param $request
     * @return $data
     * **/
    public function staffGetGuestInfo(StaffGetGuestRequest $request)
    {
        $guestID = $request->id;
        $data = $this->customerService->staffGetParticipant($guestID);

        if($data){
            return $this->respondSuccess($data);
        }else{
            return $this->respondError(
                Response::HTTP_NOT_FOUND, __('messages.participant.not_found')
            );
        }

        return $this->respondError(
            Response::HTTP_BAD_REQUEST, __('messages.participant.detail_fail')
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateParticipantRequest $request)
    {
        $requestData = $request->all();
        $weddingId = $this->customer->wedding_id;

        DB::beginTransaction();
        try {
            $data = $this->customerService->updateParticipantInfo($requestData, $weddingId);
            if($data){
                DB::commit();
                return $this->respondSuccess([
                    'message' => __('messages.participant.update_success')
                ]);
            }

            DB::rollback();
        } catch (\Exception $th) {
            DB::rollback();
            throw $th;
            return $this->respondError(
                Response::HTTP_BAD_REQUEST, __('messages.participant.update_fail')
            );
        }
    }

    /**
     * UI COUPLE - [U064] Couple Edit Guest Info
     * @param $request
     * **/
    public function coupleUpdateGuestInfo(CoupleUpdateGuestRequest $request)
    {
        $requestData = $request->all();
        DB::beginTransaction();
        try {
            $status = $this->customerService->coupleUpdateGuestInfo($requestData);
            if($status){
                DB::commit();
                return $this->respondSuccess(
                    ['message' => __('messages.participant.update_success')]
                );
            }

            DB::rollback();
        } catch (\Exception $th) {
            DB::rollback();
            throw $th;
            return $this->respondError(
                Response::HTTP_BAD_REQUEST, __('messages.participant.update_fail')
            );
        }
    }

    /**
     * Staff Edit Guest Info
     * @param $request
     * **/
    public function staffUpdateGuestInfo(StaffUpdateGuestRequest $request)
    {
        $requestData = $request->all();
        DB::beginTransaction();
        try {
            $status = $this->customerService->staffUpdateGuestInfo($requestData);
            if($status){
                DB::commit();
                return $this->respondSuccess(
                    ['message' => __('messages.participant.update_success')]
                );
            }

            DB::rollback();
        } catch (\Exception $th) {
            DB::rollback();
            throw $th;
            return $this->respondError(
                Response::HTTP_BAD_REQUEST, __('messages.participant.update_fail')
            );
        }
    }

    /**
     * UI COUPLE - [U063.1] reorder
     * @param $request
     * **/
    public function coupleReoderGuest(CoupleReoderGuestRequest $request)
    {
        $weddingID = $this->customer->wedding_id;
        $requestData = $request->only('id', 'updated_position');
        $status = $this->customerService->reoderGuest($weddingID, $requestData);

        if($status){
            return $this->respondSuccess(
                ['message' => __('messages.participant.update_success')]
            );
        }

        return $this->respondError(
            Response::HTTP_BAD_REQUEST, __('messages.participant.update_fail')
        );
    }

    /**
     * UI STAFF ADMIN - [AS157] reorder
     * @param $request
     * **/
    public function staffReoderGuest(StaffReoderGuestRequest $request)
    {
        $weddingID = $request->wedding_id;
        $requestData = $request->only('id', 'updated_position');
        $status = $this->customerService->reoderGuest($weddingID, $requestData);

        if($status){
            return $this->respondSuccess(
                ['message' => __('messages.participant.update_success')]
            );
        }

        return $this->respondError(
            Response::HTTP_BAD_REQUEST, __('messages.participant.update_fail')
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $weddingId = $this->customer->wedding_id;
        $data = $this->customerService->deleteGuests($id, $weddingId);

        if($data){
            return $this->respondSuccess(['message' => __('messages.participant.delete_success')]);
        }

        return $this->respondError(
            Response::HTTP_BAD_REQUEST, __('messages.participant.delete_fail')
        );
    }

    /**
     * Remove Multiple Guest Relatives
     *
     * @param  Reuqest array $guestID
     * @return \Illuminate\Http\Response
     */
    public function deleteMultipleGuest(DeleteMultipleGuestRequest $request)
    {
        $IDs = $request->id;
        $weddingID = $this->customer->wedding_id;
        $status = $this->customerService->deleteMultipleGuest($IDs, $weddingID);

        if($status){
            return $this->respondSuccess(['message' => __('messages.participant.delete_success')]);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getListCustomerInWedding(Request $request)
    {
        $responseData = $this->customerService->getListCustomerInWedding($request->all());

        if($responseData){
            return $this->respondSuccess($responseData);
        }


    }

    /**
     * DumpData Customer
     */
    public function createCustomerForWeeding(CreateCustomerForWeddingRequest $request)
    {
        $data = $this->customerService->dumpDataCustomer($request->wedding_id, $request->role, $request->quantity, $request->table_postion_id);
        return $this->respondSuccess($data);
    }

    /**
     * UI COUPLE - [U063] Couple Event Detail
     * @param $request
     * **/
    public function coupleUpdateGuestTable(CoupleUpdateGuestTableRequest $request)
    {
        $requestData = $request->only('id', 'table_position_id');
        $data = $this->customerService->customerJoinTable($requestData);

        if($data){
            return $this->respondSuccess([
                'messages' => __('messages.participant.update_success')
            ]);
        }

        return $this->respondError(
            Response::HTTP_BAD_REQUEST, __('messages.participant.update_fail')
        );
    }

    /**
     * UI STAFF - Button reset couple password (orange)
     * return message
     * **/
    public function resetCouplePassword(ResetCouplePasswordRequest $request)
    {
        $weddingID = $request->wedding_id;
        $status = $this->customerService->resetCouplePassword($weddingID);

        if($status){
            return $this->respondSuccess([
                'messages' => __('messages.mail.send_success')
            ]);
        }
    }

    /**
     * UI STAFF - Staff update guest content
     * return message
     * **/
    public function updateGuestContent(UpdateGuestContentRequest $request)
    {
        $requestData = $request->only('id', 'task_content', 'content');
        $status = $this->customerService->updateGuestContent($requestData);

        if($status){
            return $this->respondSuccess([
                'messages' => __('messages.participant.update_success')
            ]);
        }
    }

    /**
     * UI STAFF - STAFF EXPORT ALL GUEST CSV
     * return CSV list file
     * @param $request wedding_id
     * **/
    public function staffExportCSV(ExportCsvRequest $request)
    {
        $data = $this->customerService->dataGuestCSV($request->id, $request->type);
        if($data){
            return Excel::download(new CustomerExport($data['list']), $data['file_name']);
        }

        return $this->respondError(
            Response::HTTP_BAD_REQUEST, __('messages.participant.export_fail')
        );
    }

    /**
     * UI STAFF - STAFF EXPORT CSV
     * return CSV list file
     * **/
    public function coupleExportCSV(Request $request)
    {
        $weddingID = $this->customer->wedding_id;
        $data = $this->customerService->dataGuestCSV($weddingID, $request->type);
        if($data){
            return Excel::download(new CustomerExport($data['list']), $data['file_name']);
        }
    }

    /* UPDATE IS_SPEECH FLAG
     * @param Request $speech_status
     * return messages
     * **/
    public function updateSpeechFlag(UpdateIsSpeechRequest $request)
    {
        $requestStatus = $request->only('is_speech', 'guest_id');
        $status = $this->customerService->updateSpeechFlag($requestStatus);
        if($status){
            return $this->respondSuccess([
                'messages' => __('messages.participant.update_success')
            ]);
        }
    }


}
