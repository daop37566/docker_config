<?php

namespace App\Http\Controllers;

use App\Constants\CustomerConstant;
use App\Http\Controllers\Controller;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\CustomerLogin;
use Illuminate\Support\Facades\Auth;
use App\Constants\Role;
use App\Models\User;
use App\Services\CustomerService;
use JWTAuth;
use JWTAuthException;
use Hash;

class AuthCustomerController extends Controller
{
    private $customer;
    private $customerService;

    public function __construct(
        Customer $customer,
        CustomerService $customerService
    ){
        \Config::set('jwt.user', Customer::class);
        \Config::set('auth.providers', ['users' => [
                'driver' => 'eloquent',
                'model' => Customer::class,
            ]]);
        $this->customer = $customer;
        $this->customerService = $customerService;
    }


    /**
     * Get a JWT token via given credentials.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(CustomerLogin $request)
    {
        $username = $request->username;
        $password = $request->password;
        $customer = Customer::where('username', $username)
                            ->where('password', $password)
                            ->first();

        try {
            if(!$token = JWTAuth::fromUser($customer)){
                return $this->respondError(Response::HTTP_BAD_REQUEST, __('messages.login.couple.login_fail'));
            }
        } catch (\Exception $th) {
            throw $th;
            return $this->respondError(Response::HTTP_BAD_REQUEST, __('messages.login.couple.login_fail'));
        }

        JWTAuth::setToken($token)->toUser();
        $this->customerService->updateCustomerToken($customer->id, md5($token));
        return $this->respondSuccess($this->respondWithToken($token));
    }

    /**
     * Log out
     * Invalidate the token, so user cannot use it anymore
     * They have to relogin to get a new token
     *
     * @param Request $request
     */
    public function logout(Request $request)
    {
        //$this->validate($request, ['token' => 'required']);

        try {
            auth()->logout();
            return $this->respondSuccess([
                'message' => 'You have successfully logged out.'
            ]);
        } catch (JWTException $e) {
            throw $e;
            return $this->respondError(Response::HTTP_BAD_REQUEST, 'Failed to logout, please try again.');
        }
    }

    public function refresh()
    {
        return response(JWTAuth::getToken(), Response::HTTP_OK);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return [
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' =>  auth()->factory()->getTTL() * 60,
            'info' => \Auth::user(),
        ];
    }

    /**
     * Get a JWT token via given credentials.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function tokenLogin(Request $request)
    {
        $token = $request->token;
        $customer = Customer::where('token', $token)->first();

        try {
            if(!$token = JWTAuth::fromUser($customer)){
                return $this->respondError(Response::HTTP_BAD_REQUEST, __('messages.login.couple.login_fail'));
            }
            if($customer && $customer->join_status != CustomerConstant::JOIN_STATUS_APPROVED && $customer->role == Role::GUEST) {
                return $this->respondError(Response::HTTP_NOT_FOUND, __('messages.login.couple.login_fail'));
            }
        } catch (\Throwable $th) {
            throw $th;
            return $this->respondError(Response::HTTP_BAD_REQUEST, __('messages.login.couple.login_fail'));
        }

        JWTAuth::setToken($token)->toUser();
        $this->customerService->updateCustomerToken($customer->id, md5($token));
        return $this->respondSuccess($this->respondWithToken($token));
    }
}
