<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\Services\PolicyService;
use App\Http\Requests\CreatePolicyRequest;
use App\Http\Requests\RequestPresignedPolicyRequest;
use App\Constants\PolicyConstants;
use App\Constants\Role;

class PoliciesController extends Controller
{
    protected $policyService;

    public function __construct(PolicyService $policyService)
    {
        $this->policyService = $policyService;
    }

    /**
     * Get list 4 newest file
     * @return list
     * **/
    public function index()
    {
        $data = $this->policyService->list();
        return $this->respondSuccess($data);
    }

    /**
     * API get policy presigned url
     * @param $request
     * @return array $path
     * **/
    public function getPreSigned(RequestPresignedPolicyRequest $request)
    {
        $data = $this->policyService->getPreSigned($request->only('file_policy'));
        return $this->respondSuccess($data);
    }

    /**
     * API store data policy
     * @param request
     * @return messages
     * **/
    public function store(CreatePolicyRequest $request)
    {
        $requestData = $request->only('type', 'file_path', 'status', 'name');
        $status = $this->policyService->store($requestData);
        if($status){
            return $this->respondSuccess([
                'messages' => __('messages.policy.create_success')
            ]);
        }
    }

    /**
     * Destroy (changes status policy)
     * @param Request
     * @return messages
     * **/
    public function destroy(Request $request)
    {
        $requestData = $request->only('column', 'value');
        $status = $this->policyService->destroy($requestData);
        if($status){
            return $this->respondSuccess([
                'messages' => __('messages.policy.delete_success')
            ]);
        }
    }

    /**
     * Show policy
     * @param $type
     * @return $policyData
     * **/
    public function show($type)
    {
        $authCouple = Auth::guard('customer');
        $authAdmin = Auth::guard('api');
        $role = ($authCouple->check()) ? $authCouple->user()->role ?? 0 : $authAdmin->user()->role ?? 0;
        $data = $this->policyService->showByRole($role, $type);
        if($data){
            return $this->respondSuccess($data);
        }

        return $this->respondError(Response::HTTP_NOT_FOUND, 'Not Found !');
    }
}
