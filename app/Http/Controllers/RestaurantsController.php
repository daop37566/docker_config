<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\RestaurantRepository;

class RestaurantsController extends Controller
{
    protected $restaurantRepo;

    public function __construct(RestaurantRepository $restaurantRepo)
    {
        $this->restaurantRepo = $restaurantRepo;
    }

    public function index(Request $request)
    {
        //List
    }

    public function store(Request $request)
    {
        //Do something
    }

    public function show($id)
    {
        //Show
    }

    public function update(Request $request, $id)
    {
        //Update
    }

    public function destroy($id)
    {
        //Destroy
    }
}
