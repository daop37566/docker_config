<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Services\TableAccountService;
use App\Http\Requests\StaffGetListTableAccountRequest;

class TableAccountController extends Controller
{
    protected $tableAccountService;

    public function __construct(TableAccountService $tableAccountService)
    {
        $this->tableAccountService = $tableAccountService;
    }

    /**
     * Get list table account with wedding
     * @param $wedding_id
     * @return $data
     */
    public function getTableAccountOfWedding($wedding_id)
    {
        $exists = $this->tableAccountService->existsList($wedding_id);

        if($exists){
            $data = $this->tableAccountService->getListOfWedding($wedding_id);
            return $this->respondSuccess($data);
        }

        return $this->respondError(Response::HTTP_NOT_FOUND, "404 NOT FOUND");
    }
}
