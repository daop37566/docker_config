<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\CreateEventRequest;
use App\Http\Requests\EventIDRequest;
use App\Http\Requests\UpdateThankMsgRequest;
use App\Http\Requests\UpdateEventRequest;
use App\Http\Requests\EventLiveStreamRequest;
use App\Http\Requests\UpdateEventObserverRequest;
use App\Http\Requests\UpdateWeddingSpeechModeRequest;
use App\Http\Requests\SendInviteCardStatusRequest;
use App\Http\Requests\UpdateWeddingIsLivestreamRequest;
use App\Services\EventService;
use Auth;
use DB;
use Exception;

class EventsController extends Controller
{
    protected $eventService;
    protected $customer;
    protected $user;

    public function __construct(
        EventService $eventService
    ){
        $this->eventService = $eventService;
        $this->customer = Auth::guard('customer')->user();
        $this->user = Auth::user();

        /** Middleware Deadline Guest For Couple **/
        $this->middleware('DeadlineGuestForCouple')->only([
            'sendInviteCardToGuest'
        ]);
    }

    /**
     * Get list event
     * @param $request
     * @return $data
     * **/
    public function index(Request $request)
    {
        $requestData = $request->all();
        $responseData = $this->eventService->eventList($requestData);

        if($responseData){
            return $this->respondSuccess($responseData);
        }

        return $this->respondError(
            Response::HTTP_BAD_REQUEST, __('messages.event.list_fail')
        );
    }

    /**
     * Staff create event
     * @param $request
     * @return $dataEvent
     * **/
    public function store(CreateEventRequest $request)
    {
        DB::beginTransaction();
        $eventData = $this->eventService->createEvent($request->all());
        try {
            if($eventData){
                DB::commit();
                return $this->respondSuccess($eventData);
            }
            DB::rollback();
        } catch (\Exception $th) {
            DB::rollback();
            throw $th;
            return $this->respondError(Response::HTTP_BAD_REQUEST, __('messages.event.create_fail'));
        }
    }

    /**
     * Show detail event
     * @param $id
     * @return $dataEvent
     * **/
    public function show($id)
    {
        $staffID = $this->user->id;
        $data = $this->eventService->staffEventDetail($staffID, $id);
        if($data){
            return $this->respondSuccess($data);
        }

        return $this->respondError(Response::HTTP_NOT_FOUND, __('messages.event.detail_fail'));
    }

    /**
     * Show Observer Event Info
     * @param $id
     * @return $data
     * **/
    public function showObserverEvent($id)
    {
        $observerID = $this->customer->id;
        $data = $this->eventService->observerEventDetail($observerID, $id);
        if($data){
            return $this->respondSuccess($data);
        }

        return $this->respondError(Response::HTTP_NOT_FOUND, __('messages.event.detail_fail'));
    }

    /**
     * Show Status Event Info
     * 
     * @param $id
     * @return $data
     * **/
    public function showStatusEvent($id)
    {
        $staffID = $this->user->id;
        $data = $this->eventService->statusEventDetail($staffID, $id);
        if($data){
            return $this->respondSuccess($data);
        }

        return $this->respondError(Response::HTTP_NOT_FOUND, __('messages.event.detail_fail'));
    }

    /**
     * Couple get detail event
     * @param null
     * @return $event detail
     * **/
    public function coupleDetailEvent()
    {
        $coupleId = $this->customer->id;
        $weddingId = $this->customer->wedding_id;

        $data = $this->eventService->coupleDetailEvent($weddingId, $coupleId);
        if($data){
            return $this->respondSuccess($data);
        }

        return $this->respondError(Response::HTTP_NOT_FOUND, __('messages.event.detail_fail'));
    }

    /**
     * Update event thank messages
     * @param $request
     * @return $status
     * **/
    public function updateThankMessage(UpdateThankMsgRequest $request)
    {
        $message = $request->all();
        $weddingId = $this->customer->wedding_id;
        $data = $this->eventService->updateThankMessage($weddingId, $message);
        if($data){
            return $this->respondSuccess([
                'message' => __('messages.event.update_success'),
            ]);
        }

        return $this->respondError(Response::HTTP_BAD_REQUEST, __('messages.event.delete_fail'));
    }

    /**
     * Get event detail for livestream
     * @param null
     * @return detail event for livestream
     * **/
    public function getWeddingEventWithBearerToken()
    {
        $customerId = $this->customer->id;
        $data = $this->eventService->getWeddingEventWithBearerToken($customerId);

        if($data){
            return $this->respondSuccess($data);
        }

        return $this->respondError(Response::HTTP_BAD_REQUEST, __('messages.event.list_null'));
    }

    /**
     * Update event detail
     * @param $request
     * @return $status
     * **/
    public function update(UpdateEventRequest $request)
    {
        $requestData = $request->all();
        $id = $request->id;
        DB::beginTransaction();
        try {
            $eventData = $this->eventService->updateEvent($id, $requestData);
            if($eventData){
                DB::commit();
                return $this->respondSuccess([
                    'message' => __('messages.event.update_success')
                ]);
            }
            DB::rollback();
        } catch (\Exception $th) {
            DB::rollback();
            throw $th;
            return $this->respondError(Response::HTTP_BAD_REQUEST, __('messages.event.update_fail'));
        }
    }

    /**
     * Update observer event
     * @param $request
     * @return $status
     * **/
    public function updateObserverEvent(UpdateEventObserverRequest $request)
    {
        $requestData = $request->all();
        $id = $request->id;
        DB::beginTransaction();
        try {
            $eventData = $this->eventService->updateEvent($id, $requestData);
            if($eventData){
                DB::commit();
                return $this->respondSuccess([
                    'message' => __('messages.event.update_success')
                ]);
            }
            DB::rollback();
        } catch (\Exception $th) {
            DB::rollback();
            throw $th;
            return $this->respondError(Response::HTTP_BAD_REQUEST, __('messages.event.update_fail'));
        }
    }

    /**
     * Update state live stream
     * @param $request
     * @return $status
     * **/
    public function updateStateLivesteam(Request $request)
    {
        try {
            $data = $this->eventService->updateStateLivesteam($request->all());
            if($data){
                return $this->respondSuccess($data);
            }
        } catch (\Exception $e) {
            throw $e;
            return $this->respondError(Response::HTTP_BAD_REQUEST, $e->getMessage());
        }
    }

    /**
     * Notify to planner
     * @param null
     * @return $status
     * **/
    public function notifyToPlanner()
    {
        $weddingID = $this->customer->wedding_id;
        $status = $this->eventService->notifyToPlanner($weddingID);
        if($status){
            return $this->respondSuccess([
                'message' => __('messages.mail.send_success')
            ]);
        }

        return $this->respondError(Response::HTTP_BAD_REQUEST, [
            'message' => __('messages.mail.send_fail')
        ]);
    }

    /**
     * Update wedding speech_mode
     * @param Request
     * @return messages
     * **/
    public function updateSpeechMode(UpdateWeddingSpeechModeRequest $request)
    {
        $requestData = $request->only('id', 'speech_mode');
        $status = $this->eventService->updateSpeechMode($requestData);
        if($status){
            return $this->respondSuccess([
                'message' => __('messages.event.update_success')
            ]);
        }
    }

    /**
     * send invitation card to guest
     * @param SendInviteCardStatusRequest
     * @return messages
     * **/
    public function sendInviteCardToGuest(SendInviteCardStatusRequest $request)
    {
        $status = $this->eventService->sendWeddingCardToGuest($request->only('id'));
        if($status){
            return $this->respondSuccess([
                'message' => __('messages.mail.send_success')
            ]);
        }
    }

    /**
     * Update wedding is_livestream status
     * @param $request
     * @return $status
     * **/
    public function updateWeddingIsLivesteam(UpdateWeddingIsLivestreamRequest $request)
    {
        $requestData = $request->only('wedding_id', 'is_livestream', 'is_close');
        $status = $this->eventService->updateIsLivestream($requestData);
        if($status){
            return $this->respondSuccess([
                'message' => __('messages.event.update_success')
            ]);
        }
    }
}
