<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Traits\ApiTrait;
use Carbon\Carbon;
use App\Models\Wedding;
use App\Models\Customer;
use Closure;

class DeadlineGuestForGuestMiddleware
{
    use ApiTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $guest = Customer::where('token', $request->token)->first();
        $weddingID = $guest->wedding_id ?? null;
        $wedding = Wedding::find($weddingID);
        $deadline = $wedding->guest_invitation_response_date ?? null;
        $isLivestream = $wedding->is_livestream ?? null;
        $today = Carbon::today()->format('Y-m-d');

        if($today > $deadline || $isLivestream == STATUS_TRUE){
            return $this->respondError(Response::HTTP_METHOD_NOT_ALLOWED, 'DEADLINE GUEST RESPOND DATE');
        }

        return $next($request);
    }
}
