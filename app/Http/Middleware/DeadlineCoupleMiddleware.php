<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Traits\ApiTrait;
use Carbon\Carbon;
use App\Models\Wedding;
use Closure;

class DeadlineCoupleMiddleware
{
    use ApiTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $couple = Auth::guard('customer')->user();
        $deadline = Wedding::whereId($couple->wedding_id)->first()->couple_edit_date;
        $today = Carbon::today()->format('Y-m-d');

        if($today <= $deadline){
            return $next($request);
        }
        
        return $this->respondError(Response::HTTP_METHOD_NOT_ALLOWED, 'DEADLINE COUPLE EDIT DATE');
    }
}
