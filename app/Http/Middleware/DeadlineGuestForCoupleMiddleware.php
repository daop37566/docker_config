<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Traits\ApiTrait;
use Carbon\Carbon;
use App\Models\Wedding;
use Closure;

class DeadlineGuestForCoupleMiddleware
{
    use ApiTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     * Prevent
     * + Create guest
     * + Edit guest
     * + Delete guest
     * + Create or update template card (step 1-4)
     * + Send wedding card to guest
     * + Notify to staff admin done weding card
     */
    public function handle($request, Closure $next)
    {
        $couple = Auth::guard('customer')->user();
        $wedding = Wedding::whereId($couple->wedding_id)->first();
        $deadline = $wedding->guest_invitation_response_date ?? null;
        $isLivestream = $wedding->is_livestream ?? null;
        $today = Carbon::today()->format('Y-m-d');

        if($today > $deadline || $isLivestream == STATUS_TRUE){
            return $this->respondError(Response::HTTP_METHOD_NOT_ALLOWED, 'DEADLINE GUEST RESPOND DATE');
        }

        return $next($request);
    }
}
