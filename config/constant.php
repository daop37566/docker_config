<?php

if (!defined('STATUS_TRUE')) define('STATUS_TRUE', '1');
if (!defined('STATUS_FALSE')) define('STATUS_FALSE', '0');
if (!defined('PAGINATE')) define('PAGINATE', '10');
if (!defined('PAGINATE_ALL')) define('PAGINATE_ALL', 'all');